﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Scripting;

    [RequiredByNativeCode]
    public sealed class AssetBundleCreateRequest : AsyncOperation
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void DisableCompatibilityChecks();

        public AssetBundle assetBundle { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

