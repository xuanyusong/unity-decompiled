﻿namespace UnityEngine
{
    using System;

    [Flags]
    public enum ParticleSystemVertexStreams
    {
        All = 0x7fffffff,
        CenterAndVertexID = 0x40,
        Color = 8,
        Custom1 = 0x800,
        Custom2 = 0x1000,
        Lifetime = 0x400,
        None = 0,
        Normal = 2,
        Position = 1,
        Random = 0x2000,
        Rotation = 0x100,
        Size = 0x80,
        Tangent = 4,
        UV = 0x10,
        UV2BlendAndFrame = 0x20,
        Velocity = 0x200
    }
}

