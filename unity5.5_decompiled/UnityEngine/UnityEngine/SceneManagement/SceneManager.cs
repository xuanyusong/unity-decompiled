﻿namespace UnityEngine.SceneManagement
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.Internal;
    using UnityEngine.Scripting;

    [RequiredByNativeCode]
    public class SceneManager
    {
        public static  event UnityAction<Scene, Scene> activeSceneChanged;

        public static  event UnityAction<Scene, LoadSceneMode> sceneLoaded;

        public static  event UnityAction<Scene> sceneUnloaded;

        public static Scene CreateScene(string sceneName)
        {
            Scene scene;
            INTERNAL_CALL_CreateScene(sceneName, out scene);
            return scene;
        }

        public static Scene GetActiveScene()
        {
            Scene scene;
            INTERNAL_CALL_GetActiveScene(out scene);
            return scene;
        }

        [Obsolete("Use SceneManager.sceneCount and SceneManager.GetSceneAt(int index) to loop the all scenes instead.")]
        public static Scene[] GetAllScenes()
        {
            Scene[] sceneArray = new Scene[sceneCount];
            for (int i = 0; i < sceneCount; i++)
            {
                sceneArray[i] = GetSceneAt(i);
            }
            return sceneArray;
        }

        public static Scene GetSceneAt(int index)
        {
            Scene scene;
            INTERNAL_CALL_GetSceneAt(index, out scene);
            return scene;
        }

        public static Scene GetSceneByBuildIndex(int buildIndex)
        {
            Scene scene;
            INTERNAL_CALL_GetSceneByBuildIndex(buildIndex, out scene);
            return scene;
        }

        public static Scene GetSceneByName(string name)
        {
            Scene scene;
            INTERNAL_CALL_GetSceneByName(name, out scene);
            return scene;
        }

        public static Scene GetSceneByPath(string scenePath)
        {
            Scene scene;
            INTERNAL_CALL_GetSceneByPath(scenePath, out scene);
            return scene;
        }

        [RequiredByNativeCode]
        private static void Internal_ActiveSceneChanged(Scene previousActiveScene, Scene newActiveScene)
        {
            if (activeSceneChanged != null)
            {
                activeSceneChanged(previousActiveScene, newActiveScene);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_CreateScene(string sceneName, out Scene value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetActiveScene(out Scene value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetSceneAt(int index, out Scene value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetSceneByBuildIndex(int buildIndex, out Scene value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetSceneByName(string name, out Scene value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetSceneByPath(string scenePath, out Scene value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_MergeScenes(ref Scene sourceScene, ref Scene destinationScene);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_MoveGameObjectToScene(GameObject go, ref Scene scene);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_SetActiveScene(ref Scene scene);
        [RequiredByNativeCode]
        private static void Internal_SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (sceneLoaded != null)
            {
                sceneLoaded(scene, mode);
            }
        }

        [RequiredByNativeCode]
        private static void Internal_SceneUnloaded(Scene scene)
        {
            if (sceneUnloaded != null)
            {
                sceneUnloaded(scene);
            }
        }

        [ExcludeFromDocs]
        public static void LoadScene(int sceneBuildIndex)
        {
            LoadSceneMode single = LoadSceneMode.Single;
            LoadScene(sceneBuildIndex, single);
        }

        [ExcludeFromDocs]
        public static void LoadScene(string sceneName)
        {
            LoadSceneMode single = LoadSceneMode.Single;
            LoadScene(sceneName, single);
        }

        public static void LoadScene(int sceneBuildIndex, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
        {
            LoadSceneAsyncNameIndexInternal(null, sceneBuildIndex, mode == LoadSceneMode.Additive, true);
        }

        public static void LoadScene(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
        {
            LoadSceneAsyncNameIndexInternal(sceneName, -1, mode == LoadSceneMode.Additive, true);
        }

        [ExcludeFromDocs]
        public static AsyncOperation LoadSceneAsync(int sceneBuildIndex)
        {
            LoadSceneMode single = LoadSceneMode.Single;
            return LoadSceneAsync(sceneBuildIndex, single);
        }

        [ExcludeFromDocs]
        public static AsyncOperation LoadSceneAsync(string sceneName)
        {
            LoadSceneMode single = LoadSceneMode.Single;
            return LoadSceneAsync(sceneName, single);
        }

        public static AsyncOperation LoadSceneAsync(int sceneBuildIndex, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
        {
            return LoadSceneAsyncNameIndexInternal(null, sceneBuildIndex, mode == LoadSceneMode.Additive, false);
        }

        public static AsyncOperation LoadSceneAsync(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
        {
            return LoadSceneAsyncNameIndexInternal(sceneName, -1, mode == LoadSceneMode.Additive, false);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern AsyncOperation LoadSceneAsyncNameIndexInternal(string sceneName, int sceneBuildIndex, bool isAdditive, bool mustCompleteNextFrame);
        public static void MergeScenes(Scene sourceScene, Scene destinationScene)
        {
            INTERNAL_CALL_MergeScenes(ref sourceScene, ref destinationScene);
        }

        public static void MoveGameObjectToScene(GameObject go, Scene scene)
        {
            INTERNAL_CALL_MoveGameObjectToScene(go, ref scene);
        }

        public static bool SetActiveScene(Scene scene)
        {
            return INTERNAL_CALL_SetActiveScene(ref scene);
        }

        [Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
        public static bool UnloadScene(int sceneBuildIndex)
        {
            bool flag;
            UnloadSceneNameIndexInternal("", sceneBuildIndex, true, out flag);
            return flag;
        }

        [Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
        public static bool UnloadScene(string sceneName)
        {
            bool flag;
            UnloadSceneNameIndexInternal(sceneName, -1, true, out flag);
            return flag;
        }

        [Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
        public static bool UnloadScene(Scene scene)
        {
            bool flag;
            UnloadSceneNameIndexInternal("", scene.buildIndex, true, out flag);
            return flag;
        }

        public static AsyncOperation UnloadSceneAsync(int sceneBuildIndex)
        {
            bool flag;
            return UnloadSceneNameIndexInternal("", sceneBuildIndex, false, out flag);
        }

        public static AsyncOperation UnloadSceneAsync(string sceneName)
        {
            bool flag;
            return UnloadSceneNameIndexInternal(sceneName, -1, false, out flag);
        }

        public static AsyncOperation UnloadSceneAsync(Scene scene)
        {
            bool flag;
            return UnloadSceneNameIndexInternal("", scene.buildIndex, false, out flag);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern AsyncOperation UnloadSceneNameIndexInternal(string sceneName, int sceneBuildIndex, bool immediately, out bool outSuccess);

        public static int sceneCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int sceneCountInBuildSettings { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

