﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public class Behaviour : Component
    {
        public bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool isActiveAndEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

