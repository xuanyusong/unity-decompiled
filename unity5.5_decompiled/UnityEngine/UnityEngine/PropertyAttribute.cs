﻿namespace UnityEngine
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [AttributeUsage(AttributeTargets.Field, Inherited=true, AllowMultiple=false)]
    public abstract class PropertyAttribute : Attribute
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <order>k__BackingField;

        protected PropertyAttribute()
        {
        }

        public int order { get; set; }
    }
}

