﻿namespace UnityEngine.iOS
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Scripting;

    [StructLayout(LayoutKind.Sequential), UsedByNativeCode]
    public sealed class OnDemandResourcesRequest : AsyncOperation, IDisposable
    {
        internal OnDemandResourcesRequest()
        {
        }

        public string error { [MethodImpl(MethodImplOptions.InternalCall)] get; }
        public float loadingPriority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern string GetResourcePath(string resourceName);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        public extern void Dispose();
        ~OnDemandResourcesRequest()
        {
            this.Dispose();
        }
    }
}

