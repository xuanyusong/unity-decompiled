﻿namespace UnityEngine.iOS
{
    using System;
    using System.Runtime.CompilerServices;

    public static class OnDemandResources
    {
        public static OnDemandResourcesRequest PreloadAsync(string[] tags)
        {
            return PreloadAsyncInternal(tags);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern OnDemandResourcesRequest PreloadAsyncInternal(string[] tags);

        public static bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

