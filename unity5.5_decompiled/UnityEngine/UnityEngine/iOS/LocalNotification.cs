﻿namespace UnityEngine.iOS
{
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Scripting;

    [RequiredByNativeCode]
    public sealed class LocalNotification
    {
        private static long m_NSReferenceDateTicks;
        private IntPtr notificationWrapper;

        static LocalNotification()
        {
            DateTime time = new DateTime(0x7d1, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            m_NSReferenceDateTicks = time.Ticks;
        }

        public LocalNotification()
        {
            this.InitWrapper();
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private extern void Destroy();
        ~LocalNotification()
        {
            this.Destroy();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern double GetFireDate();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void InitWrapper();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void SetFireDate(double dt);

        public string alertAction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string alertBody { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string alertLaunchImage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int applicationIconBadgeNumber { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string defaultSoundName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public DateTime fireDate
        {
            get
            {
                return new DateTime(((long) (this.GetFireDate() * 10000000.0)) + m_NSReferenceDateTicks);
            }
            set
            {
                this.SetFireDate(((double) (value.ToUniversalTime().Ticks - m_NSReferenceDateTicks)) / 10000000.0);
            }
        }

        public bool hasAction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public UnityEngine.iOS.CalendarIdentifier repeatCalendar { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public UnityEngine.iOS.CalendarUnit repeatInterval { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string soundName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string timeZone { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public IDictionary userInfo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

