﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public class TextAsset : UnityEngine.Object
    {
        public override string ToString()
        {
            return this.text;
        }

        public byte[] bytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public string text { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

