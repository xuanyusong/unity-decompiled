﻿namespace UnityEngine
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public sealed class HelpURLAttribute : Attribute
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <URL>k__BackingField;

        public HelpURLAttribute(string url)
        {
            this.URL = url;
        }

        public string URL { get; private set; }
    }
}

