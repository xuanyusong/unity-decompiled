﻿namespace UnityEngine
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.Rendering;

    public sealed class Shader : UnityEngine.Object
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void DisableKeyword(string keyword);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void EnableKeyword(string keyword);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern Shader Find(string name);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern Shader FindBuiltin(string name);
        public static Color GetGlobalColor(int nameID)
        {
            Color color;
            INTERNAL_CALL_GetGlobalColor(nameID, out color);
            return color;
        }

        public static Color GetGlobalColor(string name)
        {
            return GetGlobalColor(PropertyToID(name));
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern float GetGlobalFloat(int nameID);
        public static float GetGlobalFloat(string name)
        {
            return GetGlobalFloat(PropertyToID(name));
        }

        public static float[] GetGlobalFloatArray(int nameID)
        {
            return GetGlobalFloatArrayImpl(nameID);
        }

        public static float[] GetGlobalFloatArray(string name)
        {
            return GetGlobalFloatArray(PropertyToID(name));
        }

        public static void GetGlobalFloatArray(int nameID, List<float> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            GetGlobalFloatArrayImplList(nameID, values);
        }

        public static void GetGlobalFloatArray(string name, List<float> values)
        {
            GetGlobalFloatArray(PropertyToID(name), values);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern float[] GetGlobalFloatArrayImpl(int nameID);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void GetGlobalFloatArrayImplList(int nameID, object list);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int GetGlobalInt(int nameID);
        public static int GetGlobalInt(string name)
        {
            return GetGlobalInt(PropertyToID(name));
        }

        public static Matrix4x4 GetGlobalMatrix(int nameID)
        {
            Matrix4x4 matrixx;
            INTERNAL_CALL_GetGlobalMatrix(nameID, out matrixx);
            return matrixx;
        }

        public static Matrix4x4 GetGlobalMatrix(string name)
        {
            return GetGlobalMatrix(PropertyToID(name));
        }

        public static Matrix4x4[] GetGlobalMatrixArray(int nameID)
        {
            return GetGlobalMatrixArrayImpl(nameID);
        }

        public static Matrix4x4[] GetGlobalMatrixArray(string name)
        {
            return GetGlobalMatrixArray(PropertyToID(name));
        }

        public static void GetGlobalMatrixArray(int nameID, List<Matrix4x4> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            GetGlobalMatrixArrayImplList(nameID, values);
        }

        public static void GetGlobalMatrixArray(string name, List<Matrix4x4> values)
        {
            GetGlobalMatrixArray(PropertyToID(name), values);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern Matrix4x4[] GetGlobalMatrixArrayImpl(int nameID);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void GetGlobalMatrixArrayImplList(int nameID, object list);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern Texture GetGlobalTexture(int nameID);
        public static Texture GetGlobalTexture(string name)
        {
            return GetGlobalTexture(PropertyToID(name));
        }

        public static Vector4 GetGlobalVector(int nameID)
        {
            Vector4 vector;
            INTERNAL_CALL_GetGlobalVector(nameID, out vector);
            return vector;
        }

        public static Vector4 GetGlobalVector(string name)
        {
            return GetGlobalVector(PropertyToID(name));
        }

        public static Vector4[] GetGlobalVectorArray(int nameID)
        {
            return GetGlobalVectorArrayImpl(nameID);
        }

        public static Vector4[] GetGlobalVectorArray(string name)
        {
            return GetGlobalVectorArray(PropertyToID(name));
        }

        public static void GetGlobalVectorArray(int nameID, List<Vector4> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            GetGlobalVectorArrayImplList(nameID, values);
        }

        public static void GetGlobalVectorArray(string name, List<Vector4> values)
        {
            GetGlobalVectorArray(PropertyToID(name), values);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern Vector4[] GetGlobalVectorArrayImpl(int nameID);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void GetGlobalVectorArrayImplList(int nameID, object list);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetGlobalColor(int nameID, out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetGlobalMatrix(int nameID, out Matrix4x4 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetGlobalVector(int nameID, out Vector4 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_SetGlobalMatrix(int nameID, ref Matrix4x4 mat);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_SetGlobalVector(int nameID, ref Vector4 vec);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool IsKeywordEnabled(string keyword);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int PropertyToID(string name);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetGlobalBuffer(int nameID, ComputeBuffer buffer);
        public static void SetGlobalBuffer(string name, ComputeBuffer buffer)
        {
            SetGlobalBuffer(PropertyToID(name), buffer);
        }

        public static void SetGlobalColor(int nameID, Color color)
        {
            SetGlobalVector(nameID, (Vector4) color);
        }

        public static void SetGlobalColor(string propertyName, Color color)
        {
            SetGlobalColor(PropertyToID(propertyName), color);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetGlobalFloat(int nameID, float value);
        public static void SetGlobalFloat(string propertyName, float value)
        {
            SetGlobalFloat(PropertyToID(propertyName), value);
        }

        public static void SetGlobalFloatArray(int nameID, List<float> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            if (values.Count == 0)
            {
                throw new ArgumentException("Zero-sized array is not allowed.");
            }
            SetGlobalFloatArrayImplList(nameID, values);
        }

        public static void SetGlobalFloatArray(int nameID, float[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            if (values.Length == 0)
            {
                throw new ArgumentException("Zero-sized array is not allowed.");
            }
            SetGlobalFloatArrayImpl(nameID, values);
        }

        public static void SetGlobalFloatArray(string name, List<float> values)
        {
            SetGlobalFloatArray(PropertyToID(name), values);
        }

        public static void SetGlobalFloatArray(string propertyName, float[] values)
        {
            SetGlobalFloatArray(PropertyToID(propertyName), values);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetGlobalFloatArrayImpl(int nameID, float[] values);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetGlobalFloatArrayImplList(int nameID, object values);
        public static void SetGlobalInt(int nameID, int value)
        {
            SetGlobalFloat(nameID, (float) value);
        }

        public static void SetGlobalInt(string propertyName, int value)
        {
            SetGlobalFloat(propertyName, (float) value);
        }

        public static void SetGlobalMatrix(int nameID, Matrix4x4 mat)
        {
            INTERNAL_CALL_SetGlobalMatrix(nameID, ref mat);
        }

        public static void SetGlobalMatrix(string propertyName, Matrix4x4 mat)
        {
            SetGlobalMatrix(PropertyToID(propertyName), mat);
        }

        public static void SetGlobalMatrixArray(int nameID, List<Matrix4x4> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            if (values.Count == 0)
            {
                throw new ArgumentException("Zero-sized array is not allowed.");
            }
            SetGlobalMatrixArrayImplList(nameID, values);
        }

        public static void SetGlobalMatrixArray(int nameID, Matrix4x4[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            if (values.Length == 0)
            {
                throw new ArgumentException("Zero-sized array is not allowed.");
            }
            SetGlobalMatrixArrayImpl(nameID, values);
        }

        public static void SetGlobalMatrixArray(string name, List<Matrix4x4> values)
        {
            SetGlobalMatrixArray(PropertyToID(name), values);
        }

        public static void SetGlobalMatrixArray(string propertyName, Matrix4x4[] values)
        {
            SetGlobalMatrixArray(PropertyToID(propertyName), values);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetGlobalMatrixArrayImpl(int nameID, Matrix4x4[] values);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetGlobalMatrixArrayImplList(int nameID, object values);
        [Obsolete("SetGlobalTexGenMode is not supported anymore. Use programmable shaders to achieve the same effect.", true)]
        public static void SetGlobalTexGenMode(string propertyName, TexGenMode mode)
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetGlobalTexture(int nameID, Texture tex);
        public static void SetGlobalTexture(string propertyName, Texture tex)
        {
            SetGlobalTexture(PropertyToID(propertyName), tex);
        }

        [Obsolete("SetGlobalTextureMatrixName is not supported anymore. Use programmable shaders to achieve the same effect.", true)]
        public static void SetGlobalTextureMatrixName(string propertyName, string matrixName)
        {
        }

        public static void SetGlobalVector(int nameID, Vector4 vec)
        {
            INTERNAL_CALL_SetGlobalVector(nameID, ref vec);
        }

        public static void SetGlobalVector(string propertyName, Vector4 vec)
        {
            SetGlobalVector(PropertyToID(propertyName), vec);
        }

        public static void SetGlobalVectorArray(int nameID, List<Vector4> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            if (values.Count == 0)
            {
                throw new ArgumentException("Zero-sized array is not allowed.");
            }
            SetGlobalVectorArrayImplList(nameID, values);
        }

        public static void SetGlobalVectorArray(int nameID, Vector4[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            if (values.Length == 0)
            {
                throw new ArgumentException("Zero-sized array is not allowed.");
            }
            SetGlobalVectorArrayImpl(nameID, values);
        }

        public static void SetGlobalVectorArray(string name, List<Vector4> values)
        {
            SetGlobalVectorArray(PropertyToID(name), values);
        }

        public static void SetGlobalVectorArray(string propertyName, Vector4[] values)
        {
            SetGlobalVectorArray(PropertyToID(propertyName), values);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetGlobalVectorArrayImpl(int nameID, Vector4[] values);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetGlobalVectorArrayImplList(int nameID, object values);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void WarmupAllShaders();

        internal string customEditor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal DisableBatchingType disableBatching { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int globalMaximumLOD { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use Graphics.activeTier instead (UnityUpgradable) -> UnityEngine.Graphics.activeTier", false)]
        public static ShaderHardwareTier globalShaderHardwareTier
        {
            get
            {
                return (ShaderHardwareTier) Graphics.activeTier;
            }
            set
            {
                Graphics.activeTier = (GraphicsTier) value;
            }
        }

        public bool isSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int maximumLOD { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int renderQueue { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

