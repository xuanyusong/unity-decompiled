﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class FixedJoint2D : AnchoredJoint2D
    {
        public float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

