﻿namespace UnityEngine
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.Internal;
    using UnityEngine.Rendering;
    using UnityEngine.Scripting;

    public sealed class ParticleSystem : Component
    {
        [CompilerGenerated]
        private static IteratorDelegate <>f__am$cache0;
        [CompilerGenerated]
        private static IteratorDelegate <>f__am$cache1;
        [CompilerGenerated]
        private static IteratorDelegate <>f__am$cache2;
        [CompilerGenerated]
        private static IteratorDelegate <>f__am$cache3;

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern ParticleSystemVertexStreams CheckVertexStreamsMatchShader(ParticleSystemVertexStreams streams, Material material);
        [ExcludeFromDocs]
        public void Clear()
        {
            bool withChildren = true;
            this.Clear(withChildren);
        }

        public void Clear([DefaultValue("true")] bool withChildren)
        {
            if (<>f__am$cache2 == null)
            {
                <>f__am$cache2 = ps => Internal_Clear(ps);
            }
            this.IterateParticleSystems(withChildren, <>f__am$cache2);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern bool CountSubEmitterParticles(ref int count);
        public void Emit(int count)
        {
            INTERNAL_CALL_Emit(this, count);
        }

        [Obsolete("Emit with a single particle structure is deprecated. Pass a ParticleSystem.EmitParams parameter instead, which allows you to override some/all of the emission properties")]
        public void Emit(Particle particle)
        {
            this.Internal_EmitOld(ref particle);
        }

        public void Emit(EmitParams emitParams, int count)
        {
            this.Internal_Emit(ref emitParams, count);
        }

        [Obsolete("Emit with specific parameters is deprecated. Pass a ParticleSystem.EmitParams parameter instead, which allows you to override some/all of the emission properties")]
        public void Emit(Vector3 position, Vector3 velocity, float size, float lifetime, Color32 color)
        {
            Particle particle = new Particle {
                position = position,
                velocity = velocity,
                lifetime = lifetime,
                startLifetime = lifetime,
                startSize = size,
                rotation3D = Vector3.zero,
                angularVelocity3D = Vector3.zero,
                startColor = color,
                randomSeed = 5
            };
            this.Internal_EmitOld(ref particle);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void GenerateNoisePreviewTexture(Texture2D dst);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern int GenerateRandomSeed();
        public int GetCustomParticleData(List<Vector4> customData, ParticleSystemCustomData streamIndex)
        {
            return this.GetCustomParticleDataInternal(customData, (int) streamIndex);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern int GetCustomParticleDataInternal(object customData, int streamIndex);
        internal Matrix4x4 GetLocalToWorldMatrix()
        {
            Matrix4x4 matrixx;
            INTERNAL_CALL_GetLocalToWorldMatrix(this, out matrixx);
            return matrixx;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern int GetMaxTexCoordStreams();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern int GetParticles(Particle[] particles);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Emit(ParticleSystem self, int count);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetLocalToWorldMatrix(ParticleSystem self, out Matrix4x4 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool Internal_Clear(ParticleSystem self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_Emit(ref EmitParams emitParams, int count);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void Internal_EmitOld(ref Particle particle);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_startColor(out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_startRotation3D(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool Internal_IsAlive(ParticleSystem self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool Internal_Pause(ParticleSystem self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool Internal_Play(ParticleSystem self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_startColor(ref Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_startRotation3D(ref Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool Internal_Simulate(ParticleSystem self, float t, bool restart, bool fixedTimeStep);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool Internal_Stop(ParticleSystem self, ParticleSystemStopBehavior stopBehavior);
        [ExcludeFromDocs]
        public bool IsAlive()
        {
            bool withChildren = true;
            return this.IsAlive(withChildren);
        }

        public bool IsAlive([DefaultValue("true")] bool withChildren)
        {
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = ps => Internal_IsAlive(ps);
            }
            return this.IterateParticleSystems(withChildren, <>f__am$cache3);
        }

        internal bool IterateParticleSystems(bool recurse, IteratorDelegate func)
        {
            bool flag = func(this);
            if (recurse)
            {
                flag |= IterateParticleSystemsRecursive(base.transform, func);
            }
            return flag;
        }

        private static bool IterateParticleSystemsRecursive(Transform transform, IteratorDelegate func)
        {
            bool flag = false;
            int childCount = transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                Transform child = transform.GetChild(i);
                ParticleSystem component = child.gameObject.GetComponent<ParticleSystem>();
                if (component != null)
                {
                    flag = func(component);
                    if (flag)
                    {
                        return flag;
                    }
                    IterateParticleSystemsRecursive(child, func);
                }
            }
            return flag;
        }

        [ExcludeFromDocs]
        public void Pause()
        {
            bool withChildren = true;
            this.Pause(withChildren);
        }

        public void Pause([DefaultValue("true")] bool withChildren)
        {
            if (<>f__am$cache1 == null)
            {
                <>f__am$cache1 = ps => Internal_Pause(ps);
            }
            this.IterateParticleSystems(withChildren, <>f__am$cache1);
        }

        [ExcludeFromDocs]
        public void Play()
        {
            bool withChildren = true;
            this.Play(withChildren);
        }

        public void Play([DefaultValue("true")] bool withChildren)
        {
            if (<>f__am$cache0 == null)
            {
                <>f__am$cache0 = ps => Internal_Play(ps);
            }
            this.IterateParticleSystems(withChildren, <>f__am$cache0);
        }

        public void SetCustomParticleData(List<Vector4> customData, ParticleSystemCustomData streamIndex)
        {
            this.SetCustomParticleDataInternal(customData, (int) streamIndex);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void SetCustomParticleDataInternal(object customData, int streamIndex);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetParticles(Particle[] particles, int size);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void SetupDefaultType(int type);
        [ExcludeFromDocs]
        public void Simulate(float t)
        {
            bool fixedTimeStep = true;
            bool restart = true;
            bool withChildren = true;
            this.Simulate(t, withChildren, restart, fixedTimeStep);
        }

        [ExcludeFromDocs]
        public void Simulate(float t, bool withChildren)
        {
            bool fixedTimeStep = true;
            bool restart = true;
            this.Simulate(t, withChildren, restart, fixedTimeStep);
        }

        [ExcludeFromDocs]
        public void Simulate(float t, bool withChildren, bool restart)
        {
            bool fixedTimeStep = true;
            this.Simulate(t, withChildren, restart, fixedTimeStep);
        }

        public void Simulate(float t, [DefaultValue("true")] bool withChildren, [DefaultValue("true")] bool restart, [DefaultValue("true")] bool fixedTimeStep)
        {
            <Simulate>c__AnonStorey0 storey = new <Simulate>c__AnonStorey0 {
                t = t,
                restart = restart,
                fixedTimeStep = fixedTimeStep
            };
            this.IterateParticleSystems(withChildren, new IteratorDelegate(storey.<>m__0));
        }

        [ExcludeFromDocs]
        public void Stop()
        {
            ParticleSystemStopBehavior stopEmitting = ParticleSystemStopBehavior.StopEmitting;
            bool withChildren = true;
            this.Stop(withChildren, stopEmitting);
        }

        [ExcludeFromDocs]
        public void Stop(bool withChildren)
        {
            ParticleSystemStopBehavior stopEmitting = ParticleSystemStopBehavior.StopEmitting;
            this.Stop(withChildren, stopEmitting);
        }

        public void Stop([DefaultValue("true")] bool withChildren, [DefaultValue("ParticleSystemStopBehavior.StopEmitting")] ParticleSystemStopBehavior stopBehavior)
        {
            <Stop>c__AnonStorey1 storey = new <Stop>c__AnonStorey1 {
                stopBehavior = stopBehavior
            };
            this.IterateParticleSystems(withChildren, new IteratorDelegate(storey.<>m__0));
        }

        public CollisionModule collision
        {
            get
            {
                return new CollisionModule(this);
            }
        }

        public ColorBySpeedModule colorBySpeed
        {
            get
            {
                return new ColorBySpeedModule(this);
            }
        }

        public ColorOverLifetimeModule colorOverLifetime
        {
            get
            {
                return new ColorOverLifetimeModule(this);
            }
        }

        [Obsolete("duration property is deprecated. Use main.duration instead.")]
        public float duration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public EmissionModule emission
        {
            get
            {
                return new EmissionModule(this);
            }
        }

        [Obsolete("emissionRate property is deprecated. Use emission.rateOverTime, emission.rateOverDistance, emission.rateOverTimeMultiplier or emission.rateOverDistanceMultiplier instead.")]
        public float emissionRate
        {
            get
            {
                return this.emission.rateOverTimeMultiplier;
            }
            set
            {
                this.emission.rateOverTime = value;
            }
        }

        [Obsolete("enableEmission property is deprecated. Use emission.enabled instead.")]
        public bool enableEmission
        {
            get
            {
                return this.emission.enabled;
            }
            set
            {
                this.emission.enabled = value;
            }
        }

        public ExternalForcesModule externalForces
        {
            get
            {
                return new ExternalForcesModule(this);
            }
        }

        public ForceOverLifetimeModule forceOverLifetime
        {
            get
            {
                return new ForceOverLifetimeModule(this);
            }
        }

        [Obsolete("gravityModifier property is deprecated. Use main.gravityModifier or main.gravityModifierMultiplier instead.")]
        public float gravityModifier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public InheritVelocityModule inheritVelocity
        {
            get
            {
                return new InheritVelocityModule(this);
            }
        }

        public bool isEmitting { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isPaused { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isPlaying { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isStopped { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public LightsModule lights
        {
            get
            {
                return new LightsModule(this);
            }
        }

        public LimitVelocityOverLifetimeModule limitVelocityOverLifetime
        {
            get
            {
                return new LimitVelocityOverLifetimeModule(this);
            }
        }

        [Obsolete("loop property is deprecated. Use main.loop instead.")]
        public bool loop { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public MainModule main
        {
            get
            {
                return new MainModule(this);
            }
        }

        [Obsolete("maxParticles property is deprecated. Use main.maxParticles instead.")]
        public int maxParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public NoiseModule noise
        {
            get
            {
                return new NoiseModule(this);
            }
        }

        public int particleCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        [Obsolete("playbackSpeed property is deprecated. Use main.simulationSpeed instead.")]
        public float playbackSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("playOnAwake property is deprecated. Use main.playOnAwake instead.")]
        public bool playOnAwake { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public uint randomSeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public RotationBySpeedModule rotationBySpeed
        {
            get
            {
                return new RotationBySpeedModule(this);
            }
        }

        public RotationOverLifetimeModule rotationOverLifetime
        {
            get
            {
                return new RotationOverLifetimeModule(this);
            }
        }

        [Obsolete("safeCollisionEventSize has been deprecated. Use GetSafeCollisionEventSize() instead (UnityUpgradable) -> ParticlePhysicsExtensions.GetSafeCollisionEventSize(UnityEngine.ParticleSystem)", false)]
        public int safeCollisionEventSize
        {
            get
            {
                return ParticleSystemExtensionsImpl.GetSafeCollisionEventSize(this);
            }
        }

        [Obsolete("scalingMode property is deprecated. Use main.scalingMode instead.")]
        public ParticleSystemScalingMode scalingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ShapeModule shape
        {
            get
            {
                return new ShapeModule(this);
            }
        }

        [Obsolete("simulationSpace property is deprecated. Use main.simulationSpace instead.")]
        public ParticleSystemSimulationSpace simulationSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public SizeBySpeedModule sizeBySpeed
        {
            get
            {
                return new SizeBySpeedModule(this);
            }
        }

        public SizeOverLifetimeModule sizeOverLifetime
        {
            get
            {
                return new SizeOverLifetimeModule(this);
            }
        }

        [Obsolete("startColor property is deprecated. Use main.startColor instead.")]
        public Color startColor
        {
            get
            {
                Color color;
                this.INTERNAL_get_startColor(out color);
                return color;
            }
            set
            {
                this.INTERNAL_set_startColor(ref value);
            }
        }

        [Obsolete("startDelay property is deprecated. Use main.startDelay or main.startDelayMultiplier instead.")]
        public float startDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("startLifetime property is deprecated. Use main.startLifetime or main.startLifetimeMultiplier instead.")]
        public float startLifetime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("startRotation property is deprecated. Use main.startRotation or main.startRotationMultiplier instead.")]
        public float startRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("startRotation3D property is deprecated. Use main.startRotationX, main.startRotationY and main.startRotationZ instead. (Or main.startRotationXMultiplier, main.startRotationYMultiplier and main.startRotationZMultiplier).")]
        public Vector3 startRotation3D
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_startRotation3D(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_startRotation3D(ref value);
            }
        }

        [Obsolete("startSize property is deprecated. Use main.startSize or main.startSizeMultiplier instead.")]
        public float startSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("startSpeed property is deprecated. Use main.startSpeed or main.startSpeedMultiplier instead.")]
        public float startSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public SubEmittersModule subEmitters
        {
            get
            {
                return new SubEmittersModule(this);
            }
        }

        public TextureSheetAnimationModule textureSheetAnimation
        {
            get
            {
                return new TextureSheetAnimationModule(this);
            }
        }

        public float time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TrailModule trails
        {
            get
            {
                return new TrailModule(this);
            }
        }

        public TriggerModule trigger
        {
            get
            {
                return new TriggerModule(this);
            }
        }

        public bool useAutoRandomSeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public VelocityOverLifetimeModule velocityOverLifetime
        {
            get
            {
                return new VelocityOverLifetimeModule(this);
            }
        }

        [CompilerGenerated]
        private sealed class <Simulate>c__AnonStorey0
        {
            internal bool fixedTimeStep;
            internal bool restart;
            internal float t;

            internal bool <>m__0(ParticleSystem ps)
            {
                return ParticleSystem.Internal_Simulate(ps, this.t, this.restart, this.fixedTimeStep);
            }
        }

        [CompilerGenerated]
        private sealed class <Stop>c__AnonStorey1
        {
            internal ParticleSystemStopBehavior stopBehavior;

            internal bool <>m__0(ParticleSystem ps)
            {
                return ParticleSystem.Internal_Stop(ps, this.stopBehavior);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Burst
        {
            private float m_Time;
            private short m_MinCount;
            private short m_MaxCount;
            public Burst(float _time, short _count)
            {
                this.m_Time = _time;
                this.m_MinCount = _count;
                this.m_MaxCount = _count;
            }

            public Burst(float _time, short _minCount, short _maxCount)
            {
                this.m_Time = _time;
                this.m_MinCount = _minCount;
                this.m_MaxCount = _maxCount;
            }

            public float time
            {
                get
                {
                    return this.m_Time;
                }
                set
                {
                    this.m_Time = value;
                }
            }
            public short minCount
            {
                get
                {
                    return this.m_MinCount;
                }
                set
                {
                    this.m_MinCount = value;
                }
            }
            public short maxCount
            {
                get
                {
                    return this.m_MaxCount;
                }
                set
                {
                    this.m_MaxCount = value;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Size=1), Obsolete("ParticleSystem.CollisionEvent has been deprecated. Use ParticleCollisionEvent instead (UnityUpgradable) -> ParticleCollisionEvent", true)]
        public struct CollisionEvent
        {
            public Vector3 intersection
            {
                get
                {
                    return new Vector3();
                }
            }
            public Vector3 normal
            {
                get
                {
                    return new Vector3();
                }
            }
            public Vector3 velocity
            {
                get
                {
                    return new Vector3();
                }
            }
            public Collider collider
            {
                get
                {
                    return null;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CollisionModule
        {
            private ParticleSystem m_ParticleSystem;
            internal CollisionModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemCollisionType type
            {
                get
                {
                    return (ParticleSystemCollisionType) GetType(this.m_ParticleSystem);
                }
                set
                {
                    SetType(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystemCollisionMode mode
            {
                get
                {
                    return (ParticleSystemCollisionMode) GetMode(this.m_ParticleSystem);
                }
                set
                {
                    SetMode(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystem.MinMaxCurve dampen
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetDampen(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetDampen(this.m_ParticleSystem, ref value);
                }
            }
            public float dampenMultiplier
            {
                get
                {
                    return GetDampenMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetDampenMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve bounce
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetBounce(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetBounce(this.m_ParticleSystem, ref value);
                }
            }
            public float bounceMultiplier
            {
                get
                {
                    return GetBounceMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetBounceMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve lifetimeLoss
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetLifetimeLoss(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetLifetimeLoss(this.m_ParticleSystem, ref value);
                }
            }
            public float lifetimeLossMultiplier
            {
                get
                {
                    return GetLifetimeLossMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetLifetimeLossMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float minKillSpeed
            {
                get
                {
                    return GetMinKillSpeed(this.m_ParticleSystem);
                }
                set
                {
                    SetMinKillSpeed(this.m_ParticleSystem, value);
                }
            }
            public float maxKillSpeed
            {
                get
                {
                    return GetMaxKillSpeed(this.m_ParticleSystem);
                }
                set
                {
                    SetMaxKillSpeed(this.m_ParticleSystem, value);
                }
            }
            public LayerMask collidesWith
            {
                get
                {
                    return GetCollidesWith(this.m_ParticleSystem);
                }
                set
                {
                    SetCollidesWith(this.m_ParticleSystem, (int) value);
                }
            }
            public bool enableDynamicColliders
            {
                get
                {
                    return GetEnableDynamicColliders(this.m_ParticleSystem);
                }
                set
                {
                    SetEnableDynamicColliders(this.m_ParticleSystem, value);
                }
            }
            public bool enableInteriorCollisions
            {
                get
                {
                    return GetEnableInteriorCollisions(this.m_ParticleSystem);
                }
                set
                {
                    SetEnableInteriorCollisions(this.m_ParticleSystem, value);
                }
            }
            public int maxCollisionShapes
            {
                get
                {
                    return GetMaxCollisionShapes(this.m_ParticleSystem);
                }
                set
                {
                    SetMaxCollisionShapes(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemCollisionQuality quality
            {
                get
                {
                    return (ParticleSystemCollisionQuality) GetQuality(this.m_ParticleSystem);
                }
                set
                {
                    SetQuality(this.m_ParticleSystem, (int) value);
                }
            }
            public float voxelSize
            {
                get
                {
                    return GetVoxelSize(this.m_ParticleSystem);
                }
                set
                {
                    SetVoxelSize(this.m_ParticleSystem, value);
                }
            }
            public float radiusScale
            {
                get
                {
                    return GetRadiusScale(this.m_ParticleSystem);
                }
                set
                {
                    SetRadiusScale(this.m_ParticleSystem, value);
                }
            }
            public bool sendCollisionMessages
            {
                get
                {
                    return GetUsesCollisionMessages(this.m_ParticleSystem);
                }
                set
                {
                    SetUsesCollisionMessages(this.m_ParticleSystem, value);
                }
            }
            public void SetPlane(int index, Transform transform)
            {
                SetPlane(this.m_ParticleSystem, index, transform);
            }

            public Transform GetPlane(int index)
            {
                return GetPlane(this.m_ParticleSystem, index);
            }

            public int maxPlaneCount
            {
                get
                {
                    return GetMaxPlaneCount(this.m_ParticleSystem);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetType(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetType(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMode(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMode(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDampen(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetDampen(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDampenMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetDampenMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetBounce(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetBounce(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetBounceMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetBounceMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLifetimeLoss(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetLifetimeLoss(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLifetimeLossMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetLifetimeLossMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMinKillSpeed(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetMinKillSpeed(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMaxKillSpeed(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetMaxKillSpeed(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCollidesWith(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetCollidesWith(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnableDynamicColliders(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnableDynamicColliders(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnableInteriorCollisions(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnableInteriorCollisions(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMaxCollisionShapes(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMaxCollisionShapes(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetQuality(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetQuality(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetVoxelSize(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetVoxelSize(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRadiusScale(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRadiusScale(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUsesCollisionMessages(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetUsesCollisionMessages(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetPlane(ParticleSystem system, int index, Transform transform);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern Transform GetPlane(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMaxPlaneCount(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ColorBySpeedModule
        {
            private ParticleSystem m_ParticleSystem;
            internal ColorBySpeedModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxGradient color
            {
                get
                {
                    ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient();
                    GetColor(this.m_ParticleSystem, ref gradient);
                    return gradient;
                }
                set
                {
                    SetColor(this.m_ParticleSystem, ref value);
                }
            }
            public Vector2 range
            {
                get
                {
                    return GetRange(this.m_ParticleSystem);
                }
                set
                {
                    SetRange(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            private static void SetRange(ParticleSystem system, Vector2 value)
            {
                INTERNAL_CALL_SetRange(system, ref value);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);
            private static Vector2 GetRange(ParticleSystem system)
            {
                Vector2 vector;
                INTERNAL_CALL_GetRange(system, out vector);
                return vector;
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ColorOverLifetimeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal ColorOverLifetimeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxGradient color
            {
                get
                {
                    ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient();
                    GetColor(this.m_ParticleSystem, ref gradient);
                    return gradient;
                }
                set
                {
                    SetColor(this.m_ParticleSystem, ref value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct EmissionModule
        {
            private ParticleSystem m_ParticleSystem;
            internal EmissionModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve rateOverTime
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRateOverTime(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRateOverTime(this.m_ParticleSystem, ref value);
                }
            }
            public float rateOverTimeMultiplier
            {
                get
                {
                    return GetRateOverTimeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRateOverTimeMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve rateOverDistance
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRateOverDistance(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRateOverDistance(this.m_ParticleSystem, ref value);
                }
            }
            public float rateOverDistanceMultiplier
            {
                get
                {
                    return GetRateOverDistanceMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRateOverDistanceMultiplier(this.m_ParticleSystem, value);
                }
            }
            public void SetBursts(ParticleSystem.Burst[] bursts)
            {
                SetBursts(this.m_ParticleSystem, bursts, bursts.Length);
            }

            public void SetBursts(ParticleSystem.Burst[] bursts, int size)
            {
                SetBursts(this.m_ParticleSystem, bursts, size);
            }

            public int GetBursts(ParticleSystem.Burst[] bursts)
            {
                return GetBursts(this.m_ParticleSystem, bursts);
            }

            public int burstCount
            {
                get
                {
                    return GetBurstCount(this.m_ParticleSystem);
                }
            }
            [Obsolete("ParticleSystemEmissionType no longer does anything. Time and Distance based emission are now both always active.")]
            public ParticleSystemEmissionType type
            {
                get
                {
                    return ParticleSystemEmissionType.Time;
                }
                set
                {
                }
            }
            [Obsolete("rate property is deprecated. Use rateOverTime or rateOverDistance instead.")]
            public ParticleSystem.MinMaxCurve rate
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRateOverTime(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRateOverTime(this.m_ParticleSystem, ref value);
                }
            }
            [Obsolete("rateMultiplier property is deprecated. Use rateOverTimeMultiplier or rateOverDistanceMultiplier instead.")]
            public float rateMultiplier
            {
                get
                {
                    return GetRateOverTimeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRateOverTimeMultiplier(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetBurstCount(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRateOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetRateOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRateOverTimeMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRateOverTimeMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRateOverDistance(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetRateOverDistance(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRateOverDistanceMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRateOverDistanceMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetBursts(ParticleSystem system, ParticleSystem.Burst[] bursts, int size);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetBursts(ParticleSystem system, ParticleSystem.Burst[] bursts);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct EmitParams
        {
            internal ParticleSystem.Particle m_Particle;
            internal bool m_PositionSet;
            internal bool m_VelocitySet;
            internal bool m_AxisOfRotationSet;
            internal bool m_RotationSet;
            internal bool m_AngularVelocitySet;
            internal bool m_StartSizeSet;
            internal bool m_StartColorSet;
            internal bool m_RandomSeedSet;
            internal bool m_StartLifetimeSet;
            internal bool m_ApplyShapeToPosition;
            public Vector3 position
            {
                get
                {
                    return this.m_Particle.position;
                }
                set
                {
                    this.m_Particle.position = value;
                    this.m_PositionSet = true;
                }
            }
            public bool applyShapeToPosition
            {
                get
                {
                    return this.m_ApplyShapeToPosition;
                }
                set
                {
                    this.m_ApplyShapeToPosition = value;
                }
            }
            public Vector3 velocity
            {
                get
                {
                    return this.m_Particle.velocity;
                }
                set
                {
                    this.m_Particle.velocity = value;
                    this.m_VelocitySet = true;
                }
            }
            public float startLifetime
            {
                get
                {
                    return this.m_Particle.startLifetime;
                }
                set
                {
                    this.m_Particle.startLifetime = value;
                    this.m_StartLifetimeSet = true;
                }
            }
            public float startSize
            {
                get
                {
                    return this.m_Particle.startSize;
                }
                set
                {
                    this.m_Particle.startSize = value;
                    this.m_StartSizeSet = true;
                }
            }
            public Vector3 startSize3D
            {
                get
                {
                    return this.m_Particle.startSize3D;
                }
                set
                {
                    this.m_Particle.startSize3D = value;
                    this.m_StartSizeSet = true;
                }
            }
            public Vector3 axisOfRotation
            {
                get
                {
                    return this.m_Particle.axisOfRotation;
                }
                set
                {
                    this.m_Particle.axisOfRotation = value;
                    this.m_AxisOfRotationSet = true;
                }
            }
            public float rotation
            {
                get
                {
                    return this.m_Particle.rotation;
                }
                set
                {
                    this.m_Particle.rotation = value;
                    this.m_RotationSet = true;
                }
            }
            public Vector3 rotation3D
            {
                get
                {
                    return this.m_Particle.rotation3D;
                }
                set
                {
                    this.m_Particle.rotation3D = value;
                    this.m_RotationSet = true;
                }
            }
            public float angularVelocity
            {
                get
                {
                    return this.m_Particle.angularVelocity;
                }
                set
                {
                    this.m_Particle.angularVelocity = value;
                    this.m_AngularVelocitySet = true;
                }
            }
            public Vector3 angularVelocity3D
            {
                get
                {
                    return this.m_Particle.angularVelocity3D;
                }
                set
                {
                    this.m_Particle.angularVelocity3D = value;
                    this.m_AngularVelocitySet = true;
                }
            }
            public Color32 startColor
            {
                get
                {
                    return this.m_Particle.startColor;
                }
                set
                {
                    this.m_Particle.startColor = value;
                    this.m_StartColorSet = true;
                }
            }
            public uint randomSeed
            {
                get
                {
                    return this.m_Particle.randomSeed;
                }
                set
                {
                    this.m_Particle.randomSeed = value;
                    this.m_RandomSeedSet = true;
                }
            }
            public void ResetPosition()
            {
                this.m_PositionSet = false;
            }

            public void ResetVelocity()
            {
                this.m_VelocitySet = false;
            }

            public void ResetAxisOfRotation()
            {
                this.m_AxisOfRotationSet = false;
            }

            public void ResetRotation()
            {
                this.m_RotationSet = false;
            }

            public void ResetAngularVelocity()
            {
                this.m_AngularVelocitySet = false;
            }

            public void ResetStartSize()
            {
                this.m_StartSizeSet = false;
            }

            public void ResetStartColor()
            {
                this.m_StartColorSet = false;
            }

            public void ResetRandomSeed()
            {
                this.m_RandomSeedSet = false;
            }

            public void ResetStartLifetime()
            {
                this.m_StartLifetimeSet = false;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ExternalForcesModule
        {
            private ParticleSystem m_ParticleSystem;
            internal ExternalForcesModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public float multiplier
            {
                get
                {
                    return GetMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetMultiplier(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetMultiplier(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ForceOverLifetimeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal ForceOverLifetimeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve x
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public ParticleSystem.MinMaxCurve y
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public ParticleSystem.MinMaxCurve z
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float xMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float yMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float zMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemSimulationSpace space
            {
                get
                {
                    return (!GetWorldSpace(this.m_ParticleSystem) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World);
                }
                set
                {
                    SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
                }
            }
            public bool randomized
            {
                get
                {
                    return GetRandomized(this.m_ParticleSystem);
                }
                set
                {
                    SetRandomized(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWorldSpace(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetWorldSpace(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRandomized(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetRandomized(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct InheritVelocityModule
        {
            private ParticleSystem m_ParticleSystem;
            internal InheritVelocityModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemInheritVelocityMode mode
            {
                get
                {
                    return (ParticleSystemInheritVelocityMode) GetMode(this.m_ParticleSystem);
                }
                set
                {
                    SetMode(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystem.MinMaxCurve curve
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetCurve(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetCurve(this.m_ParticleSystem, ref value);
                }
            }
            public float curveMultiplier
            {
                get
                {
                    return GetCurveMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetCurveMultiplier(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMode(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMode(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCurve(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetCurve(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCurveMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetCurveMultiplier(ParticleSystem system);
        }

        internal delegate bool IteratorDelegate(ParticleSystem ps);

        [StructLayout(LayoutKind.Sequential)]
        public struct LightsModule
        {
            private ParticleSystem m_ParticleSystem;
            internal LightsModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public float ratio
            {
                get
                {
                    return GetRatio(this.m_ParticleSystem);
                }
                set
                {
                    SetRatio(this.m_ParticleSystem, value);
                }
            }
            public bool useRandomDistribution
            {
                get
                {
                    return GetUseRandomDistribution(this.m_ParticleSystem);
                }
                set
                {
                    SetUseRandomDistribution(this.m_ParticleSystem, value);
                }
            }
            public Light light
            {
                get
                {
                    return GetLightPrefab(this.m_ParticleSystem);
                }
                set
                {
                    SetLightPrefab(this.m_ParticleSystem, value);
                }
            }
            public bool useParticleColor
            {
                get
                {
                    return GetUseParticleColor(this.m_ParticleSystem);
                }
                set
                {
                    SetUseParticleColor(this.m_ParticleSystem, value);
                }
            }
            public bool sizeAffectsRange
            {
                get
                {
                    return GetSizeAffectsRange(this.m_ParticleSystem);
                }
                set
                {
                    SetSizeAffectsRange(this.m_ParticleSystem, value);
                }
            }
            public bool alphaAffectsIntensity
            {
                get
                {
                    return GetAlphaAffectsIntensity(this.m_ParticleSystem);
                }
                set
                {
                    SetAlphaAffectsIntensity(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve range
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRange(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRange(this.m_ParticleSystem, ref value);
                }
            }
            public float rangeMultiplier
            {
                get
                {
                    return GetRangeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRangeMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve intensity
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetIntensity(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetIntensity(this.m_ParticleSystem, ref value);
                }
            }
            public float intensityMultiplier
            {
                get
                {
                    return GetIntensityMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetIntensityMultiplier(this.m_ParticleSystem, value);
                }
            }
            public int maxLights
            {
                get
                {
                    return GetMaxLights(this.m_ParticleSystem);
                }
                set
                {
                    SetMaxLights(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRatio(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRatio(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUseRandomDistribution(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetUseRandomDistribution(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLightPrefab(ParticleSystem system, Light value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern Light GetLightPrefab(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUseParticleColor(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetUseParticleColor(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSizeAffectsRange(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSizeAffectsRange(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetAlphaAffectsIntensity(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetAlphaAffectsIntensity(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRange(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetRange(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRangeMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRangeMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetIntensity(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetIntensity(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetIntensityMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetIntensityMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMaxLights(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMaxLights(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LimitVelocityOverLifetimeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal LimitVelocityOverLifetimeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve limitX
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float limitXMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve limitY
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public float limitYMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve limitZ
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float limitZMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve limit
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetMagnitude(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetMagnitude(this.m_ParticleSystem, ref value);
                }
            }
            public float limitMultiplier
            {
                get
                {
                    return GetMagnitudeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetMagnitudeMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float dampen
            {
                get
                {
                    return GetDampen(this.m_ParticleSystem);
                }
                set
                {
                    SetDampen(this.m_ParticleSystem, value);
                }
            }
            public bool separateAxes
            {
                get
                {
                    return GetSeparateAxes(this.m_ParticleSystem);
                }
                set
                {
                    SetSeparateAxes(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemSimulationSpace space
            {
                get
                {
                    return (!GetWorldSpace(this.m_ParticleSystem) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World);
                }
                set
                {
                    SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMagnitude(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetMagnitude(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMagnitudeMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetMagnitudeMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDampen(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetDampen(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSeparateAxes(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSeparateAxes(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWorldSpace(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetWorldSpace(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MainModule
        {
            private ParticleSystem m_ParticleSystem;
            internal MainModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public float duration
            {
                get
                {
                    return GetDuration(this.m_ParticleSystem);
                }
                set
                {
                    SetDuration(this.m_ParticleSystem, value);
                }
            }
            public bool loop
            {
                get
                {
                    return GetLoop(this.m_ParticleSystem);
                }
                set
                {
                    SetLoop(this.m_ParticleSystem, value);
                }
            }
            public bool prewarm
            {
                get
                {
                    return GetPrewarm(this.m_ParticleSystem);
                }
                set
                {
                    SetPrewarm(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startDelay
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartDelay(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartDelay(this.m_ParticleSystem, ref value);
                }
            }
            public float startDelayMultiplier
            {
                get
                {
                    return GetStartDelayMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartDelayMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startLifetime
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartLifetime(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartLifetime(this.m_ParticleSystem, ref value);
                }
            }
            public float startLifetimeMultiplier
            {
                get
                {
                    return GetStartLifetimeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartLifetimeMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startSpeed
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartSpeed(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartSpeed(this.m_ParticleSystem, ref value);
                }
            }
            public float startSpeedMultiplier
            {
                get
                {
                    return GetStartSpeedMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartSpeedMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool startSize3D
            {
                get
                {
                    return GetStartSize3D(this.m_ParticleSystem);
                }
                set
                {
                    SetStartSize3D(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startSize
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartSizeX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartSizeX(this.m_ParticleSystem, ref value);
                }
            }
            public float startSizeMultiplier
            {
                get
                {
                    return GetStartSizeXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartSizeXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startSizeX
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartSizeX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartSizeX(this.m_ParticleSystem, ref value);
                }
            }
            public float startSizeXMultiplier
            {
                get
                {
                    return GetStartSizeXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartSizeXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startSizeY
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartSizeY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartSizeY(this.m_ParticleSystem, ref value);
                }
            }
            public float startSizeYMultiplier
            {
                get
                {
                    return GetStartSizeYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartSizeYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startSizeZ
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartSizeZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartSizeZ(this.m_ParticleSystem, ref value);
                }
            }
            public float startSizeZMultiplier
            {
                get
                {
                    return GetStartSizeZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartSizeZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool startRotation3D
            {
                get
                {
                    return GetStartRotation3D(this.m_ParticleSystem);
                }
                set
                {
                    SetStartRotation3D(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startRotation
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartRotationZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartRotationZ(this.m_ParticleSystem, ref value);
                }
            }
            public float startRotationMultiplier
            {
                get
                {
                    return GetStartRotationZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartRotationZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startRotationX
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartRotationX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartRotationX(this.m_ParticleSystem, ref value);
                }
            }
            public float startRotationXMultiplier
            {
                get
                {
                    return GetStartRotationXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartRotationXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startRotationY
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartRotationY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartRotationY(this.m_ParticleSystem, ref value);
                }
            }
            public float startRotationYMultiplier
            {
                get
                {
                    return GetStartRotationYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartRotationYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startRotationZ
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartRotationZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartRotationZ(this.m_ParticleSystem, ref value);
                }
            }
            public float startRotationZMultiplier
            {
                get
                {
                    return GetStartRotationZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartRotationZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float randomizeRotationDirection
            {
                get
                {
                    return GetRandomizeRotationDirection(this.m_ParticleSystem);
                }
                set
                {
                    SetRandomizeRotationDirection(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxGradient startColor
            {
                get
                {
                    ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient();
                    GetStartColor(this.m_ParticleSystem, ref gradient);
                    return gradient;
                }
                set
                {
                    SetStartColor(this.m_ParticleSystem, ref value);
                }
            }
            public ParticleSystem.MinMaxCurve gravityModifier
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetGravityModifier(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetGravityModifier(this.m_ParticleSystem, ref value);
                }
            }
            public float gravityModifierMultiplier
            {
                get
                {
                    return GetGravityModifierMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetGravityModifierMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemSimulationSpace simulationSpace
            {
                get
                {
                    return GetSimulationSpace(this.m_ParticleSystem);
                }
                set
                {
                    SetSimulationSpace(this.m_ParticleSystem, value);
                }
            }
            public Transform customSimulationSpace
            {
                get
                {
                    return GetCustomSimulationSpace(this.m_ParticleSystem);
                }
                set
                {
                    SetCustomSimulationSpace(this.m_ParticleSystem, value);
                }
            }
            public float simulationSpeed
            {
                get
                {
                    return GetSimulationSpeed(this.m_ParticleSystem);
                }
                set
                {
                    SetSimulationSpeed(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemScalingMode scalingMode
            {
                get
                {
                    return GetScalingMode(this.m_ParticleSystem);
                }
                set
                {
                    SetScalingMode(this.m_ParticleSystem, value);
                }
            }
            public bool playOnAwake
            {
                get
                {
                    return GetPlayOnAwake(this.m_ParticleSystem);
                }
                set
                {
                    SetPlayOnAwake(this.m_ParticleSystem, value);
                }
            }
            public int maxParticles
            {
                get
                {
                    return GetMaxParticles(this.m_ParticleSystem);
                }
                set
                {
                    SetMaxParticles(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDuration(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetDuration(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLoop(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetLoop(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetPrewarm(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetPrewarm(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartDelay(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartDelay(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartDelayMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartDelayMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartLifetimeMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartLifetimeMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSpeedMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartSpeedMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSize3D(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetStartSize3D(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSizeX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartSizeX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSizeXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartSizeXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSizeY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartSizeY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSizeYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartSizeYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSizeZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartSizeZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartSizeZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartSizeZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotation3D(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetStartRotation3D(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotationX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartRotationX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotationXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartRotationXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotationY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartRotationY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotationYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartRotationYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotationZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartRotationZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartRotationZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartRotationZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRandomizeRotationDirection(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRandomizeRotationDirection(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetGravityModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetGravityModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetGravityModifierMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetGravityModifierMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSimulationSpace(ParticleSystem system, ParticleSystemSimulationSpace value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern ParticleSystemSimulationSpace GetSimulationSpace(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCustomSimulationSpace(ParticleSystem system, Transform value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern Transform GetCustomSimulationSpace(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSimulationSpeed(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetSimulationSpeed(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetScalingMode(ParticleSystem system, ParticleSystemScalingMode value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern ParticleSystemScalingMode GetScalingMode(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetPlayOnAwake(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetPlayOnAwake(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMaxParticles(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMaxParticles(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MinMaxCurve
        {
            private ParticleSystemCurveMode m_Mode;
            private float m_CurveMultiplier;
            private AnimationCurve m_CurveMin;
            private AnimationCurve m_CurveMax;
            private float m_ConstantMin;
            private float m_ConstantMax;
            public MinMaxCurve(float constant)
            {
                this.m_Mode = ParticleSystemCurveMode.Constant;
                this.m_CurveMultiplier = 0f;
                this.m_CurveMin = null;
                this.m_CurveMax = null;
                this.m_ConstantMin = 0f;
                this.m_ConstantMax = constant;
            }

            public MinMaxCurve(float multiplier, AnimationCurve curve)
            {
                this.m_Mode = ParticleSystemCurveMode.Curve;
                this.m_CurveMultiplier = multiplier;
                this.m_CurveMin = null;
                this.m_CurveMax = curve;
                this.m_ConstantMin = 0f;
                this.m_ConstantMax = 0f;
            }

            public MinMaxCurve(float multiplier, AnimationCurve min, AnimationCurve max)
            {
                this.m_Mode = ParticleSystemCurveMode.TwoCurves;
                this.m_CurveMultiplier = multiplier;
                this.m_CurveMin = min;
                this.m_CurveMax = max;
                this.m_ConstantMin = 0f;
                this.m_ConstantMax = 0f;
            }

            public MinMaxCurve(float min, float max)
            {
                this.m_Mode = ParticleSystemCurveMode.TwoConstants;
                this.m_CurveMultiplier = 0f;
                this.m_CurveMin = null;
                this.m_CurveMax = null;
                this.m_ConstantMin = min;
                this.m_ConstantMax = max;
            }

            public ParticleSystemCurveMode mode
            {
                get
                {
                    return this.m_Mode;
                }
                set
                {
                    this.m_Mode = value;
                }
            }
            [Obsolete("Please use MinMaxCurve.curveMultiplier instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/MinMaxCurve.curveMultiplier")]
            public float curveScalar
            {
                get
                {
                    return this.m_CurveMultiplier;
                }
                set
                {
                    this.m_CurveMultiplier = value;
                }
            }
            public float curveMultiplier
            {
                get
                {
                    return this.m_CurveMultiplier;
                }
                set
                {
                    this.m_CurveMultiplier = value;
                }
            }
            public AnimationCurve curveMax
            {
                get
                {
                    return this.m_CurveMax;
                }
                set
                {
                    this.m_CurveMax = value;
                }
            }
            public AnimationCurve curveMin
            {
                get
                {
                    return this.m_CurveMin;
                }
                set
                {
                    this.m_CurveMin = value;
                }
            }
            public float constantMax
            {
                get
                {
                    return this.m_ConstantMax;
                }
                set
                {
                    this.m_ConstantMax = value;
                }
            }
            public float constantMin
            {
                get
                {
                    return this.m_ConstantMin;
                }
                set
                {
                    this.m_ConstantMin = value;
                }
            }
            public float constant
            {
                get
                {
                    return this.m_ConstantMax;
                }
                set
                {
                    this.m_ConstantMax = value;
                }
            }
            public AnimationCurve curve
            {
                get
                {
                    return this.m_CurveMax;
                }
                set
                {
                    this.m_CurveMax = value;
                }
            }
            public float Evaluate(float time)
            {
                return this.Evaluate(time, 1f);
            }

            public float Evaluate(float time, float lerpFactor)
            {
                time = Mathf.Clamp(time, 0f, 1f);
                lerpFactor = Mathf.Clamp(lerpFactor, 0f, 1f);
                if (this.m_Mode == ParticleSystemCurveMode.Constant)
                {
                    return this.m_ConstantMax;
                }
                if (this.m_Mode == ParticleSystemCurveMode.TwoConstants)
                {
                    return Mathf.Lerp(this.m_ConstantMin, this.m_ConstantMax, lerpFactor);
                }
                float b = this.m_CurveMax.Evaluate(time) * this.m_CurveMultiplier;
                if (this.m_Mode == ParticleSystemCurveMode.TwoCurves)
                {
                    return Mathf.Lerp(this.m_CurveMin.Evaluate(time) * this.m_CurveMultiplier, b, lerpFactor);
                }
                return b;
            }

            public static implicit operator ParticleSystem.MinMaxCurve(float constant)
            {
                return new ParticleSystem.MinMaxCurve(constant);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MinMaxGradient
        {
            private ParticleSystemGradientMode m_Mode;
            private Gradient m_GradientMin;
            private Gradient m_GradientMax;
            private Color m_ColorMin;
            private Color m_ColorMax;
            public MinMaxGradient(Color color)
            {
                this.m_Mode = ParticleSystemGradientMode.Color;
                this.m_GradientMin = null;
                this.m_GradientMax = null;
                this.m_ColorMin = Color.black;
                this.m_ColorMax = color;
            }

            public MinMaxGradient(Gradient gradient)
            {
                this.m_Mode = ParticleSystemGradientMode.Gradient;
                this.m_GradientMin = null;
                this.m_GradientMax = gradient;
                this.m_ColorMin = Color.black;
                this.m_ColorMax = Color.black;
            }

            public MinMaxGradient(Color min, Color max)
            {
                this.m_Mode = ParticleSystemGradientMode.TwoColors;
                this.m_GradientMin = null;
                this.m_GradientMax = null;
                this.m_ColorMin = min;
                this.m_ColorMax = max;
            }

            public MinMaxGradient(Gradient min, Gradient max)
            {
                this.m_Mode = ParticleSystemGradientMode.TwoGradients;
                this.m_GradientMin = min;
                this.m_GradientMax = max;
                this.m_ColorMin = Color.black;
                this.m_ColorMax = Color.black;
            }

            public ParticleSystemGradientMode mode
            {
                get
                {
                    return this.m_Mode;
                }
                set
                {
                    this.m_Mode = value;
                }
            }
            public Gradient gradientMax
            {
                get
                {
                    return this.m_GradientMax;
                }
                set
                {
                    this.m_GradientMax = value;
                }
            }
            public Gradient gradientMin
            {
                get
                {
                    return this.m_GradientMin;
                }
                set
                {
                    this.m_GradientMin = value;
                }
            }
            public Color colorMax
            {
                get
                {
                    return this.m_ColorMax;
                }
                set
                {
                    this.m_ColorMax = value;
                }
            }
            public Color colorMin
            {
                get
                {
                    return this.m_ColorMin;
                }
                set
                {
                    this.m_ColorMin = value;
                }
            }
            public Color color
            {
                get
                {
                    return this.m_ColorMax;
                }
                set
                {
                    this.m_ColorMax = value;
                }
            }
            public Gradient gradient
            {
                get
                {
                    return this.m_GradientMax;
                }
                set
                {
                    this.m_GradientMax = value;
                }
            }
            public Color Evaluate(float time)
            {
                return this.Evaluate(time, 1f);
            }

            public Color Evaluate(float time, float lerpFactor)
            {
                time = Mathf.Clamp(time, 0f, 1f);
                lerpFactor = Mathf.Clamp(lerpFactor, 0f, 1f);
                if (this.m_Mode == ParticleSystemGradientMode.Color)
                {
                    return this.m_ColorMax;
                }
                if (this.m_Mode == ParticleSystemGradientMode.TwoColors)
                {
                    return Color.Lerp(this.m_ColorMin, this.m_ColorMax, lerpFactor);
                }
                Color b = this.m_GradientMax.Evaluate(time);
                if (this.m_Mode == ParticleSystemGradientMode.TwoGradients)
                {
                    return Color.Lerp(this.m_GradientMin.Evaluate(time), b, lerpFactor);
                }
                return b;
            }

            public static implicit operator ParticleSystem.MinMaxGradient(Color color)
            {
                return new ParticleSystem.MinMaxGradient(color);
            }

            public static implicit operator ParticleSystem.MinMaxGradient(Gradient gradient)
            {
                return new ParticleSystem.MinMaxGradient(gradient);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NoiseModule
        {
            private ParticleSystem m_ParticleSystem;
            internal NoiseModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public bool separateAxes
            {
                get
                {
                    return GetSeparateAxes(this.m_ParticleSystem);
                }
                set
                {
                    SetSeparateAxes(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve strength
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStrengthX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStrengthX(this.m_ParticleSystem, ref value);
                }
            }
            public float strengthMultiplier
            {
                get
                {
                    return GetStrengthXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStrengthXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve strengthX
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStrengthX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStrengthX(this.m_ParticleSystem, ref value);
                }
            }
            public float strengthXMultiplier
            {
                get
                {
                    return GetStrengthXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStrengthXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve strengthY
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStrengthY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStrengthY(this.m_ParticleSystem, ref value);
                }
            }
            public float strengthYMultiplier
            {
                get
                {
                    return GetStrengthYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStrengthYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve strengthZ
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStrengthZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStrengthZ(this.m_ParticleSystem, ref value);
                }
            }
            public float strengthZMultiplier
            {
                get
                {
                    return GetStrengthZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStrengthZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float frequency
            {
                get
                {
                    return GetFrequency(this.m_ParticleSystem);
                }
                set
                {
                    SetFrequency(this.m_ParticleSystem, value);
                }
            }
            public bool damping
            {
                get
                {
                    return GetDamping(this.m_ParticleSystem);
                }
                set
                {
                    SetDamping(this.m_ParticleSystem, value);
                }
            }
            public int octaveCount
            {
                get
                {
                    return GetOctaveCount(this.m_ParticleSystem);
                }
                set
                {
                    SetOctaveCount(this.m_ParticleSystem, value);
                }
            }
            public float octaveMultiplier
            {
                get
                {
                    return GetOctaveMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetOctaveMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float octaveScale
            {
                get
                {
                    return GetOctaveScale(this.m_ParticleSystem);
                }
                set
                {
                    SetOctaveScale(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemNoiseQuality quality
            {
                get
                {
                    return (ParticleSystemNoiseQuality) GetQuality(this.m_ParticleSystem);
                }
                set
                {
                    SetQuality(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystem.MinMaxCurve scrollSpeed
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetScrollSpeed(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetScrollSpeed(this.m_ParticleSystem, ref value);
                }
            }
            public float scrollSpeedMultiplier
            {
                get
                {
                    return GetScrollSpeedMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetScrollSpeedMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool remapEnabled
            {
                get
                {
                    return GetRemapEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetRemapEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve remap
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRemapX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRemapX(this.m_ParticleSystem, ref value);
                }
            }
            public float remapMultiplier
            {
                get
                {
                    return GetRemapXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRemapXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve remapX
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRemapX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRemapX(this.m_ParticleSystem, ref value);
                }
            }
            public float remapXMultiplier
            {
                get
                {
                    return GetRemapXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRemapXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve remapY
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRemapY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRemapY(this.m_ParticleSystem, ref value);
                }
            }
            public float remapYMultiplier
            {
                get
                {
                    return GetRemapYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRemapYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve remapZ
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetRemapZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetRemapZ(this.m_ParticleSystem, ref value);
                }
            }
            public float remapZMultiplier
            {
                get
                {
                    return GetRemapZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetRemapZMultiplier(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSeparateAxes(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSeparateAxes(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStrengthX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStrengthX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStrengthY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStrengthY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStrengthZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStrengthZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStrengthXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStrengthXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStrengthYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStrengthYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStrengthZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStrengthZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetFrequency(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetFrequency(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDamping(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetDamping(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetOctaveCount(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetOctaveCount(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetOctaveMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetOctaveMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetOctaveScale(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetOctaveScale(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetQuality(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetQuality(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetScrollSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetScrollSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetScrollSpeedMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetScrollSpeedMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetRemapEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetRemapX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetRemapY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetRemapZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRemapXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRemapYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRemapZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRemapZMultiplier(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential), RequiredByNativeCode("particleSystemParticle", Optional=true)]
        public struct Particle
        {
            private Vector3 m_Position;
            private Vector3 m_Velocity;
            private Vector3 m_AnimatedVelocity;
            private Vector3 m_InitialVelocity;
            private Vector3 m_AxisOfRotation;
            private Vector3 m_Rotation;
            private Vector3 m_AngularVelocity;
            private Vector3 m_StartSize;
            private Color32 m_StartColor;
            private uint m_RandomSeed;
            private float m_Lifetime;
            private float m_StartLifetime;
            private float m_EmitAccumulator0;
            private float m_EmitAccumulator1;
            public Vector3 position
            {
                get
                {
                    return this.m_Position;
                }
                set
                {
                    this.m_Position = value;
                }
            }
            public Vector3 velocity
            {
                get
                {
                    return this.m_Velocity;
                }
                set
                {
                    this.m_Velocity = value;
                }
            }
            [Obsolete("Please use Particle.remainingLifetime instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/Particle.remainingLifetime")]
            public float lifetime
            {
                get
                {
                    return this.m_Lifetime;
                }
                set
                {
                    this.m_Lifetime = value;
                }
            }
            public float remainingLifetime
            {
                get
                {
                    return this.m_Lifetime;
                }
                set
                {
                    this.m_Lifetime = value;
                }
            }
            public float startLifetime
            {
                get
                {
                    return this.m_StartLifetime;
                }
                set
                {
                    this.m_StartLifetime = value;
                }
            }
            public float startSize
            {
                get
                {
                    return this.m_StartSize.x;
                }
                set
                {
                    this.m_StartSize = new Vector3(value, value, value);
                }
            }
            public Vector3 startSize3D
            {
                get
                {
                    return this.m_StartSize;
                }
                set
                {
                    this.m_StartSize = value;
                }
            }
            public Vector3 axisOfRotation
            {
                get
                {
                    return this.m_AxisOfRotation;
                }
                set
                {
                    this.m_AxisOfRotation = value;
                }
            }
            public float rotation
            {
                get
                {
                    return (this.m_Rotation.z * 57.29578f);
                }
                set
                {
                    this.m_Rotation = new Vector3(0f, 0f, value * 0.01745329f);
                }
            }
            public Vector3 rotation3D
            {
                get
                {
                    return (Vector3) (this.m_Rotation * 57.29578f);
                }
                set
                {
                    this.m_Rotation = (Vector3) (value * 0.01745329f);
                }
            }
            public float angularVelocity
            {
                get
                {
                    return (this.m_AngularVelocity.z * 57.29578f);
                }
                set
                {
                    this.m_AngularVelocity.z = value * 0.01745329f;
                }
            }
            public Vector3 angularVelocity3D
            {
                get
                {
                    return (Vector3) (this.m_AngularVelocity * 57.29578f);
                }
                set
                {
                    this.m_AngularVelocity = (Vector3) (value * 0.01745329f);
                }
            }
            public Color32 startColor
            {
                get
                {
                    return this.m_StartColor;
                }
                set
                {
                    this.m_StartColor = value;
                }
            }
            [Obsolete("randomValue property is deprecated. Use randomSeed instead to control random behavior of particles.")]
            public float randomValue
            {
                get
                {
                    return BitConverter.ToSingle(BitConverter.GetBytes(this.m_RandomSeed), 0);
                }
                set
                {
                    this.m_RandomSeed = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
                }
            }
            public uint randomSeed
            {
                get
                {
                    return this.m_RandomSeed;
                }
                set
                {
                    this.m_RandomSeed = value;
                }
            }
            public float GetCurrentSize(ParticleSystem system)
            {
                return GetCurrentSize(system, ref this);
            }

            public Vector3 GetCurrentSize3D(ParticleSystem system)
            {
                return GetCurrentSize3D(system, ref this);
            }

            public Color32 GetCurrentColor(ParticleSystem system)
            {
                return GetCurrentColor(system, ref this);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetCurrentSize(ParticleSystem system, ref ParticleSystem.Particle particle);
            private static Vector3 GetCurrentSize3D(ParticleSystem system, ref ParticleSystem.Particle particle)
            {
                Vector3 vector;
                INTERNAL_CALL_GetCurrentSize3D(system, ref particle, out vector);
                return vector;
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_GetCurrentSize3D(ParticleSystem system, ref ParticleSystem.Particle particle, out Vector3 value);
            private static Color32 GetCurrentColor(ParticleSystem system, ref ParticleSystem.Particle particle)
            {
                Color32 color;
                INTERNAL_CALL_GetCurrentColor(system, ref particle, out color);
                return color;
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_GetCurrentColor(ParticleSystem system, ref ParticleSystem.Particle particle, out Color32 value);
            [Obsolete("size property is deprecated. Use startSize or GetCurrentSize() instead.")]
            public float size
            {
                get
                {
                    return this.m_StartSize.x;
                }
                set
                {
                    this.m_StartSize = new Vector3(value, value, value);
                }
            }
            [Obsolete("color property is deprecated. Use startColor or GetCurrentColor() instead.")]
            public Color32 color
            {
                get
                {
                    return this.m_StartColor;
                }
                set
                {
                    this.m_StartColor = value;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RotationBySpeedModule
        {
            private ParticleSystem m_ParticleSystem;
            internal RotationBySpeedModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve x
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float xMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve y
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public float yMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve z
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float zMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool separateAxes
            {
                get
                {
                    return GetSeparateAxes(this.m_ParticleSystem);
                }
                set
                {
                    SetSeparateAxes(this.m_ParticleSystem, value);
                }
            }
            public Vector2 range
            {
                get
                {
                    return GetRange(this.m_ParticleSystem);
                }
                set
                {
                    SetRange(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSeparateAxes(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSeparateAxes(ParticleSystem system);
            private static void SetRange(ParticleSystem system, Vector2 value)
            {
                INTERNAL_CALL_SetRange(system, ref value);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);
            private static Vector2 GetRange(ParticleSystem system)
            {
                Vector2 vector;
                INTERNAL_CALL_GetRange(system, out vector);
                return vector;
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RotationOverLifetimeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal RotationOverLifetimeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve x
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float xMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve y
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public float yMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve z
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float zMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool separateAxes
            {
                get
                {
                    return GetSeparateAxes(this.m_ParticleSystem);
                }
                set
                {
                    SetSeparateAxes(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSeparateAxes(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSeparateAxes(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ShapeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal ShapeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemShapeType shapeType
            {
                get
                {
                    return (ParticleSystemShapeType) GetShapeType(this.m_ParticleSystem);
                }
                set
                {
                    SetShapeType(this.m_ParticleSystem, (int) value);
                }
            }
            public float randomDirectionAmount
            {
                get
                {
                    return GetRandomDirectionAmount(this.m_ParticleSystem);
                }
                set
                {
                    SetRandomDirectionAmount(this.m_ParticleSystem, value);
                }
            }
            public float sphericalDirectionAmount
            {
                get
                {
                    return GetSphericalDirectionAmount(this.m_ParticleSystem);
                }
                set
                {
                    SetSphericalDirectionAmount(this.m_ParticleSystem, value);
                }
            }
            public bool alignToDirection
            {
                get
                {
                    return GetAlignToDirection(this.m_ParticleSystem);
                }
                set
                {
                    SetAlignToDirection(this.m_ParticleSystem, value);
                }
            }
            public float radius
            {
                get
                {
                    return GetRadius(this.m_ParticleSystem);
                }
                set
                {
                    SetRadius(this.m_ParticleSystem, value);
                }
            }
            public float angle
            {
                get
                {
                    return GetAngle(this.m_ParticleSystem);
                }
                set
                {
                    SetAngle(this.m_ParticleSystem, value);
                }
            }
            public float length
            {
                get
                {
                    return GetLength(this.m_ParticleSystem);
                }
                set
                {
                    SetLength(this.m_ParticleSystem, value);
                }
            }
            public Vector3 box
            {
                get
                {
                    return GetBox(this.m_ParticleSystem);
                }
                set
                {
                    SetBox(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemMeshShapeType meshShapeType
            {
                get
                {
                    return (ParticleSystemMeshShapeType) GetMeshShapeType(this.m_ParticleSystem);
                }
                set
                {
                    SetMeshShapeType(this.m_ParticleSystem, (int) value);
                }
            }
            public Mesh mesh
            {
                get
                {
                    return GetMesh(this.m_ParticleSystem);
                }
                set
                {
                    SetMesh(this.m_ParticleSystem, value);
                }
            }
            public MeshRenderer meshRenderer
            {
                get
                {
                    return GetMeshRenderer(this.m_ParticleSystem);
                }
                set
                {
                    SetMeshRenderer(this.m_ParticleSystem, value);
                }
            }
            public SkinnedMeshRenderer skinnedMeshRenderer
            {
                get
                {
                    return GetSkinnedMeshRenderer(this.m_ParticleSystem);
                }
                set
                {
                    SetSkinnedMeshRenderer(this.m_ParticleSystem, value);
                }
            }
            public bool useMeshMaterialIndex
            {
                get
                {
                    return GetUseMeshMaterialIndex(this.m_ParticleSystem);
                }
                set
                {
                    SetUseMeshMaterialIndex(this.m_ParticleSystem, value);
                }
            }
            public int meshMaterialIndex
            {
                get
                {
                    return GetMeshMaterialIndex(this.m_ParticleSystem);
                }
                set
                {
                    SetMeshMaterialIndex(this.m_ParticleSystem, value);
                }
            }
            public bool useMeshColors
            {
                get
                {
                    return GetUseMeshColors(this.m_ParticleSystem);
                }
                set
                {
                    SetUseMeshColors(this.m_ParticleSystem, value);
                }
            }
            public float normalOffset
            {
                get
                {
                    return GetNormalOffset(this.m_ParticleSystem);
                }
                set
                {
                    SetNormalOffset(this.m_ParticleSystem, value);
                }
            }
            public float meshScale
            {
                get
                {
                    return GetMeshScale(this.m_ParticleSystem);
                }
                set
                {
                    SetMeshScale(this.m_ParticleSystem, value);
                }
            }
            public float arc
            {
                get
                {
                    return GetArc(this.m_ParticleSystem);
                }
                set
                {
                    SetArc(this.m_ParticleSystem, value);
                }
            }
            [Obsolete("randomDirection property is deprecated. Use randomDirectionAmount instead.")]
            public bool randomDirection
            {
                get
                {
                    return (GetRandomDirectionAmount(this.m_ParticleSystem) >= 0.5f);
                }
                set
                {
                    SetRandomDirectionAmount(this.m_ParticleSystem, 1f);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetShapeType(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetShapeType(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRandomDirectionAmount(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRandomDirectionAmount(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSphericalDirectionAmount(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetSphericalDirectionAmount(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetAlignToDirection(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetAlignToDirection(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRadius(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRadius(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetAngle(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetAngle(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLength(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetLength(ParticleSystem system);
            private static void SetBox(ParticleSystem system, Vector3 value)
            {
                INTERNAL_CALL_SetBox(system, ref value);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_SetBox(ParticleSystem system, ref Vector3 value);
            private static Vector3 GetBox(ParticleSystem system)
            {
                Vector3 vector;
                INTERNAL_CALL_GetBox(system, out vector);
                return vector;
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_GetBox(ParticleSystem system, out Vector3 value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMeshShapeType(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMeshShapeType(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMesh(ParticleSystem system, Mesh value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern Mesh GetMesh(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMeshRenderer(ParticleSystem system, MeshRenderer value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern MeshRenderer GetMeshRenderer(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSkinnedMeshRenderer(ParticleSystem system, SkinnedMeshRenderer value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern SkinnedMeshRenderer GetSkinnedMeshRenderer(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUseMeshMaterialIndex(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetUseMeshMaterialIndex(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMeshMaterialIndex(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMeshMaterialIndex(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUseMeshColors(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetUseMeshColors(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetNormalOffset(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetNormalOffset(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMeshScale(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetMeshScale(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetArc(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetArc(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SizeBySpeedModule
        {
            private ParticleSystem m_ParticleSystem;
            internal SizeBySpeedModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve size
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float sizeMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve x
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float xMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve y
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public float yMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve z
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float zMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool separateAxes
            {
                get
                {
                    return GetSeparateAxes(this.m_ParticleSystem);
                }
                set
                {
                    SetSeparateAxes(this.m_ParticleSystem, value);
                }
            }
            public Vector2 range
            {
                get
                {
                    return GetRange(this.m_ParticleSystem);
                }
                set
                {
                    SetRange(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSeparateAxes(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSeparateAxes(ParticleSystem system);
            private static void SetRange(ParticleSystem system, Vector2 value)
            {
                INTERNAL_CALL_SetRange(system, ref value);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);
            private static Vector2 GetRange(ParticleSystem system)
            {
                Vector2 vector;
                INTERNAL_CALL_GetRange(system, out vector);
                return vector;
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SizeOverLifetimeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal SizeOverLifetimeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve size
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float sizeMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve x
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public float xMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve y
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public float yMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve z
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float zMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public bool separateAxes
            {
                get
                {
                    return GetSeparateAxes(this.m_ParticleSystem);
                }
                set
                {
                    SetSeparateAxes(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSeparateAxes(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSeparateAxes(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SubEmittersModule
        {
            private ParticleSystem m_ParticleSystem;
            internal SubEmittersModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public int subEmittersCount
            {
                get
                {
                    return GetSubEmittersCount(this.m_ParticleSystem);
                }
            }
            public void AddSubEmitter(ParticleSystem subEmitter, ParticleSystemSubEmitterType type, ParticleSystemSubEmitterProperties properties)
            {
                AddSubEmitter(this.m_ParticleSystem, subEmitter, (int) type, (int) properties);
            }

            public void RemoveSubEmitter(int index)
            {
                RemoveSubEmitter(this.m_ParticleSystem, index);
            }

            public void SetSubEmitterSystem(int index, ParticleSystem subEmitter)
            {
                SetSubEmitterSystem(this.m_ParticleSystem, index, subEmitter);
            }

            public void SetSubEmitterType(int index, ParticleSystemSubEmitterType type)
            {
                SetSubEmitterType(this.m_ParticleSystem, index, (int) type);
            }

            public void SetSubEmitterProperties(int index, ParticleSystemSubEmitterProperties properties)
            {
                SetSubEmitterProperties(this.m_ParticleSystem, index, (int) properties);
            }

            public ParticleSystem GetSubEmitterSystem(int index)
            {
                return GetSubEmitterSystem(this.m_ParticleSystem, index);
            }

            public ParticleSystemSubEmitterType GetSubEmitterType(int index)
            {
                return (ParticleSystemSubEmitterType) GetSubEmitterType(this.m_ParticleSystem, index);
            }

            public ParticleSystemSubEmitterProperties GetSubEmitterProperties(int index)
            {
                return (ParticleSystemSubEmitterProperties) GetSubEmitterProperties(this.m_ParticleSystem, index);
            }

            [Obsolete("birth0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
            public ParticleSystem birth0
            {
                get
                {
                    return GetBirth(this.m_ParticleSystem, 0);
                }
                set
                {
                    SetBirth(this.m_ParticleSystem, 0, value);
                }
            }
            [Obsolete("birth1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
            public ParticleSystem birth1
            {
                get
                {
                    return GetBirth(this.m_ParticleSystem, 1);
                }
                set
                {
                    SetBirth(this.m_ParticleSystem, 1, value);
                }
            }
            [Obsolete("collision0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
            public ParticleSystem collision0
            {
                get
                {
                    return GetCollision(this.m_ParticleSystem, 0);
                }
                set
                {
                    SetCollision(this.m_ParticleSystem, 0, value);
                }
            }
            [Obsolete("collision1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
            public ParticleSystem collision1
            {
                get
                {
                    return GetCollision(this.m_ParticleSystem, 1);
                }
                set
                {
                    SetCollision(this.m_ParticleSystem, 1, value);
                }
            }
            [Obsolete("death0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
            public ParticleSystem death0
            {
                get
                {
                    return GetDeath(this.m_ParticleSystem, 0);
                }
                set
                {
                    SetDeath(this.m_ParticleSystem, 0, value);
                }
            }
            [Obsolete("death1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
            public ParticleSystem death1
            {
                get
                {
                    return GetDeath(this.m_ParticleSystem, 1);
                }
                set
                {
                    SetDeath(this.m_ParticleSystem, 1, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetSubEmittersCount(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetBirth(ParticleSystem system, int index, ParticleSystem value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern ParticleSystem GetBirth(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCollision(ParticleSystem system, int index, ParticleSystem value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern ParticleSystem GetCollision(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDeath(ParticleSystem system, int index, ParticleSystem value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern ParticleSystem GetDeath(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void AddSubEmitter(ParticleSystem system, ParticleSystem subEmitter, int type, int properties);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void RemoveSubEmitter(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSubEmitterSystem(ParticleSystem system, int index, ParticleSystem subEmitter);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSubEmitterType(ParticleSystem system, int index, int type);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSubEmitterProperties(ParticleSystem system, int index, int properties);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern ParticleSystem GetSubEmitterSystem(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetSubEmitterType(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetSubEmitterProperties(ParticleSystem system, int index);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TextureSheetAnimationModule
        {
            private ParticleSystem m_ParticleSystem;
            internal TextureSheetAnimationModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public int numTilesX
            {
                get
                {
                    return GetNumTilesX(this.m_ParticleSystem);
                }
                set
                {
                    SetNumTilesX(this.m_ParticleSystem, value);
                }
            }
            public int numTilesY
            {
                get
                {
                    return GetNumTilesY(this.m_ParticleSystem);
                }
                set
                {
                    SetNumTilesY(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemAnimationType animation
            {
                get
                {
                    return (ParticleSystemAnimationType) GetAnimationType(this.m_ParticleSystem);
                }
                set
                {
                    SetAnimationType(this.m_ParticleSystem, (int) value);
                }
            }
            public bool useRandomRow
            {
                get
                {
                    return GetUseRandomRow(this.m_ParticleSystem);
                }
                set
                {
                    SetUseRandomRow(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve frameOverTime
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetFrameOverTime(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetFrameOverTime(this.m_ParticleSystem, ref value);
                }
            }
            public float frameOverTimeMultiplier
            {
                get
                {
                    return GetFrameOverTimeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetFrameOverTimeMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve startFrame
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetStartFrame(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetStartFrame(this.m_ParticleSystem, ref value);
                }
            }
            public float startFrameMultiplier
            {
                get
                {
                    return GetStartFrameMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetStartFrameMultiplier(this.m_ParticleSystem, value);
                }
            }
            public int cycleCount
            {
                get
                {
                    return GetCycleCount(this.m_ParticleSystem);
                }
                set
                {
                    SetCycleCount(this.m_ParticleSystem, value);
                }
            }
            public int rowIndex
            {
                get
                {
                    return GetRowIndex(this.m_ParticleSystem);
                }
                set
                {
                    SetRowIndex(this.m_ParticleSystem, value);
                }
            }
            public UVChannelFlags uvChannelMask
            {
                get
                {
                    return (UVChannelFlags) GetUVChannelMask(this.m_ParticleSystem);
                }
                set
                {
                    SetUVChannelMask(this.m_ParticleSystem, (int) value);
                }
            }
            public float flipU
            {
                get
                {
                    return GetFlipU(this.m_ParticleSystem);
                }
                set
                {
                    SetFlipU(this.m_ParticleSystem, value);
                }
            }
            public float flipV
            {
                get
                {
                    return GetFlipV(this.m_ParticleSystem);
                }
                set
                {
                    SetFlipV(this.m_ParticleSystem, value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetNumTilesX(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetNumTilesX(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetNumTilesY(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetNumTilesY(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetAnimationType(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetAnimationType(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUseRandomRow(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetUseRandomRow(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetFrameOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetFrameOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetFrameOverTimeMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetFrameOverTimeMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartFrame(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetStartFrame(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetStartFrameMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetStartFrameMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCycleCount(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetCycleCount(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRowIndex(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetRowIndex(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetUVChannelMask(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetUVChannelMask(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetFlipU(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetFlipU(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetFlipV(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetFlipV(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TrailModule
        {
            private ParticleSystem m_ParticleSystem;
            internal TrailModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public float ratio
            {
                get
                {
                    return GetRatio(this.m_ParticleSystem);
                }
                set
                {
                    SetRatio(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve lifetime
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetLifetime(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetLifetime(this.m_ParticleSystem, ref value);
                }
            }
            public float lifetimeMultiplier
            {
                get
                {
                    return GetLifetimeMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetLifetimeMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float minVertexDistance
            {
                get
                {
                    return GetMinVertexDistance(this.m_ParticleSystem);
                }
                set
                {
                    SetMinVertexDistance(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemTrailTextureMode textureMode
            {
                get
                {
                    return (ParticleSystemTrailTextureMode) GetTextureMode(this.m_ParticleSystem);
                }
                set
                {
                    SetTextureMode(this.m_ParticleSystem, (float) value);
                }
            }
            public bool worldSpace
            {
                get
                {
                    return GetWorldSpace(this.m_ParticleSystem);
                }
                set
                {
                    SetWorldSpace(this.m_ParticleSystem, value);
                }
            }
            public bool dieWithParticles
            {
                get
                {
                    return GetDieWithParticles(this.m_ParticleSystem);
                }
                set
                {
                    SetDieWithParticles(this.m_ParticleSystem, value);
                }
            }
            public bool sizeAffectsWidth
            {
                get
                {
                    return GetSizeAffectsWidth(this.m_ParticleSystem);
                }
                set
                {
                    SetSizeAffectsWidth(this.m_ParticleSystem, value);
                }
            }
            public bool sizeAffectsLifetime
            {
                get
                {
                    return GetSizeAffectsLifetime(this.m_ParticleSystem);
                }
                set
                {
                    SetSizeAffectsLifetime(this.m_ParticleSystem, value);
                }
            }
            public bool inheritParticleColor
            {
                get
                {
                    return GetInheritParticleColor(this.m_ParticleSystem);
                }
                set
                {
                    SetInheritParticleColor(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxGradient colorOverLifetime
            {
                get
                {
                    ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient();
                    GetColorOverLifetime(this.m_ParticleSystem, ref gradient);
                    return gradient;
                }
                set
                {
                    SetColorOverLifetime(this.m_ParticleSystem, ref value);
                }
            }
            public ParticleSystem.MinMaxCurve widthOverTrail
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetWidthOverTrail(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetWidthOverTrail(this.m_ParticleSystem, ref value);
                }
            }
            public float widthOverTrailMultiplier
            {
                get
                {
                    return GetWidthOverTrailMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetWidthOverTrailMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxGradient colorOverTrail
            {
                get
                {
                    ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient();
                    GetColorOverTrail(this.m_ParticleSystem, ref gradient);
                    return gradient;
                }
                set
                {
                    SetColorOverTrail(this.m_ParticleSystem, ref value);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRatio(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRatio(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetLifetimeMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetLifetimeMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetMinVertexDistance(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetMinVertexDistance(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetTextureMode(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetTextureMode(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWorldSpace(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetWorldSpace(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetDieWithParticles(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetDieWithParticles(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSizeAffectsWidth(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSizeAffectsWidth(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetSizeAffectsLifetime(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetSizeAffectsLifetime(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetInheritParticleColor(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetInheritParticleColor(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetColorOverLifetime(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetColorOverLifetime(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWidthOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetWidthOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWidthOverTrailMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetWidthOverTrailMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetColorOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetColorOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TriggerModule
        {
            private ParticleSystem m_ParticleSystem;
            internal TriggerModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemOverlapAction inside
            {
                get
                {
                    return (ParticleSystemOverlapAction) GetInside(this.m_ParticleSystem);
                }
                set
                {
                    SetInside(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystemOverlapAction outside
            {
                get
                {
                    return (ParticleSystemOverlapAction) GetOutside(this.m_ParticleSystem);
                }
                set
                {
                    SetOutside(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystemOverlapAction enter
            {
                get
                {
                    return (ParticleSystemOverlapAction) GetEnter(this.m_ParticleSystem);
                }
                set
                {
                    SetEnter(this.m_ParticleSystem, (int) value);
                }
            }
            public ParticleSystemOverlapAction exit
            {
                get
                {
                    return (ParticleSystemOverlapAction) GetExit(this.m_ParticleSystem);
                }
                set
                {
                    SetExit(this.m_ParticleSystem, (int) value);
                }
            }
            public float radiusScale
            {
                get
                {
                    return GetRadiusScale(this.m_ParticleSystem);
                }
                set
                {
                    SetRadiusScale(this.m_ParticleSystem, value);
                }
            }
            public void SetCollider(int index, Component collider)
            {
                SetCollider(this.m_ParticleSystem, index, collider);
            }

            public Component GetCollider(int index)
            {
                return GetCollider(this.m_ParticleSystem, index);
            }

            public int maxColliderCount
            {
                get
                {
                    return GetMaxColliderCount(this.m_ParticleSystem);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetInside(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetInside(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetOutside(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetOutside(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnter(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetEnter(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetExit(ParticleSystem system, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetExit(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetRadiusScale(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetRadiusScale(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetCollider(ParticleSystem system, int index, Component collider);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern Component GetCollider(ParticleSystem system, int index);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern int GetMaxColliderCount(ParticleSystem system);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct VelocityOverLifetimeModule
        {
            private ParticleSystem m_ParticleSystem;
            internal VelocityOverLifetimeModule(ParticleSystem particleSystem)
            {
                this.m_ParticleSystem = particleSystem;
            }

            public bool enabled
            {
                get
                {
                    return GetEnabled(this.m_ParticleSystem);
                }
                set
                {
                    SetEnabled(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystem.MinMaxCurve x
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetX(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetX(this.m_ParticleSystem, ref value);
                }
            }
            public ParticleSystem.MinMaxCurve y
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetY(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetY(this.m_ParticleSystem, ref value);
                }
            }
            public ParticleSystem.MinMaxCurve z
            {
                get
                {
                    ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
                    GetZ(this.m_ParticleSystem, ref curve);
                    return curve;
                }
                set
                {
                    SetZ(this.m_ParticleSystem, ref value);
                }
            }
            public float xMultiplier
            {
                get
                {
                    return GetXMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetXMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float yMultiplier
            {
                get
                {
                    return GetYMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetYMultiplier(this.m_ParticleSystem, value);
                }
            }
            public float zMultiplier
            {
                get
                {
                    return GetZMultiplier(this.m_ParticleSystem);
                }
                set
                {
                    SetZMultiplier(this.m_ParticleSystem, value);
                }
            }
            public ParticleSystemSimulationSpace space
            {
                get
                {
                    return (!GetWorldSpace(this.m_ParticleSystem) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World);
                }
                set
                {
                    SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
                }
            }
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetEnabled(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetEnabled(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetXMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetXMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetYMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetYMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetZMultiplier(ParticleSystem system, float value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern float GetZMultiplier(ParticleSystem system);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWorldSpace(ParticleSystem system, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool GetWorldSpace(ParticleSystem system);
        }
    }
}

