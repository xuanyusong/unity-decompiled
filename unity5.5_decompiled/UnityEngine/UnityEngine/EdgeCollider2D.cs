﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class EdgeCollider2D : Collider2D
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void Reset();

        public int edgeCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int pointCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Vector2[] points { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

