﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class FrictionJoint2D : AnchoredJoint2D
    {
        public float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float maxTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

