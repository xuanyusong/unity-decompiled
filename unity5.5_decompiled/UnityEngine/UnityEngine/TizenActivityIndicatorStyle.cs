﻿namespace UnityEngine
{
    using System;

    public enum TizenActivityIndicatorStyle
    {
        DontShow = -1,
        InversedLarge = 1,
        InversedSmall = 3,
        Large = 0,
        Small = 2
    }
}

