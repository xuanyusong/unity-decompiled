﻿namespace UnityEngine.Windows.Speech
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class KeywordRecognizer : PhraseRecognizer
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private IEnumerable<string> <Keywords>k__BackingField;

        public KeywordRecognizer(string[] keywords) : this(keywords, ConfidenceLevel.Medium)
        {
        }

        public KeywordRecognizer(string[] keywords, ConfidenceLevel minimumConfidence)
        {
            if (keywords == null)
            {
                throw new ArgumentNullException("keywords");
            }
            if (keywords.Length == 0)
            {
                throw new ArgumentException("At least one keyword must be specified.", "keywords");
            }
            int length = keywords.Length;
            for (int i = 0; i < length; i++)
            {
                if (keywords[i] == null)
                {
                    throw new ArgumentNullException(string.Format("Keyword at index {0} is null.", i));
                }
            }
            this.Keywords = keywords;
            base.m_Recognizer = base.CreateFromKeywords(keywords, minimumConfidence);
        }

        public IEnumerable<string> Keywords { get; private set; }
    }
}

