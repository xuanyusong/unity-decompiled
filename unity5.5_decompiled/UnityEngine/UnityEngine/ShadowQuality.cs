﻿namespace UnityEngine
{
    using System;

    public enum ShadowQuality
    {
        Disable,
        HardOnly,
        All
    }
}

