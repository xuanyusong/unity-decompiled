﻿namespace UnityEngine
{
    using System;

    public enum ParticleSystemTrailTextureMode
    {
        Stretch,
        Tile
    }
}

