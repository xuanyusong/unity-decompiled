﻿namespace UnityEngine
{
    using System;

    public enum ParticleSystemStopBehavior
    {
        StopEmittingAndClear,
        StopEmitting
    }
}

