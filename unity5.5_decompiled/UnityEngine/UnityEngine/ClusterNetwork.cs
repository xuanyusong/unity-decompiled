﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ClusterNetwork
    {
        public static bool isDisconnected { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool isMasterOfCluster { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int nodeIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

