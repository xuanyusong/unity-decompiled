﻿namespace UnityEngine.Rendering
{
    using System;
    using UnityEngine.Scripting;

    [UsedByNativeCode]
    public enum GraphicsDeviceType
    {
        Direct3D11 = 2,
        Direct3D12 = 0x12,
        Direct3D9 = 1,
        Metal = 0x10,
        N3DS = 0x13,
        Null = 4,
        [Obsolete("OpenGL2 is no longer supported in Unity 5.5+")]
        OpenGL2 = 0,
        OpenGLCore = 0x11,
        OpenGLES2 = 8,
        OpenGLES3 = 11,
        [Obsolete("PS3 is no longer supported in Unity 5.5+")]
        PlayStation3 = 3,
        PlayStation4 = 13,
        PlayStationMobile = 15,
        PlayStationVita = 12,
        Vulkan = 0x15,
        [Obsolete("Xbox360 is no longer supported in Unity 5.5+")]
        Xbox360 = 6,
        XboxOne = 14
    }
}

