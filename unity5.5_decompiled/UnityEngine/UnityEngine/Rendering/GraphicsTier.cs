﻿namespace UnityEngine.Rendering
{
    using System;

    public enum GraphicsTier
    {
        Tier1,
        Tier2,
        Tier3
    }
}

