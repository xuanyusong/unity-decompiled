﻿namespace UnityEngine.Rendering
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SplashScreen
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Begin();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Draw();

        public static bool isFinished { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

