﻿namespace UnityEngine.Rendering
{
    using System;

    public enum TextureDimension
    {
        Any = 1,
        Cube = 4,
        CubeArray = 6,
        None = 0,
        Tex2D = 2,
        Tex2DArray = 5,
        Tex3D = 3,
        Unknown = -1
    }
}

