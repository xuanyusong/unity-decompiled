﻿namespace UnityEngine
{
    using System;

    public enum ParticleSystemSubEmitterType
    {
        Birth,
        Collision,
        Death
    }
}

