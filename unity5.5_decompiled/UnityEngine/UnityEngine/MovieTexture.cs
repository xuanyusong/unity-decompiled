﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MovieTexture : Texture
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Pause(MovieTexture self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Play(MovieTexture self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_Stop(MovieTexture self);
        public void Pause()
        {
            INTERNAL_CALL_Pause(this);
        }

        public void Play()
        {
            INTERNAL_CALL_Play(this);
        }

        public void Stop()
        {
            INTERNAL_CALL_Stop(this);
        }

        public AudioClip audioClip { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float duration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isPlaying { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isReadyToPlay { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool loop { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

