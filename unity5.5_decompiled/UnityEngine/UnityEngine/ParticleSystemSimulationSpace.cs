﻿namespace UnityEngine
{
    using System;

    public enum ParticleSystemSimulationSpace
    {
        Local,
        World,
        Custom
    }
}

