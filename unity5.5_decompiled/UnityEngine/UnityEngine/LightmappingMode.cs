﻿namespace UnityEngine
{
    using System;

    public enum LightmappingMode
    {
        Baked = 2,
        Mixed = 1,
        Realtime = 4
    }
}

