﻿namespace UnityEngine.Diagnostics
{
    using System;
    using System.Runtime.CompilerServices;

    public static class PlayerConnection
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SendFile(string remoteFilePath, byte[] data);

        public static bool connected { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

