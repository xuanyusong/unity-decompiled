﻿namespace UnityEngine.Tizen
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class Window
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_get_evasGL(out IntPtr value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_get_windowHandle(out IntPtr value);

        public static IntPtr evasGL
        {
            get
            {
                IntPtr ptr;
                INTERNAL_get_evasGL(out ptr);
                return ptr;
            }
        }

        public static IntPtr windowHandle
        {
            get
            {
                IntPtr ptr;
                INTERNAL_get_windowHandle(out ptr);
                return ptr;
            }
        }
    }
}

