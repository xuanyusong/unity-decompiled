﻿namespace UnityEngine.VR.WSA
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class HolographicSettings
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ActivateLatentFramePresentation(bool activated);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_InternalSetFocusPointForFrame(ref Vector3 position);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_InternalSetFocusPointForFrameWithNormal(ref Vector3 position, ref Vector3 normal);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_InternalSetFocusPointForFrameWithNormalVelocity(ref Vector3 position, ref Vector3 normal, ref Vector3 velocity);
        private static void InternalSetFocusPointForFrame(Vector3 position)
        {
            INTERNAL_CALL_InternalSetFocusPointForFrame(ref position);
        }

        private static void InternalSetFocusPointForFrameWithNormal(Vector3 position, Vector3 normal)
        {
            INTERNAL_CALL_InternalSetFocusPointForFrameWithNormal(ref position, ref normal);
        }

        private static void InternalSetFocusPointForFrameWithNormalVelocity(Vector3 position, Vector3 normal, Vector3 velocity)
        {
            INTERNAL_CALL_InternalSetFocusPointForFrameWithNormalVelocity(ref position, ref normal, ref velocity);
        }

        public static void SetFocusPointForFrame(Vector3 position)
        {
            InternalSetFocusPointForFrame(position);
        }

        public static void SetFocusPointForFrame(Vector3 position, Vector3 normal)
        {
            InternalSetFocusPointForFrameWithNormal(position, normal);
        }

        public static void SetFocusPointForFrame(Vector3 position, Vector3 normal, Vector3 velocity)
        {
        }

        public static bool IsLatentFramePresentation { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

