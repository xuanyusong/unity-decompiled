﻿namespace UnityEngine.VR.WSA
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using UnityEngine;
    using UnityEngine.Scripting;

    public sealed class WorldAnchor : Component
    {
        public event OnTrackingChangedDelegate OnTrackingChanged;

        private WorldAnchor()
        {
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool INTERNAL_CALL_IsLocated_Internal(WorldAnchor self);
        [RequiredByNativeCode]
        private static void Internal_TriggerEventOnTrackingLost(WorldAnchor self, bool located)
        {
            if ((self != null) && (self.OnTrackingChanged != null))
            {
                self.OnTrackingChanged(self, located);
            }
        }

        private bool IsLocated_Internal()
        {
            return INTERNAL_CALL_IsLocated_Internal(this);
        }

        public bool isLocated
        {
            get
            {
                return this.IsLocated_Internal();
            }
        }

        public delegate void OnTrackingChangedDelegate(WorldAnchor self, bool located);
    }
}

