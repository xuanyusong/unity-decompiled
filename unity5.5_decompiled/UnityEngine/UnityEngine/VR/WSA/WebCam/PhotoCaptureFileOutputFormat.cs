﻿namespace UnityEngine.VR.WSA.WebCam
{
    using System;

    public enum PhotoCaptureFileOutputFormat
    {
        PNG,
        JPG
    }
}

