﻿namespace UnityEngine.VR.WSA.WebCam
{
    using System;

    public enum CapturePixelFormat
    {
        BGRA32,
        NV12,
        JPEG,
        PNG
    }
}

