﻿namespace UnityEngine.VR.WSA.WebCam
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class PhotoCaptureFrame : IDisposable
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <dataLength>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <hasLocationData>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private CapturePixelFormat <pixelFormat>k__BackingField;
        private IntPtr m_NativePtr;

        internal PhotoCaptureFrame(IntPtr nativePtr)
        {
        }

        private void Cleanup()
        {
        }

        public void CopyRawImageDataIntoBuffer(List<byte> byteBuffer)
        {
        }

        public void Dispose()
        {
            this.Cleanup();
            GC.SuppressFinalize(this);
        }

        ~PhotoCaptureFrame()
        {
            this.Cleanup();
        }

        public IntPtr GetUnsafePointerToBuffer()
        {
            return IntPtr.Zero;
        }

        public bool TryGetCameraToWorldMatrix(out Matrix4x4 cameraToWorldMatrix)
        {
            cameraToWorldMatrix = Matrix4x4.identity;
            return false;
        }

        public bool TryGetProjectionMatrix(out Matrix4x4 projectionMatrix)
        {
            projectionMatrix = Matrix4x4.identity;
            return false;
        }

        public bool TryGetProjectionMatrix(float nearClipPlane, float farClipPlane, out Matrix4x4 projectionMatrix)
        {
            projectionMatrix = Matrix4x4.identity;
            return false;
        }

        public void UploadImageDataToTexture(Texture2D targetTexture)
        {
        }

        public int dataLength { get; private set; }

        public bool hasLocationData { get; private set; }

        public CapturePixelFormat pixelFormat { get; private set; }
    }
}

