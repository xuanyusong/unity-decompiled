﻿namespace UnityEngine.VR.WSA
{
    using System;

    public enum PositionalLocatorState
    {
        Unavailable,
        OrientationOnly,
        Activating,
        Active,
        Inhibited
    }
}

