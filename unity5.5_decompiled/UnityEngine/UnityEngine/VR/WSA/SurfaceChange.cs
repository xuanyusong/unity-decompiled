﻿namespace UnityEngine.VR.WSA
{
    using System;

    public enum SurfaceChange
    {
        Added,
        Updated,
        Removed
    }
}

