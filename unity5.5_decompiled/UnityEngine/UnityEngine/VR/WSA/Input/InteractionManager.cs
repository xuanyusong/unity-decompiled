﻿namespace UnityEngine.VR.WSA.Input
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    public sealed class InteractionManager
    {
        [CompilerGenerated]
        private static InternalSourceEventHandler <>f__mg$cache0;
        private static InternalSourceEventHandler m_OnSourceEventHandler;

        public static  event SourceEventHandler SourceDetected;

        public static  event SourceEventHandler SourceLost;

        public static  event SourceEventHandler SourcePressed;

        public static  event SourceEventHandler SourceReleased;

        public static  event SourceEventHandler SourceUpdated;

        static InteractionManager()
        {
            if (<>f__mg$cache0 == null)
            {
                <>f__mg$cache0 = new InternalSourceEventHandler(InteractionManager.OnSourceEvent);
            }
            m_OnSourceEventHandler = <>f__mg$cache0;
            Initialize(Marshal.GetFunctionPointerForDelegate(m_OnSourceEventHandler));
        }

        public static InteractionSourceState[] GetCurrentReading()
        {
            InteractionSourceState[] sourceStates = new InteractionSourceState[numSourceStates];
            if (sourceStates.Length > 0)
            {
                GetCurrentReading_Internal(sourceStates);
            }
            return sourceStates;
        }

        public static int GetCurrentReading(InteractionSourceState[] sourceStates)
        {
            if (sourceStates == null)
            {
                throw new ArgumentNullException("sourceStates");
            }
            if (sourceStates.Length > 0)
            {
                return GetCurrentReading_Internal(sourceStates);
            }
            return 0;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern int GetCurrentReading_Internal(InteractionSourceState[] sourceStates);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Initialize(IntPtr internalSourceEventHandler);
        private static void OnSourceEvent(EventType eventType, InteractionSourceState state)
        {
            switch (eventType)
            {
                case EventType.SourceDetected:
                {
                    SourceEventHandler sourceDetected = SourceDetected;
                    if (sourceDetected != null)
                    {
                        sourceDetected(state);
                    }
                    break;
                }
                case EventType.SourceLost:
                {
                    SourceEventHandler sourceLost = SourceLost;
                    if (sourceLost != null)
                    {
                        sourceLost(state);
                    }
                    break;
                }
                case EventType.SourceUpdated:
                {
                    SourceEventHandler sourceUpdated = SourceUpdated;
                    if (sourceUpdated != null)
                    {
                        sourceUpdated(state);
                    }
                    break;
                }
                case EventType.SourcePressed:
                {
                    SourceEventHandler sourcePressed = SourcePressed;
                    if (sourcePressed != null)
                    {
                        sourcePressed(state);
                    }
                    break;
                }
                case EventType.SourceReleased:
                {
                    SourceEventHandler sourceReleased = SourceReleased;
                    if (sourceReleased != null)
                    {
                        sourceReleased(state);
                    }
                    break;
                }
                default:
                    throw new ArgumentException("OnSourceEvent: Invalid EventType");
            }
        }

        public static int numSourceStates { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        private enum EventType
        {
            SourceDetected,
            SourceLost,
            SourceUpdated,
            SourcePressed,
            SourceReleased
        }

        private delegate void InternalSourceEventHandler(InteractionManager.EventType eventType, InteractionSourceState state);

        public delegate void SourceEventHandler(InteractionSourceState state);
    }
}

