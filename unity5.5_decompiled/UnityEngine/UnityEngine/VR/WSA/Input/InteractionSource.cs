﻿namespace UnityEngine.VR.WSA.Input
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine.Scripting;

    [StructLayout(LayoutKind.Sequential), RequiredByNativeCode]
    public struct InteractionSource
    {
        internal uint m_id;
        internal InteractionSourceKind m_kind;
        public uint id
        {
            get
            {
                return this.m_id;
            }
        }
        public InteractionSourceKind kind
        {
            get
            {
                return this.m_kind;
            }
        }
    }
}

