﻿namespace UnityEngine.VR.WSA.Sharing
{
    using System;

    public enum SerializationCompletionReason
    {
        Succeeded,
        NotSupported,
        AccessDenied,
        UnknownError
    }
}

