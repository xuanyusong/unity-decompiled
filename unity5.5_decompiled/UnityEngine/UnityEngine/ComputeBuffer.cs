﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Security;

    public sealed class ComputeBuffer : IDisposable
    {
        internal IntPtr m_Ptr;

        public ComputeBuffer(int count, int stride) : this(count, stride, ComputeBufferType.Default)
        {
        }

        public ComputeBuffer(int count, int stride, ComputeBufferType type)
        {
            if (count <= 0)
            {
                throw new ArgumentException("Attempting to create a zero length compute buffer", "count");
            }
            if (stride < 0)
            {
                throw new ArgumentException("Attempting to create a compute buffer with a negative stride", "stride");
            }
            this.m_Ptr = IntPtr.Zero;
            InitBuffer(this, count, stride, type);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void CopyCount(ComputeBuffer src, ComputeBuffer dst, int dstOffset);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void DestroyBuffer(ComputeBuffer buf);
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                DestroyBuffer(this);
            }
            else if (this.m_Ptr != IntPtr.Zero)
            {
                Debug.LogWarning("GarbageCollector disposing of ComputeBuffer. Please use ComputeBuffer.Release() or .Dispose() to manually release the buffer.");
            }
            this.m_Ptr = IntPtr.Zero;
        }

        ~ComputeBuffer()
        {
            this.Dispose(false);
        }

        [SecuritySafeCritical]
        public void GetData(Array data)
        {
            this.InternalGetData(data, Marshal.SizeOf(data.GetType().GetElementType()));
        }

        public IntPtr GetNativeBufferPtr()
        {
            IntPtr ptr;
            INTERNAL_CALL_GetNativeBufferPtr(this, out ptr);
            return ptr;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void InitBuffer(ComputeBuffer buf, int count, int stride, ComputeBufferType type);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_GetNativeBufferPtr(ComputeBuffer self, out IntPtr value);
        [MethodImpl(MethodImplOptions.InternalCall), SecurityCritical]
        private extern void InternalGetData(Array data, int elemSize);
        [MethodImpl(MethodImplOptions.InternalCall), SecurityCritical]
        private extern void InternalSetData(Array data, int elemSize);
        public void Release()
        {
            this.Dispose();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetCounterValue(uint counterValue);
        [SecuritySafeCritical]
        public void SetData(Array data)
        {
            this.InternalSetData(data, Marshal.SizeOf(data.GetType().GetElementType()));
        }

        public int count { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int stride { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

