﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.Internal;

    public sealed class Cloth : Component
    {
        public void ClearTransformMotion()
        {
            INTERNAL_CALL_ClearTransformMotion(this);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_ClearTransformMotion(Cloth self);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_externalAcceleration(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_randomAcceleration(out Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_externalAcceleration(ref Vector3 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_randomAcceleration(ref Vector3 value);
        [ExcludeFromDocs]
        public void SetEnabledFading(bool enabled)
        {
            float interpolationTime = 0.5f;
            this.SetEnabledFading(enabled, interpolationTime);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetEnabledFading(bool enabled, [DefaultValue("0.5f")] float interpolationTime);

        public float bendingStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public CapsuleCollider[] capsuleColliders { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ClothSkinningCoefficient[] coefficients { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float collisionMassScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float damping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3 externalAcceleration
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_externalAcceleration(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_externalAcceleration(ref value);
            }
        }

        public float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3[] normals { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Vector3 randomAcceleration
        {
            get
            {
                Vector3 vector;
                this.INTERNAL_get_randomAcceleration(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_randomAcceleration(ref value);
            }
        }

        [Obsolete("Deprecated. Cloth.selfCollisions is no longer supported since Unity 5.0.", true)]
        public bool selfCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float sleepThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool solverFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ClothSphereColliderPair[] sphereColliders { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float stretchingStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float useContinuousCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool useGravity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float useVirtualParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector3[] vertices { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float worldAccelerationScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float worldVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

