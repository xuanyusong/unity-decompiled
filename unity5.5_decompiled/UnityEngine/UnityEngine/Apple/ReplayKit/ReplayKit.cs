﻿namespace UnityEngine.Apple.ReplayKit
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Internal;

    public static class ReplayKit
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool Discard();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void HideCameraPreview();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool Preview();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool ShowCameraPreviewAt(float posX, float posY);
        [ExcludeFromDocs]
        public static void StartBroadcasting(BroadcastStatusCallback callback)
        {
            bool enableCamera = false;
            bool enableMicrophone = false;
            StartBroadcasting(callback, enableMicrophone, enableCamera);
        }

        [ExcludeFromDocs]
        public static void StartBroadcasting(BroadcastStatusCallback callback, bool enableMicrophone)
        {
            bool enableCamera = false;
            StartBroadcasting(callback, enableMicrophone, enableCamera);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void StartBroadcasting(BroadcastStatusCallback callback, [DefaultValue("false")] bool enableMicrophone, [DefaultValue("false")] bool enableCamera);
        [ExcludeFromDocs]
        public static bool StartRecording()
        {
            bool enableCamera = false;
            bool enableMicrophone = false;
            return StartRecording(enableMicrophone, enableCamera);
        }

        [ExcludeFromDocs]
        public static bool StartRecording(bool enableMicrophone)
        {
            bool enableCamera = false;
            return StartRecording(enableMicrophone, enableCamera);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool StartRecording([DefaultValue("false")] bool enableMicrophone, [DefaultValue("false")] bool enableCamera);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void StopBroadcasting();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool StopRecording();

        public static bool APIAvailable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool broadcastingAPIAvailable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string broadcastURL { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool cameraEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool isBroadcasting { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool isRecording { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string lastError { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool microphoneEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool recordingAvailable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public delegate void BroadcastStatusCallback(bool hasStarted, string errorMessage);
    }
}

