﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ProceduralTexture : Texture
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern Color32[] GetPixels32(int x, int y, int blockWidth, int blockHeight);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern ProceduralMaterial GetProceduralMaterial();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern ProceduralOutputType GetProceduralOutputType();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern bool HasBeenGenerated();

        public TextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool hasAlpha { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

