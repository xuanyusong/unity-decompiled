﻿namespace UnityEngine.AI
{
    using System;
    using UnityEngine.Scripting.APIUpdating;

    [MovedFrom("UnityEngine")]
    public enum ObstacleAvoidanceType
    {
        NoObstacleAvoidance,
        LowQualityObstacleAvoidance,
        MedQualityObstacleAvoidance,
        GoodQualityObstacleAvoidance,
        HighQualityObstacleAvoidance
    }
}

