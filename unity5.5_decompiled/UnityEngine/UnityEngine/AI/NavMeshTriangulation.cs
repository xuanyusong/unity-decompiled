﻿namespace UnityEngine.AI
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Scripting;
    using UnityEngine.Scripting.APIUpdating;

    [StructLayout(LayoutKind.Sequential), MovedFrom("UnityEngine"), UsedByNativeCode]
    public struct NavMeshTriangulation
    {
        public Vector3[] vertices;
        public int[] indices;
        public int[] areas;
        [Obsolete("Use areas instead.")]
        public int[] layers
        {
            get
            {
                return this.areas;
            }
        }
    }
}

