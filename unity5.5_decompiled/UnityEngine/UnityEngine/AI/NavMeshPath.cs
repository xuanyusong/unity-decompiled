﻿namespace UnityEngine.AI
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Scripting.APIUpdating;

    [StructLayout(LayoutKind.Sequential), MovedFrom("UnityEngine")]
    public sealed class NavMeshPath
    {
        internal IntPtr m_Ptr;
        internal Vector3[] m_corners;
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern NavMeshPath();
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        private extern void DestroyNavMeshPath();
        ~NavMeshPath()
        {
            this.DestroyNavMeshPath();
            this.m_Ptr = IntPtr.Zero;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern int GetCornersNonAlloc(Vector3[] results);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern Vector3[] CalculateCornersInternal();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void ClearCornersInternal();
        public void ClearCorners()
        {
            this.ClearCornersInternal();
            this.m_corners = null;
        }

        private void CalculateCorners()
        {
            if (this.m_corners == null)
            {
                this.m_corners = this.CalculateCornersInternal();
            }
        }

        public Vector3[] corners
        {
            get
            {
                this.CalculateCorners();
                return this.m_corners;
            }
        }
        public NavMeshPathStatus status { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

