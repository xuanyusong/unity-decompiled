﻿namespace UnityEngine
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine.Scripting;

    [StructLayout(LayoutKind.Sequential), UsedByNativeCode]
    public struct ClothSkinningCoefficient
    {
        public float maxDistance;
        public float collisionSphereDistance;
    }
}

