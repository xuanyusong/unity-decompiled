﻿namespace UnityEngine
{
    using System;

    [Flags]
    public enum ParticleSystemSubEmitterProperties
    {
        InheritColor = 1,
        InheritEverything = 7,
        InheritNothing = 0,
        InheritRotation = 4,
        InheritSize = 2
    }
}

