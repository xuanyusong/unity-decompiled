﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Tree : Component
    {
        public ScriptableObject data { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool hasSpeedTreeWind { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

