﻿namespace UnityEngine.WSA
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class Cursor
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetCustomCursor(uint id);
    }
}

