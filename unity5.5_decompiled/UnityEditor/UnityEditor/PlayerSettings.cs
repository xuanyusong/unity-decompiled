﻿namespace UnityEditor
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using UnityEditor.Rendering;
    using UnityEngine;
    using UnityEngine.Internal;
    using UnityEngine.Rendering;
    using UnityEngine.Scripting;

    public sealed class PlayerSettings : UnityEngine.Object
    {
        private static SerializedObject _serializedObject;
        internal static readonly char[] defineSplits = new char[] { ';', ',', ' ', '\0' };

        internal static SerializedProperty FindProperty(string name)
        {
            SerializedProperty property = GetSerializedObject().FindProperty(name);
            if (property == null)
            {
                Debug.LogError("Failed to find:" + name);
            }
            return property;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string GetAdditionalIl2CppArgs();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern int GetArchitecture(BuildTargetGroup targetGroup);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void GetBatchingForPlatform(BuildTarget platform, out int staticBatching, out int dynamicBatching);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool GetCloudServiceEnabled(string serviceKey);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern GraphicsDeviceType[] GetGraphicsAPIs(BuildTarget platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern Texture2D GetIconForPlatformAtSize(string platform, int width, int height);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern int[] GetIconHeightsForPlatform(string platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern Texture2D[] GetIconsForPlatform(string platform);
        public static Texture2D[] GetIconsForTargetGroup(BuildTargetGroup platform)
        {
            Texture2D[] iconsForPlatform = GetIconsForPlatform(GetPlatformName(platform));
            if (iconsForPlatform.Length == 0)
            {
                return new Texture2D[GetIconSizesForTargetGroup(platform).Length];
            }
            return iconsForPlatform;
        }

        public static int[] GetIconSizesForTargetGroup(BuildTargetGroup platform)
        {
            return GetIconWidthsForPlatform(GetPlatformName(platform));
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern int[] GetIconWidthsForPlatform(string platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetIncrementalIl2CppBuild(BuildTargetGroup targetGroup);
        internal static string GetPlatformName(BuildTargetGroup targetGroup)
        {
            <GetPlatformName>c__AnonStorey0 storey = new <GetPlatformName>c__AnonStorey0 {
                targetGroup = targetGroup
            };
            BuildPlayerWindow.BuildPlatform platform = BuildPlayerWindow.GetValidPlatforms().Find(new Predicate<BuildPlayerWindow.BuildPlatform>(storey.<>m__0));
            return ((platform != null) ? platform.name : string.Empty);
        }

        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static bool GetPropertyBool(string name)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            return GetPropertyBool(name, unknown);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use explicit API instead.")]
        public static extern bool GetPropertyBool(string name, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target);
        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static int GetPropertyInt(string name)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            return GetPropertyInt(name, unknown);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use explicit API instead.")]
        public static extern int GetPropertyInt(string name, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target);
        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static bool GetPropertyOptionalBool(string name, ref bool value)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            return GetPropertyOptionalBool(name, ref value, unknown);
        }

        [Obsolete("Use explicit API instead.")]
        public static bool GetPropertyOptionalBool(string name, ref bool value, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target)
        {
            value = GetPropertyBool(name, target);
            return true;
        }

        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static bool GetPropertyOptionalInt(string name, ref int value)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            return GetPropertyOptionalInt(name, ref value, unknown);
        }

        [Obsolete("Use explicit API instead.")]
        public static bool GetPropertyOptionalInt(string name, ref int value, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target)
        {
            value = GetPropertyInt(name, target);
            return true;
        }

        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static bool GetPropertyOptionalString(string name, ref string value)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            return GetPropertyOptionalString(name, ref value, unknown);
        }

        [Obsolete("Use explicit API instead.")]
        public static bool GetPropertyOptionalString(string name, ref string value, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target)
        {
            value = GetPropertyString(name, target);
            return true;
        }

        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static string GetPropertyString(string name)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            return GetPropertyString(name, unknown);
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use explicit API instead.")]
        public static extern string GetPropertyString(string name, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern ScriptingImplementation GetScriptingBackend(BuildTargetGroup targetGroup);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern string GetScriptingDefineSymbolsForGroup(BuildTargetGroup targetGroup);
        internal static SerializedObject GetSerializedObject()
        {
            if (_serializedObject == null)
            {
                _serializedObject = new SerializedObject(InternalGetPlayerSettingsObject());
            }
            return _serializedObject;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern StackTraceLogType GetStackTraceLogType(LogType logType);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern GraphicsDeviceType[] GetSupportedGraphicsAPIs(BuildTarget platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern string GetTemplateCustomValue(string key);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetUseDefaultGraphicsAPIs(BuildTarget platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool HasAspectRatio(AspectRatio aspectRatio);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_get_cursorHotspot(out Vector2 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_set_cursorHotspot(ref Vector2 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern UnityEngine.Object InternalGetPlayerSettingsObject();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetAdditionalIl2CppArgs(string additionalArgs);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetApiCompatibilityInternal(int value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetArchitecture(BuildTargetGroup targetGroup, int architecture);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetAspectRatio(AspectRatio aspectRatio, bool enable);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetBatchingForPlatform(BuildTarget platform, int staticBatching, int dynamicBatching);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetCloudServiceEnabled(string serviceKey, bool enabled);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetDirty();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetGraphicsAPIs(BuildTarget platform, GraphicsDeviceType[] apis);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetIconsForPlatform(string platform, Texture2D[] icons);
        public static void SetIconsForTargetGroup(BuildTargetGroup platform, Texture2D[] icons)
        {
            SetIconsForPlatform(GetPlatformName(platform), icons);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetIncrementalIl2CppBuild(BuildTargetGroup targetGroup, bool enabled);
        [Obsolete("Use explicit API instead."), ExcludeFromDocs]
        public static void SetPropertyBool(string name, bool value)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            SetPropertyBool(name, value, unknown);
        }

        [Obsolete("Use explicit API instead.")]
        public static void SetPropertyBool(string name, bool value, BuildTarget target)
        {
            SetPropertyBool(name, value, BuildPipeline.GetBuildTargetGroup(target));
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use explicit API instead.")]
        public static extern void SetPropertyBool(string name, bool value, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target);
        [Obsolete("Use explicit API instead."), ExcludeFromDocs]
        public static void SetPropertyInt(string name, int value)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            SetPropertyInt(name, value, unknown);
        }

        [Obsolete("Use explicit API instead.")]
        public static void SetPropertyInt(string name, int value, BuildTarget target)
        {
            SetPropertyInt(name, value, BuildPipeline.GetBuildTargetGroup(target));
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use explicit API instead.")]
        public static extern void SetPropertyInt(string name, int value, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target);
        [ExcludeFromDocs, Obsolete("Use explicit API instead.")]
        public static void SetPropertyString(string name, string value)
        {
            BuildTargetGroup unknown = BuildTargetGroup.Unknown;
            SetPropertyString(name, value, unknown);
        }

        [Obsolete("Use explicit API instead.")]
        public static void SetPropertyString(string name, string value, BuildTarget target)
        {
            SetPropertyString(name, value, BuildPipeline.GetBuildTargetGroup(target));
        }

        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use explicit API instead.")]
        public static extern void SetPropertyString(string name, string value, [UnityEngine.Internal.DefaultValue("BuildTargetGroup.Unknown")] BuildTargetGroup target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetScriptingBackend(BuildTargetGroup targetGroup, ScriptingImplementation backend);
        public static void SetScriptingDefineSymbolsForGroup(BuildTargetGroup targetGroup, string defines)
        {
            if (!string.IsNullOrEmpty(defines))
            {
                defines = string.Join(";", defines.Split(defineSplits, StringSplitOptions.RemoveEmptyEntries));
            }
            SetScriptingDefineSymbolsForGroupInternal(targetGroup, defines);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void SetScriptingDefineSymbolsForGroupInternal(BuildTargetGroup targetGroup, string defines);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetStackTraceLogType(LogType logType, StackTraceLogType stackTraceType);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void SetTemplateCustomValue(string key, string value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetUseDefaultGraphicsAPIs(BuildTarget platform, bool automatic);

        public static int accelerometerFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ActionOnDotNetUnhandledException actionOnDotNetUnhandledException { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool advancedLicense { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool allowedAutorotateToLandscapeLeft { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool allowedAutorotateToLandscapeRight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool allowedAutorotateToPortrait { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool allowedAutorotateToPortraitUpsideDown { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool allowFullscreenSwitch { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("The option alwaysDisplayWatermark is deprecated and is always false", true)]
        public static bool alwaysDisplayWatermark
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public static string aotOptions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ApiCompatibilityLevel apiCompatibilityLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool bakeCollisionMeshes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string bundleIdentifier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string bundleVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool captureSingleScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string cloudProjectId
        {
            get
            {
                return cloudProjectIdRaw;
            }
            internal set
            {
                cloudProjectIdRaw = value;
            }
        }

        private static string cloudProjectIdRaw { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ColorSpace colorSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string companyName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Vector2 cursorHotspot
        {
            get
            {
                Vector2 vector;
                INTERNAL_get_cursorHotspot(out vector);
                return vector;
            }
            set
            {
                INTERNAL_set_cursorHotspot(ref value);
            }
        }

        public static D3D11FullscreenMode d3d11FullscreenMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static D3D9FullscreenMode d3d9FullscreenMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Texture2D defaultCursor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static UIOrientation defaultInterfaceOrientation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool defaultIsFullScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool defaultIsNativeResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int defaultScreenHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int defaultScreenWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int defaultWebScreenHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int defaultWebScreenWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static ResolutionDialogSetting displayResolutionDialog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool enableCrashReportAPI { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool enableInternalProfiler { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use AssetBundles instead for streaming data", true)]
        public static int firstStreamedLevelWithResources
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public static bool forceSingleInstance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool gpuSkinning { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool graphicsJobs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string iPhoneBundleIdentifier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string keyaliasPass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string keystorePass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use PlayerSettings.iOS.locationUsageDescription instead (UnityUpgradable) -> UnityEditor.PlayerSettings/iOS.locationUsageDescription", false)]
        public static string locationUsageDescription
        {
            get
            {
                return iOS.locationUsageDescription;
            }
            set
            {
                iOS.locationUsageDescription = value;
            }
        }

        public static bool logObjCUncaughtExceptions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static MacFullscreenMode macFullscreenMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool mobileMTRendering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("mobileRenderingPath is ignored, use UnityEditor.Rendering.TierSettings with UnityEditor.Rendering.SetTierSettings/GetTierSettings instead", false)]
        public static RenderingPath mobileRenderingPath
        {
            get
            {
                return EditorGraphicsSettings.GetCurrentTierSettings().renderingPath;
            }
            set
            {
            }
        }

        public static bool MTRendering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool muteOtherAudioSources { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool openGLRequireES31 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool openGLRequireES31AEP { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Guid productGUID
        {
            get
            {
                return new Guid(productGUIDRaw);
            }
        }

        private static byte[] productGUIDRaw { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string productName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool protectGraphicsMemory { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("renderingPath is ignored, use UnityEditor.Rendering.TierSettings with UnityEditor.Rendering.SetTierSettings/GetTierSettings instead", false)]
        public static RenderingPath renderingPath
        {
            get
            {
                return EditorGraphicsSettings.GetCurrentTierSettings().renderingPath;
            }
            set
            {
            }
        }

        public static bool resizableWindow { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Texture2D resolutionDialogBanner { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool runInBackground { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use PlayerSettings.SplashScreen.show instead")]
        public static bool showUnitySplashScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("singlePassStereoRendering will be deprecated. Use stereoRenderingPath instead.")]
        public static bool singlePassStereoRendering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use PlayerSettings.SplashScreen.unityLogoStyle instead")]
        public static SplashScreenStyle splashScreenStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static string spritePackerPolicy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool statusBarHidden { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static StereoRenderingPath stereoRenderingPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use VREditor.GetStereoDeviceEnabled instead")]
        public static bool stereoscopic3D { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool stripEngineCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static StrippingLevel strippingLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool stripUnusedMeshComponents { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static bool submitAnalytics { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("targetGlesGraphics is ignored, use SetGraphicsAPIs/GetGraphicsAPIs APIs", false)]
        public static TargetGlesGraphics targetGlesGraphics
        {
            get
            {
                return TargetGlesGraphics.Automatic;
            }
            set
            {
            }
        }

        [Obsolete("targetIOSGraphics is ignored, use SetGraphicsAPIs/GetGraphicsAPIs APIs", false)]
        public static TargetIOSGraphics targetIOSGraphics
        {
            get
            {
                return TargetIOSGraphics.Automatic;
            }
            set
            {
            }
        }

        internal static string[] templateCustomKeys { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool use32BitDisplayBuffer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool useAnimatedAutorotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use UnityEditor.PlayerSettings.SetGraphicsAPIs/GetGraphicsAPIs instead")]
        public static bool useDirect3D11 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool useMacAppStoreValidation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool usePlayerLog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static Texture2D virtualRealitySplashScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool virtualRealitySupported { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool visibleInBackground { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static string webPlayerTemplate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int xboxAdditionalTitleMemorySize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool xboxDeployKinectHeadOrientation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool xboxDeployKinectHeadPosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool xboxDeployKinectResources { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxEnableAvatar { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxEnableGuest { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxEnableKinect { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxEnableKinectAutoTracking { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxEnableSpeech { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxGenerateSpa { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string xboxImageXexFilePath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static int xboxOneResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool xboxPIXTextureCapture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string xboxSpaFilePath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static uint xboxSpeechDB { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static Texture2D xboxSplashScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static string xboxTitleId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [CompilerGenerated]
        private sealed class <GetPlatformName>c__AnonStorey0
        {
            internal BuildTargetGroup targetGroup;

            internal bool <>m__0(BuildPlayerWindow.BuildPlatform p)
            {
                return (p.targetGroup == this.targetGroup);
            }
        }

        public sealed class Android
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern Texture2D GetAndroidBannerForHeight(int height);
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern AndroidBanner[] GetAndroidBanners();
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern void SetAndroidBanners(Texture2D[] banners);

            internal static bool androidBannerEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            internal static AndroidGamepadSupportLevel androidGamepadSupportLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool androidIsGame { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool androidTVCompatibility { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int bundleVersionCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            internal static bool createWallpaper { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool disableDepthAndStencilBuffers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool forceInternetPermission { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool forceSDCardPermission { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string keyaliasName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string keyaliasPass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string keystoreName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string keystorePass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool licenseVerification { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static AndroidSdkVersions minSdkVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static AndroidPreferredInstallLocation preferredInstallLocation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static AndroidShowActivityIndicatorOnLoading showActivityIndicatorOnLoading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static AndroidSplashScreenScale splashScreenScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static AndroidTargetDevice targetDevice { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("This has been replaced by disableDepthAndStencilBuffers")]
            public static bool use24BitDepthBuffer
            {
                get
                {
                    return !disableDepthAndStencilBuffers;
                }
                set
                {
                }
            }

            public static bool useAPKExpansionFiles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        }

        public sealed class iOS
        {
            internal static iOSDeviceRequirementGroup AddDeviceRequirementsForAssetBundleVariant(string name)
            {
                return new iOSDeviceRequirementGroup(name);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern bool CheckAssetBundleVariantHasDeviceRequirements(string name);
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern string[] GetAssetBundleVariantsWithDeviceRequirements();
            internal static iOSDeviceRequirementGroup GetDeviceRequirementsForAssetBundleVariant(string name)
            {
                if (!CheckAssetBundleVariantHasDeviceRequirements(name))
                {
                    return null;
                }
                return new iOSDeviceRequirementGroup(name);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern string[] GetURLSchemes();
            internal static void RemoveDeviceRequirementsForAssetBundleVariant(string name)
            {
                iOSDeviceRequirementGroup deviceRequirementsForAssetBundleVariant = GetDeviceRequirementsForAssetBundleVariant(name);
                for (int i = 0; i < deviceRequirementsForAssetBundleVariant.count; i++)
                {
                    deviceRequirementsForAssetBundleVariant.RemoveAt(0);
                }
            }

            public static bool allowHTTPDownload { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static iOSAppInBackgroundBehavior appInBackgroundBehavior { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string appleDeveloperTeamID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool appleEnableAutomaticSigning { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string applicationDisplayName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static iOSBackgroundMode backgroundModes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string buildNumber { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string cameraUsageDescription { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("exitOnSuspend is deprecated, use appInBackgroundBehavior", false)]
            public static bool exitOnSuspend
            {
                get
                {
                    return (appInBackgroundBehavior == iOSAppInBackgroundBehavior.Exit);
                }
                set
                {
                    appInBackgroundBehavior = iOSAppInBackgroundBehavior.Exit;
                }
            }

            public static bool forceHardShadowsOnMetal { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string iOSManualProvisioningProfileID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string locationUsageDescription { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string microphoneUsageDescription { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use PlayerSettings.muteOtherAudioSources instead (UnityUpgradable) -> UnityEditor.PlayerSettings.muteOtherAudioSources", false)]
            public static bool overrideIPodMusic
            {
                get
                {
                    return PlayerSettings.muteOtherAudioSources;
                }
                set
                {
                    PlayerSettings.muteOtherAudioSources = value;
                }
            }

            public static bool prerenderedIcon { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool requiresFullScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool requiresPersistentWiFi { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static ScriptCallOptimizationLevel scriptCallOptimization { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static iOSSdkVersion sdkVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static iOSShowActivityIndicatorOnLoading showActivityIndicatorOnLoading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static iOSStatusBarStyle statusBarStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static iOSTargetDevice targetDevice { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("targetOSVersion is obsolete, use targetOSVersionString")]
            public static iOSTargetOSVersion targetOSVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string targetOSVersionString { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use Screen.SetResolution at runtime", true)]
            public static iOSTargetResolution targetResolution
            {
                get
                {
                    return iOSTargetResolution.Native;
                }
                set
                {
                }
            }

            public static string tvOSManualProvisioningProfileID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool useOnDemandResources { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        }

        public sealed class N3DS
        {
            public static string applicationId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool compressStaticMem { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool disableDepthAndStencilBuffers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool disableStereoscopicView { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool enableSharedListOpt { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool enableVSync { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string extSaveDataNumber { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static LogoStyle logoStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static MediaSize mediaSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Region region { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int stackSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static TargetPlatform targetPlatform { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string title { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool useExtSaveData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public enum LogoStyle
            {
                Nintendo,
                Distributed,
                iQue,
                Licensed
            }

            public enum MediaSize
            {
                _128MB,
                _256MB,
                _512MB,
                _1GB,
                _2GB
            }

            public enum Region
            {
                All = 7,
                America = 2,
                China = 4,
                Europe = 3,
                Japan = 1,
                Korea = 5,
                Taiwan = 6
            }

            public enum TargetPlatform
            {
                NewNintendo3DS = 2,
                Nintendo3DS = 1
            }
        }

        public sealed class PS4
        {
            public static int applicationParameter1 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int applicationParameter2 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int applicationParameter3 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int applicationParameter4 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int appType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string appVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool attrib3DSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int attribCpuUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool attribExclusiveVR { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PlayStationVREyeToEyeDistanceSettings attribEyeToEyeDistanceSettingVR { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool attribMoveSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool attribShareSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool attribUserManagement { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int audio3dVirtualSpeakerCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string BackgroundImagePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string BGMPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PS4AppCategory category { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string contentID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool contentSearchFeaturesUsed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool disableAutoHideSplash { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int downloadDataSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PS4EnterButtonAssignment enterButtonAssignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int garlicHeapSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string[] includedModules { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string masterVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string monoEnv { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int npAgeRating { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string NPtitleDatPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npTitleSecret { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npTrophyPackPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string paramSfxPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int parentalLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string passcode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PatchChangeinfoPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool patchDayOne { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PatchLatestPkgPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PatchPkgPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool playerPrefsSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int playTogetherPlayerCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool pnFriends { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool pnGameCustomData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool pnPresence { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool pnSessions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PrivacyGuardImagePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int proGarlicHeapSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PronunciationSIGPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PronunciationXMLPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PS4RemotePlayKeyAssignment remotePlayKeyAssignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string remotePlayKeyMappingDir { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool reprojectionSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool restrictedAudioUsageRights { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string SaveDataImagePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int scriptOptimizationLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string SdkOverride { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string ShareFilePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string ShareOverlayImagePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int socialScreenEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string StartupImagePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool useAudio3dBackend { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            internal static bool useDebugIl2cppLibs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool useResolutionFallback { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int videoOutBaseModeInitialWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int videoOutInitialWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int videoOutPixelFormat { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int videoOutReprojectionRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("videoOutResolution is deprecated. Use PlayerSettings.PS4.videoOutInitialWidth and PlayerSettings.PS4.videoOutReprojectionRate to control initial display resolution and reprojection rate.")]
            public static int videoOutResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool videoRecordingFeaturesUsed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public enum PlayStationVREyeToEyeDistanceSettings
            {
                PerUser,
                ForceDefault,
                DynamicModeAtRuntime
            }

            public enum PS4AppCategory
            {
                Application,
                Patch
            }

            public enum PS4EnterButtonAssignment
            {
                CircleButton,
                CrossButton
            }

            public enum PS4RemotePlayKeyAssignment
            {
                None = -1,
                PatternA = 0,
                PatternB = 1,
                PatternC = 2,
                PatternD = 3,
                PatternE = 4,
                PatternF = 5,
                PatternG = 6,
                PatternH = 7
            }
        }

        public sealed class PSM
        {
        }

        public sealed class PSVita
        {
            public static bool acquireBGM { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("AllowTwitterDialog has no effect as of SDK 3.570")]
            public static bool AllowTwitterDialog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string appVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PSVitaAppCategory category { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string contentID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PSVitaDRMType drmType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PSVitaEnterButtonAssignment enterButtonAssignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool healthWarning { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool infoBarColor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool infoBarOnStartup { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string keystoneFile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string liveAreaBackroundPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string liveAreaGatePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string liveAreaPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string liveAreaTrialPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string manualPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string masterVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int mediaCapacity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PSVitaMemoryExpansionMode memoryExpansionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int npAgeRating { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npCommsPassphrase { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npCommsSig { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npCommunicationsID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool npSupportGBMorGJP { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npTitleDatPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string npTrophyPackPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string packagePassword { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string paramSfxPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int parentalLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string patchChangeInfoPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string patchOriginalPackage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PSVitaPowerMode powerMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int saveDataQuota { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string shortTitle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int storageType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PSVitaTvBootMode tvBootMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool tvDisableEmu { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool upgradable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            internal static bool useDebugIl2cppLibs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("useLibLocation has no effect as of SDK 3.570")]
            public static bool useLibLocation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public enum PSVitaAppCategory
            {
                Application,
                ApplicationPatch
            }

            public enum PSVitaDRMType
            {
                PaidFor,
                Free
            }

            public enum PSVitaEnterButtonAssignment
            {
                Default,
                CircleButton,
                CrossButton
            }

            public enum PSVitaMemoryExpansionMode
            {
                None,
                ExpandBy29MB,
                ExpandBy77MB,
                ExpandBy109MB
            }

            public enum PSVitaPowerMode
            {
                ModeA,
                ModeB,
                ModeC
            }

            public enum PSVitaTvBootMode
            {
                Default,
                PSVitaBootablePSVitaTvBootable,
                PSVitaBootablePSVitaTvNotBootable
            }
        }

        public sealed class SamsungTV
        {
            public static string deviceAddress { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productAuthor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productAuthorEmail { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static SamsungTVProductCategories productCategory { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productDescription { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productLink { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public enum SamsungTVProductCategories
            {
                Games,
                Videos,
                Sports,
                Lifestyle,
                Information,
                Education,
                Kids
            }
        }

        public sealed class SplashScreen
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_get_backgroundColor(out Color value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_set_backgroundColor(ref Color value);

            public static float animationBackgroundZoom { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static float animationLogoZoom { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static AnimationMode animationMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Sprite background { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Color backgroundColor
            {
                get
                {
                    Color color;
                    INTERNAL_get_backgroundColor(out color);
                    return color;
                }
                set
                {
                    INTERNAL_set_backgroundColor(ref value);
                }
            }

            public static Sprite backgroundPortrait { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static DrawMode drawMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PlayerSettings.SplashScreenLogo[] logos { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static float overlayOpacity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool show { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool showUnityLogo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static UnityLogoStyle unityLogoStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public enum AnimationMode
            {
                Static,
                Dolly,
                Custom
            }

            public enum DrawMode
            {
                UnityLogoBelow,
                AllSequential
            }

            public enum UnityLogoStyle
            {
                DarkOnLight,
                LightOnDark
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SplashScreenLogo
        {
            private const float k_MinLogoTime = 2f;
            private static Sprite s_UnityLogo;
            private Sprite m_Logo;
            private float m_Duration;
            static SplashScreenLogo()
            {
                s_UnityLogo = Resources.GetBuiltinResource<Sprite>("UnitySplash-cube.png");
            }

            [ExcludeFromDocs]
            public static PlayerSettings.SplashScreenLogo Create(float duration)
            {
                Sprite logo = null;
                return Create(duration, logo);
            }

            [ExcludeFromDocs]
            public static PlayerSettings.SplashScreenLogo Create()
            {
                Sprite logo = null;
                float duration = 2f;
                return Create(duration, logo);
            }

            public static PlayerSettings.SplashScreenLogo Create([UnityEngine.Internal.DefaultValue("k_MinLogoTime")] float duration, [UnityEngine.Internal.DefaultValue("null")] Sprite logo)
            {
                return new PlayerSettings.SplashScreenLogo { m_Duration = duration, m_Logo = logo };
            }

            [ExcludeFromDocs]
            public static PlayerSettings.SplashScreenLogo CreateWithUnityLogo()
            {
                float duration = 2f;
                return CreateWithUnityLogo(duration);
            }

            public static PlayerSettings.SplashScreenLogo CreateWithUnityLogo([UnityEngine.Internal.DefaultValue("k_MinLogoTime")] float duration)
            {
                return new PlayerSettings.SplashScreenLogo { m_Duration = duration, m_Logo = s_UnityLogo };
            }

            public Sprite logo
            {
                get
                {
                    return this.m_Logo;
                }
                set
                {
                    this.m_Logo = value;
                }
            }
            public static Sprite unityLogo
            {
                get
                {
                    return s_UnityLogo;
                }
            }
            public float duration
            {
                get
                {
                    return Mathf.Max(this.m_Duration, 2f);
                }
                set
                {
                    this.m_Duration = Mathf.Max(value, 2f);
                }
            }
        }

        public sealed class Tizen
        {
            public static bool GetCapability(PlayerSettings.TizenCapability capability)
            {
                string str = InternalGetCapability(capability.ToString());
                if (string.IsNullOrEmpty(str))
                {
                    return false;
                }
                try
                {
                    return (bool) TypeDescriptor.GetConverter(typeof(bool)).ConvertFromString(str);
                }
                catch
                {
                    Debug.LogError("Failed to parse value  ('" + capability.ToString() + "," + str + "') to bool type.");
                    return false;
                }
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern string InternalGetCapability(string name);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void InternalSetCapability(string name, string value);
            public static void SetCapability(PlayerSettings.TizenCapability capability, bool value)
            {
                InternalSetCapability(capability.ToString(), value.ToString());
            }

            public static string deploymentTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int deploymentTargetType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static TizenOSVersion minOSVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productDescription { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string productURL { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static TizenShowActivityIndicatorOnLoading showActivityIndicatorOnLoading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string signingProfileName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        }

        public enum TizenCapability
        {
            Location,
            DataSharing,
            NetworkGet,
            WifiDirect,
            CallHistoryRead,
            Power,
            ContactWrite,
            MessageWrite,
            ContentWrite,
            Push,
            AccountRead,
            ExternalStorage,
            Recorder,
            PackageManagerInfo,
            NFCCardEmulation,
            CalendarWrite,
            WindowPrioritySet,
            VolumeSet,
            CallHistoryWrite,
            AlarmSet,
            Call,
            Email,
            ContactRead,
            Shortcut,
            KeyManager,
            LED,
            NetworkProfile,
            AlarmGet,
            Display,
            CalendarRead,
            NFC,
            AccountWrite,
            Bluetooth,
            Notification,
            NetworkSet,
            ExternalStorageAppData,
            Download,
            Telephony,
            MessageRead,
            MediaStorage,
            Internet,
            Camera,
            Haptic,
            AppManagerLaunch,
            SystemSettings
        }

        public sealed class tvOS
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern Texture2D[] GetLargeIconLayers();
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern Texture2D[] GetSmallIconLayers();
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern Texture2D[] GetTopShelfImageLayers();
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern Texture2D[] GetTopShelfImageWideLayers();
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern void SetLargeIconLayers(Texture2D[] layers);
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern void SetSmallIconLayers(Texture2D[] layers);
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern void SetTopShelfImageLayers(Texture2D[] layers);
            [MethodImpl(MethodImplOptions.InternalCall)]
            internal static extern void SetTopShelfImageWideLayers(Texture2D[] layers);

            public static bool requireExtendedGameController { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static tvOSSdkVersion sdkVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static tvOSTargetOSVersion targetOSVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string targetOSVersionString { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        }

        public sealed class WebGL
        {
            public static bool analyzeBuildSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static WebGLCompressionFormat compressionFormat { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool dataCaching { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool debugSymbols { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string emscriptenArgs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static WebGLExceptionSupport exceptionSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int memorySize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string modulesDirectory { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string template { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool useEmbeddedResources { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool useWasm { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        }

        public sealed class WiiU
        {
            public static int accountBossSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int accountSaveSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string[] addOnUniqueIDs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool allowScreenCapture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int commonBossSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int commonSaveSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int controllerCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool drcBufferDisabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int gamePadMSAA { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Texture2D gamePadStartupScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static string groupID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string joinGameId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string joinGameModeMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int loaderThreadStackSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int mainThreadStackSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string olvAccessKey { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string profilerLibraryPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool supportsBalanceBoard { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool supportsClassicController { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool supportsMotionPlus { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool supportsNunchuk { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool supportsProController { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int systemHeapSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string tinCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string titleID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static WiiUTVResolution tvResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Texture2D tvStartupScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; }
        }

        public sealed class WSA
        {
            public static bool GetCapability(PlayerSettings.WSACapability capability)
            {
                string str = InternalGetCapability(capability.ToString());
                if (string.IsNullOrEmpty(str))
                {
                    return false;
                }
                try
                {
                    return (bool) TypeDescriptor.GetConverter(typeof(bool)).ConvertFromString(str);
                }
                catch
                {
                    Debug.LogError("Failed to parse value  ('" + capability.ToString() + "," + str + "') to bool type.");
                    return false;
                }
            }

            public static string GetVisualAssetsImage(PlayerSettings.WSAImageType type, PlayerSettings.WSAImageScale scale)
            {
                ValidateWSAImageType(type);
                ValidateWSAImageScale(scale);
                return GetWSAImage(type, scale);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern string GetWSAImage(PlayerSettings.WSAImageType type, PlayerSettings.WSAImageScale scale);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_get_internalFileTypeAssociations(out PlayerSettings.WSAFileTypeAssociations value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_get_splashScreenBackgroundColorRaw(out Color value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_get_tileBackgroundColor(out Color value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_set_internalFileTypeAssociations(ref PlayerSettings.WSAFileTypeAssociations value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_set_splashScreenBackgroundColorRaw(ref Color value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void INTERNAL_set_tileBackgroundColor(ref Color value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern string InternalGetCapability(string name);
            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void InternalSetCapability(string name, string value);
            public static void SetCapability(PlayerSettings.WSACapability capability, bool value)
            {
                InternalSetCapability(capability.ToString(), value.ToString());
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern bool SetCertificate(string path, string password);
            public static void SetVisualAssetsImage(string image, PlayerSettings.WSAImageType type, PlayerSettings.WSAImageScale scale)
            {
                ValidateWSAImageType(type);
                ValidateWSAImageScale(scale);
                SetWSAImage(image, type, scale);
            }

            [MethodImpl(MethodImplOptions.InternalCall)]
            private static extern void SetWSAImage(string image, PlayerSettings.WSAImageType type, PlayerSettings.WSAImageScale scale);
            internal static string ValidatePackageVersion(string value)
            {
                Regex regex = new Regex(@"^(\d+)\.(\d+)\.(\d+)\.(\d+)$", RegexOptions.CultureInvariant | RegexOptions.Compiled);
                if (regex.IsMatch(value))
                {
                    return value;
                }
                return "1.0.0.0";
            }

            private static void ValidateWSAImageScale(PlayerSettings.WSAImageScale scale)
            {
                if (((((scale != PlayerSettings.WSAImageScale.Target16) && (scale != PlayerSettings.WSAImageScale.Target24)) && ((scale != PlayerSettings.WSAImageScale.Target32) && (scale != PlayerSettings.WSAImageScale.Target48))) && (((scale != PlayerSettings.WSAImageScale._80) && (scale != PlayerSettings.WSAImageScale._100)) && ((scale != PlayerSettings.WSAImageScale._125) && (scale != PlayerSettings.WSAImageScale._140)))) && ((((scale != PlayerSettings.WSAImageScale._150) && (scale != PlayerSettings.WSAImageScale._180)) && ((scale != PlayerSettings.WSAImageScale._200) && (scale != PlayerSettings.WSAImageScale._240))) && ((scale != PlayerSettings.WSAImageScale.Target256) && (scale != PlayerSettings.WSAImageScale._400))))
                {
                    throw new Exception("Unknown image scale: " + scale);
                }
            }

            private static void ValidateWSAImageType(PlayerSettings.WSAImageType type)
            {
                switch (type)
                {
                    case PlayerSettings.WSAImageType.StoreTileLogo:
                    case PlayerSettings.WSAImageType.StoreTileWideLogo:
                    case PlayerSettings.WSAImageType.StoreTileSmallLogo:
                    case PlayerSettings.WSAImageType.StoreSmallTile:
                    case PlayerSettings.WSAImageType.StoreLargeTile:
                    case PlayerSettings.WSAImageType.PhoneAppIcon:
                    case PlayerSettings.WSAImageType.PhoneSmallTile:
                    case PlayerSettings.WSAImageType.PhoneMediumTile:
                    case PlayerSettings.WSAImageType.PhoneWideTile:
                    case PlayerSettings.WSAImageType.PhoneSplashScreen:
                    case PlayerSettings.WSAImageType.UWPSquare44x44Logo:
                    case PlayerSettings.WSAImageType.UWPSquare71x71Logo:
                    case PlayerSettings.WSAImageType.UWPSquare150x150Logo:
                    case PlayerSettings.WSAImageType.UWPSquare310x310Logo:
                    case PlayerSettings.WSAImageType.UWPWide310x150Logo:
                        return;
                }
                if ((type != PlayerSettings.WSAImageType.PackageLogo) && (type != PlayerSettings.WSAImageType.SplashScreenImage))
                {
                    throw new Exception("Unknown WSA image type: " + type);
                }
            }

            public static string applicationDescription { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string certificateIssuer { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static DateTime? certificateNotAfter
            {
                get
                {
                    long certificateNotAfterRaw = PlayerSettings.WSA.certificateNotAfterRaw;
                    if (certificateNotAfterRaw != 0L)
                    {
                        return new DateTime?(DateTime.FromFileTime(certificateNotAfterRaw));
                    }
                    return null;
                }
            }

            private static long certificateNotAfterRaw { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            internal static string certificatePassword { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static string certificatePath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static string certificateSubject { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static string commandLineArgsFile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PlayerSettings.WSACompilationOverrides compilationOverrides { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PlayerSettings.WSADefaultTileSize defaultTileSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("PlayerSettings.WSA.enableIndependentInputSource is deprecated. Use PlayerSettings.WSA.inputSource.", false)]
            public static bool enableIndependentInputSource
            {
                get
                {
                    return (inputSource == PlayerSettings.WSAInputSource.IndependentInputSource);
                }
                set
                {
                    inputSource = !value ? PlayerSettings.WSAInputSource.CoreWindow : PlayerSettings.WSAInputSource.IndependentInputSource;
                }
            }

            [Obsolete("PlayerSettings.enableLowLatencyPresentationAPI is deprecated. It is now always enabled.", false)]
            public static bool enableLowLatencyPresentationAPI { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PlayerSettings.WSAInputSource inputSource { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            internal static PlayerSettings.WSAFileTypeAssociations internalFileTypeAssociations
            {
                get
                {
                    PlayerSettings.WSAFileTypeAssociations associations;
                    INTERNAL_get_internalFileTypeAssociations(out associations);
                    return associations;
                }
                set
                {
                    INTERNAL_set_internalFileTypeAssociations(ref value);
                }
            }

            internal static string internalProtocolName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool largeTileShowName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool mediumTileShowName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string packageLogo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string packageLogo140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string packageLogo180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string packageLogo240 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string packageName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Version packageVersion
            {
                get
                {
                    Version version;
                    try
                    {
                        version = new Version(ValidatePackageVersion(packageVersionRaw));
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(string.Format("{0}, the raw string was {1}", exception.Message, packageVersionRaw));
                    }
                    return version;
                }
                set
                {
                    packageVersionRaw = value.ToString();
                }
            }

            private static string packageVersionRaw { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneAppIcon { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneAppIcon140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneAppIcon240 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneMediumTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneMediumTile140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneMediumTile240 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneSmallTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneSmallTile140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneSmallTile240 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneSplashScreenImage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneSplashScreenImageScale140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneSplashScreenImageScale240 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneWideTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneWideTile140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string phoneWideTile240 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Color? splashScreenBackgroundColor
            {
                get
                {
                    if (splashScreenUseBackgroundColor)
                    {
                        return new Color?(splashScreenBackgroundColorRaw);
                    }
                    return null;
                }
                set
                {
                    splashScreenUseBackgroundColor = value.HasValue;
                    if (value.HasValue)
                    {
                        splashScreenBackgroundColorRaw = value.Value;
                    }
                }
            }

            private static Color splashScreenBackgroundColorRaw
            {
                get
                {
                    Color color;
                    INTERNAL_get_splashScreenBackgroundColorRaw(out color);
                    return color;
                }
                set
                {
                    INTERNAL_set_splashScreenBackgroundColorRaw(ref value);
                }
            }

            private static bool splashScreenUseBackgroundColor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeLargeTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeLargeTile140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeLargeTile180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeLargeTile80 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSmallTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSmallTile140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSmallTile180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSmallTile80 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSplashScreenImage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSplashScreenImageScale140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeSplashScreenImageScale180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileLogo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileLogo140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileLogo180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileLogo80 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileSmallLogo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileSmallLogo140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileSmallLogo180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileSmallLogo80 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileWideLogo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileWideLogo140 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileWideLogo180 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            [Obsolete("Use GetVisualAssetsImage()/SetVisualAssetsImage()")]
            public static string storeTileWideLogo80 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static Color tileBackgroundColor
            {
                get
                {
                    Color color;
                    INTERNAL_get_tileBackgroundColor(out color);
                    return color;
                }
                set
                {
                    INTERNAL_set_tileBackgroundColor(ref value);
                }
            }

            public static PlayerSettings.WSAApplicationForegroundText tileForegroundText { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string tileShortName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static PlayerSettings.WSAApplicationShowName tileShowName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool wideTileShowName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static class Declarations
            {
                public static PlayerSettings.WSAFileTypeAssociations fileTypeAssociations
                {
                    get
                    {
                        return PlayerSettings.WSA.internalFileTypeAssociations;
                    }
                    set
                    {
                        PlayerSettings.WSA.internalFileTypeAssociations = value;
                    }
                }

                public static string protocolName
                {
                    get
                    {
                        return PlayerSettings.WSA.internalProtocolName;
                    }
                    set
                    {
                        PlayerSettings.WSA.internalProtocolName = value;
                    }
                }
            }
        }

        public enum WSAApplicationForegroundText
        {
            Dark = 2,
            Light = 1
        }

        public enum WSAApplicationShowName
        {
            NotSet,
            AllLogos,
            NoLogos,
            StandardLogoOnly,
            WideLogoOnly
        }

        public enum WSACapability
        {
            EnterpriseAuthentication,
            InternetClient,
            InternetClientServer,
            MusicLibrary,
            PicturesLibrary,
            PrivateNetworkClientServer,
            RemovableStorage,
            SharedUserCertificates,
            VideosLibrary,
            WebCam,
            Proximity,
            Microphone,
            Location,
            HumanInterfaceDevice,
            AllJoyn,
            BlockedChatMessages,
            Chat,
            CodeGeneration,
            Objects3D,
            PhoneCall,
            UserAccountInformation,
            VoipCall,
            Bluetooth,
            SpatialPerception,
            InputInjectionBrokered
        }

        public enum WSACompilationOverrides
        {
            None,
            UseNetCore,
            UseNetCorePartially
        }

        public enum WSADefaultTileSize
        {
            NotSet,
            Medium,
            Wide
        }

        [StructLayout(LayoutKind.Sequential), RequiredByNativeCode]
        public struct WSAFileTypeAssociations
        {
            public string name;
            public PlayerSettings.WSASupportedFileType[] supportedFileTypes;
        }

        public enum WSAImageScale
        {
            _100 = 100,
            _125 = 0x7d,
            _140 = 140,
            _150 = 150,
            _180 = 180,
            _200 = 200,
            _240 = 240,
            _400 = 400,
            _80 = 80,
            Target16 = 0x10,
            Target24 = 0x18,
            Target256 = 0x100,
            Target32 = 0x20,
            Target48 = 0x30
        }

        public enum WSAImageType
        {
            PackageLogo = 1,
            PhoneAppIcon = 0x15,
            PhoneMediumTile = 0x17,
            PhoneSmallTile = 0x16,
            PhoneSplashScreen = 0x19,
            PhoneWideTile = 0x18,
            SplashScreenImage = 2,
            StoreLargeTile = 15,
            StoreSmallTile = 14,
            StoreTileLogo = 11,
            StoreTileSmallLogo = 13,
            StoreTileWideLogo = 12,
            UWPSquare150x150Logo = 0x21,
            UWPSquare310x310Logo = 0x22,
            UWPSquare44x44Logo = 0x1f,
            UWPSquare71x71Logo = 0x20,
            UWPWide310x150Logo = 0x23
        }

        public enum WSAInputSource
        {
            CoreWindow,
            IndependentInputSource,
            SwapChainPanel
        }

        [StructLayout(LayoutKind.Sequential), RequiredByNativeCode]
        public struct WSASupportedFileType
        {
            public string contentType;
            public string fileType;
        }

        public sealed class XboxOne
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern bool AddAllowedProductId(string id);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern bool GetCapability(string capability);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern int GetGameRating(string name);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void GetSocketDefinition(string name, out string port, out int protocol, out int[] usages, out string templateName, out int sessionRequirment, out int[] deviceUsages);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern bool GetSupportedLanguage(string language);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void RemoveAllowedProductId(string id);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void RemoveSocketDefinition(string name);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void SetCapability(string capability, bool value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void SetGameRating(string name, int value);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void SetSocketDefinition(string name, string port, int protocol, int[] usages, string templateName, int sessionRequirment, int[] deviceUsages);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void SetSupportedLanguage(string language, bool enabled);
            [MethodImpl(MethodImplOptions.InternalCall)]
            public static extern void UpdateAllowedProductId(int idx, string id);

            public static string[] AllowedProductIds { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static string AppManifestOverridePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string ContentId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static XboxOneLoggingLevel defaultLoggingLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string Description { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool DisableKinectGpuReservation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool EnablePIXSampling { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool EnableVariableGPU { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string GameOsOverridePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static bool IsContentPackage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static int monoLoggingLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static XboxOnePackageUpdateGranularity PackageUpdateGranularity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static XboxOneEncryptionLevel PackagingEncryption { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string PackagingOverridePath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static uint PersistentLocalStorageSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string ProductId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string SandboxId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string SCID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string[] SocketNames { [MethodImpl(MethodImplOptions.InternalCall)] get; }

            public static string TitleId { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string UpdateKey { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

            public static string Version { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
        }
    }
}

