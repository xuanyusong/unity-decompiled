﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class TrueTypeFontImporter : AssetImporter
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern Font GenerateEditableFont(string path);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern bool IsFormatSupported();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern Font[] LookupFallbackFontReferences(string[] _names);

        public AscentCalculationMode ascentCalculationMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int characterPadding { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int characterSpacing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string customCharacters { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string[] fontNames { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Font[] fontReferences { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public FontRenderingMode fontRenderingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("FontRenderModes are no longer supported.", true)]
        private int fontRenderMode
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public FontTextureCase fontTextureCase { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string fontTTFName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool includeFontData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Per-Font styles are no longer supported. Set the style in the rendering component, or import a styled version of the font.", true)]
        private FontStyle style
        {
            get
            {
                return FontStyle.Normal;
            }
            set
            {
            }
        }

        [Obsolete("use2xBehaviour is deprecated. Use ascentCalculationMode instead")]
        private bool use2xBehaviour
        {
            get
            {
                return (this.ascentCalculationMode == AscentCalculationMode.Legacy2x);
            }
            set
            {
                if (value)
                {
                    this.ascentCalculationMode = AscentCalculationMode.Legacy2x;
                }
                else if (this.ascentCalculationMode == AscentCalculationMode.Legacy2x)
                {
                    this.ascentCalculationMode = AscentCalculationMode.FaceAscender;
                }
            }
        }
    }
}

