﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class StaticOcclusionCulling
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Cancel();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Clear();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool Compute();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GenerateInBackground();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void InvalidatePrevisualisationData();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void SetDefaultOcclusionBakeSettings();

        public static float backfaceThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool doesSceneHaveManualPortals { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static bool isRunning { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static float smallestHole { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float smallestOccluder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static int umbraDataSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

