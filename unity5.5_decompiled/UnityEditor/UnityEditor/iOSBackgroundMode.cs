﻿namespace UnityEditor
{
    using System;

    [Flags]
    public enum iOSBackgroundMode : uint
    {
        Audio = 1,
        BluetoothCentral = 0x20,
        BluetoothPeripheral = 0x40,
        ExternalAccessory = 0x10,
        Fetch = 0x80,
        Location = 2,
        NewsstandContent = 8,
        None = 0,
        RemoteNotification = 0x100,
        VOIP = 4
    }
}

