﻿namespace UnityEditor
{
    using System;

    [Flags]
    public enum EditorSelectedRenderState
    {
        Hidden,
        Wireframe,
        Highlight
    }
}

