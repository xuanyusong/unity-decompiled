﻿namespace UnityEditor
{
    using System;

    public enum TextureImporterType
    {
        [Obsolete("Use Default instead. All texture types now have an Advanced foldout (UnityUpgradable) -> Default")]
        Advanced = 5,
        [Obsolete("Use NormalMap (UnityUpgradable) -> NormalMap")]
        Bump = 1,
        Cookie = 4,
        [Obsolete("Use importer.textureShape = TextureImporterShape.TextureCube")]
        Cubemap = 3,
        Cursor = 7,
        Default = 0,
        GUI = 2,
        [Obsolete("HDRI is not supported anymore")]
        HDRI = 9,
        [Obsolete("Use Default (UnityUpgradable) -> Default")]
        Image = 0,
        Lightmap = 6,
        NormalMap = 1,
        [Obsolete("Use a texture setup as a cubemap with glossy reflection instead")]
        Reflection = 3,
        SingleChannel = 10,
        Sprite = 8
    }
}

