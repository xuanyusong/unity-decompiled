﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEditor.Animations;
    using UnityEngine;

    public class ModelImporter : AssetImporter
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern string CalculateBestFittingPreviewGameObject();
        public void CreateDefaultMaskForClip(ModelImporterClipAnimation clip)
        {
            if (this.defaultClipAnimations.Length > 0)
            {
                AvatarMask mask = new AvatarMask();
                this.defaultClipAnimations[0].ConfigureMaskFromClip(ref mask);
                clip.ConfigureClipFromMask(mask);
                UnityEngine.Object.DestroyImmediate(mask);
            }
            else
            {
                Debug.LogError("Cannot create default mask because the current importer doesn't have any animation information");
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern AnimationClip GetPreviewAnimationClipForTake(string takeName);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_humanDescription(out HumanDescription value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_humanDescription(ref HumanDescription value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void UpdateSkeletonPose(SkeletonBone[] skeletonBones, SerializedProperty serializedProperty);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void UpdateTransformMask(AvatarMask mask, SerializedProperty serializedProperty);

        public bool addCollider { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterAnimationCompression animationCompression { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float animationPositionError { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float animationRotationError { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float animationScaleError { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterAnimationType animationType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public WrapMode animationWrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool bakeIK { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterClipAnimation[] clipAnimations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterClipAnimation[] defaultClipAnimations { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public string[] extraExposedTransformPaths { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float fileScale { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public ModelImporterGenerateAnimations generateAnimations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use importMaterials, materialName and materialSearch instead")]
        public ModelImporterGenerateMaterials generateMaterials { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool generateSecondaryUV { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float globalScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public HumanDescription humanDescription
        {
            get
            {
                HumanDescription description;
                this.INTERNAL_get_humanDescription(out description);
                return description;
            }
            set
            {
                this.INTERNAL_set_humanDescription(ref value);
            }
        }

        public ModelImporterHumanoidOversampling humanoidOversampling { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool importAnimation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool importBlendShapes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TakeInfo[] importedTakeInfos { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool importMaterials { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterNormals importNormals { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterTangents importTangents { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal bool isAssetOlderOr42 { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isBakeIKSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isFileScaleUsed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool isTangentImportSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool isUseFileUnitsSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public ModelImporterMaterialName materialName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterMaterialSearch materialSearch { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ModelImporterMeshCompression meshCompression { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string motionNodeName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("normalImportMode is deprecated. Use importNormals instead")]
        public ModelImporterTangentSpaceMode normalImportMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float normalSmoothingAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool optimizeGameObjects { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool optimizeMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use animationCompression instead", true)]
        private bool reduceKeyframes
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public string[] referencedClips { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool resampleCurves { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("use resampleCurves instead.")]
        public bool resampleRotations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float secondaryUVAngleDistortion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float secondaryUVAreaDistortion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float secondaryUVHardAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float secondaryUVPackMargin { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Avatar sourceAvatar
        {
            get
            {
                return this.sourceAvatarInternal;
            }
            set
            {
                Avatar avatar = value;
                if (value != null)
                {
                    ModelImporter atPath = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(value)) as ModelImporter;
                    if (atPath != null)
                    {
                        this.humanDescription = atPath.humanDescription;
                    }
                    else
                    {
                        Debug.LogError("Avatar must be from a ModelImporter, otherwise use ModelImporter.humanDescription");
                        avatar = null;
                    }
                }
                this.sourceAvatarInternal = avatar;
            }
        }

        internal Avatar sourceAvatarInternal { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("splitAnimations has been deprecated please use clipAnimations instead.", true)]
        public bool splitAnimations
        {
            get
            {
                return (this.clipAnimations.Length != 0);
            }
            set
            {
            }
        }

        [Obsolete("Please use tangentImportMode instead")]
        public bool splitTangentsAcrossSeams
        {
            get
            {
                return (this.importTangents == ModelImporterTangents.CalculateLegacyWithSplitTangents);
            }
            set
            {
                if ((this.importTangents == ModelImporterTangents.CalculateLegacyWithSplitTangents) && !value)
                {
                    this.importTangents = ModelImporterTangents.CalculateLegacy;
                }
                else if ((this.importTangents == ModelImporterTangents.CalculateLegacy) && value)
                {
                    this.importTangents = ModelImporterTangents.CalculateLegacyWithSplitTangents;
                }
            }
        }

        public bool swapUVChannels { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("tangentImportMode is deprecated. Use importTangents instead")]
        public ModelImporterTangentSpaceMode tangentImportMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string[] transformPaths { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool useFileUnits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

