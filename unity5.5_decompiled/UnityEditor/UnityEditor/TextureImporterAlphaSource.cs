﻿namespace UnityEditor
{
    using System;

    public enum TextureImporterAlphaSource
    {
        None,
        FromInput,
        FromGrayScale
    }
}

