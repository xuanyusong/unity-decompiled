﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;

    internal class TreeViewItemAlphaNumericSort : IComparer<UnityEditor.TreeViewItem>
    {
        public int Compare(UnityEditor.TreeViewItem lhs, UnityEditor.TreeViewItem rhs)
        {
            if (lhs == rhs)
            {
                return 0;
            }
            if (lhs == null)
            {
                return -1;
            }
            if (rhs == null)
            {
                return 1;
            }
            return UnityEditor.EditorUtility.NaturalCompare(lhs.displayName, rhs.displayName);
        }
    }
}

