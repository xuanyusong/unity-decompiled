﻿namespace UnityEditor
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Rendering;

    public sealed class SpeedTreeImporter : AssetImporter
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <shininess>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Color <specColor>k__BackingField;
        public static readonly string[] windQualityNames = new string[] { "None", "Fastest", "Fast", "Better", "Best", "Palm" };

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void GenerateMaterials();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_hueVariation(out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_mainColor(out Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_hueVariation(ref Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_mainColor(ref Color value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void SetMaterialVersionToCurrent();

        public float alphaTestRef { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool animateCrossFading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int bestWindQuality { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public float billboardTransitionCrossFadeWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool[] castShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool[] enableBump { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool[] enableHue { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool enableSmoothLODTransition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float fadeOutWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool hasBillboard { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool hasImported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Color hueVariation
        {
            get
            {
                Color color;
                this.INTERNAL_get_hueVariation(out color);
                return color;
            }
            set
            {
                this.INTERNAL_set_hueVariation(ref value);
            }
        }

        public float[] LODHeights { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Color mainColor
        {
            get
            {
                Color color;
                this.INTERNAL_get_mainColor(out color);
                return color;
            }
            set
            {
                this.INTERNAL_set_mainColor(ref value);
            }
        }

        public string materialFolderPath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal bool materialsShouldBeRegenerated { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool[] receiveShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public ReflectionProbeUsage[] reflectionProbeUsages { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float scaleFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("shininess is no longer used and has been deprecated.", true)]
        public float shininess { get; set; }

        [Obsolete("specColor is no longer used and has been deprecated.", true)]
        public Color specColor { get; set; }

        public bool[] useLightProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int[] windQualities { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

