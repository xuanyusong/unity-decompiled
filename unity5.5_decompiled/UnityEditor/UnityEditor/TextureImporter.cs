﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Internal;

    public sealed class TextureImporter : AssetImporter
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void ClearPlatformTextureSettings(string platform);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool DoesSourceTextureHaveAlpha();
        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("DoesSourceTextureHaveColor always returns true in Unity.")]
        public extern bool DoesSourceTextureHaveColor();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern TextureImporterFormat FormatFromTextureParameters(TextureImporterSettings settings, TextureImporterPlatformSettings platformSettings, bool doesTextureContainAlpha, bool sourceWasHDR, BuildTarget destinationPlatform);
        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use UnityEditor.TextureImporter.GetPlatformTextureSettings() instead.")]
        public extern bool GetAllowsAlphaSplitting();
        public TextureImporterFormat GetAutomaticFormat(string platform)
        {
            TextureImporterSettings dest = new TextureImporterSettings();
            this.ReadTextureSettings(dest);
            TextureImporterPlatformSettings platformTextureSettings = this.GetPlatformTextureSettings(platform);
            List<BuildPlayerWindow.BuildPlatform> validPlatforms = BuildPlayerWindow.GetValidPlatforms();
            foreach (BuildPlayerWindow.BuildPlatform platform2 in validPlatforms)
            {
                if (platform2.name == platform)
                {
                    return FormatFromTextureParameters(dest, platformTextureSettings, this.DoesSourceTextureHaveAlpha(), this.IsSourceTextureHDR(), platform2.DefaultTarget);
                }
            }
            return TextureImporterFormat.Automatic;
        }

        public TextureImporterPlatformSettings GetDefaultPlatformTextureSettings()
        {
            return this.GetPlatformTextureSettings(TextureImporterInspector.s_DefaultPlatformName);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern string GetImportWarnings();
        public TextureImporterPlatformSettings GetPlatformTextureSettings(string platform)
        {
            TextureImporterPlatformSettings dest = new TextureImporterPlatformSettings();
            this.Internal_GetPlatformTextureSettings(platform, dest);
            return dest;
        }

        public bool GetPlatformTextureSettings(string platform, out int maxTextureSize, out TextureImporterFormat textureFormat)
        {
            int compressionQuality = 0;
            bool flag = false;
            return this.GetPlatformTextureSettings(platform, out maxTextureSize, out textureFormat, out compressionQuality, out flag);
        }

        public bool GetPlatformTextureSettings(string platform, out int maxTextureSize, out TextureImporterFormat textureFormat, out int compressionQuality)
        {
            bool flag = false;
            return this.GetPlatformTextureSettings(platform, out maxTextureSize, out textureFormat, out compressionQuality, out flag);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool GetPlatformTextureSettings(string platform, out int maxTextureSize, out TextureImporterFormat textureFormat, out int compressionQuality, out bool etc1AlphaSplitEnabled);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void GetWidthAndHeight(ref int width, ref int height);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_spriteBorder(out Vector4 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_spritePivot(out Vector2 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void Internal_GetPlatformTextureSettings(string platform, TextureImporterPlatformSettings dest);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_spriteBorder(ref Vector4 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_spritePivot(ref Vector2 value);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool IsETC1SupportedByBuildTarget(BuildTarget target);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern bool IsSourceTextureHDR();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool IsTextureFormatETC1Compression(TextureFormat fmt);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void ReadTextureImportInstructions(BuildTarget target, out TextureFormat desiredFormat, out ColorSpace colorSpace, out int compressionQuality);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void ReadTextureSettings(TextureImporterSettings dest);
        [MethodImpl(MethodImplOptions.InternalCall), Obsolete("Use UnityEditor.TextureImporter.SetPlatformTextureSettings() instead.")]
        public extern void SetAllowsAlphaSplitting(bool flag);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetPlatformTextureSettings(TextureImporterPlatformSettings platformSettings);
        [Obsolete("Use UnityEditor.TextureImporter.SetPlatformTextureSettings(TextureImporterPlatformSettings) instead."), ExcludeFromDocs]
        public void SetPlatformTextureSettings(string platform, int maxTextureSize, TextureImporterFormat textureFormat)
        {
            bool allowsAlphaSplit = false;
            this.SetPlatformTextureSettings(platform, maxTextureSize, textureFormat, allowsAlphaSplit);
        }

        [Obsolete("Use UnityEditor.TextureImporter.SetPlatformTextureSettings(TextureImporterPlatformSettings) instead.")]
        public void SetPlatformTextureSettings(string platform, int maxTextureSize, TextureImporterFormat textureFormat, [DefaultValue("false")] bool allowsAlphaSplit)
        {
            TextureImporterPlatformSettings dest = new TextureImporterPlatformSettings();
            this.Internal_GetPlatformTextureSettings(platform, dest);
            dest.overridden = true;
            dest.maxTextureSize = maxTextureSize;
            dest.format = textureFormat;
            dest.allowsAlphaSplitting = allowsAlphaSplit;
            this.SetPlatformTextureSettings(dest);
        }

        [Obsolete("Use UnityEditor.TextureImporter.SetPlatformTextureSettings(TextureImporterPlatformSettings) instead.")]
        public void SetPlatformTextureSettings(string platform, int maxTextureSize, TextureImporterFormat textureFormat, int compressionQuality, bool allowsAlphaSplit)
        {
            TextureImporterPlatformSettings dest = new TextureImporterPlatformSettings();
            this.Internal_GetPlatformTextureSettings(platform, dest);
            dest.overridden = true;
            dest.maxTextureSize = maxTextureSize;
            dest.format = textureFormat;
            dest.compressionQuality = compressionQuality;
            dest.allowsAlphaSplitting = allowsAlphaSplit;
            this.SetPlatformTextureSettings(dest);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetTextureSettings(TextureImporterSettings src);

        public bool allowAlphaSplitting { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool alphaIsTransparency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterAlphaSource alphaSource { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int anisoLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool borderMipmap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int compressionQuality { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool convertToNormalmap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("correctGamma Property deprecated. Mipmaps are always generated in linear space.")]
        public bool correctGamma { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool crunchedCompression { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static string defaultPlatformName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool fadeout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public UnityEngine.FilterMode filterMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterGenerateCubemap generateCubemap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("generateMipsInLinearSpace Property deprecated. Mipmaps are always generated in linear space.")]
        public bool generateMipsInLinearSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use UnityEditor.TextureImporter.alphaSource instead.")]
        public bool grayscaleToAlpha
        {
            get
            {
                return (this.alphaSource == TextureImporterAlphaSource.FromGrayScale);
            }
            set
            {
                if (value)
                {
                    this.alphaSource = TextureImporterAlphaSource.FromGrayScale;
                }
                else
                {
                    this.alphaSource = TextureImporterAlphaSource.FromInput;
                }
            }
        }

        public float heightmapScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("lightmap Property deprecated. Check [[TextureImporterSettings.textureType]] instead. Getter will work as expected. Setter will set textureType to Lightmap if true, nothing otherwise.")]
        public bool lightmap
        {
            get
            {
                return (this.textureType == TextureImporterType.Lightmap);
            }
            set
            {
                if (value)
                {
                    this.textureType = TextureImporterType.Lightmap;
                }
                else
                {
                    this.textureType = TextureImporterType.Default;
                }
            }
        }

        [Obsolete("linearTexture Property deprecated. Use sRGBTexture instead.")]
        public bool linearTexture
        {
            get
            {
                return !this.sRGBTexture;
            }
            set
            {
                this.sRGBTexture = !value;
            }
        }

        public int maxTextureSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float mipMapBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool mipmapEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int mipmapFadeDistanceEnd { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int mipmapFadeDistanceStart { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterMipFilter mipmapFilter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("normalmap Property deprecated. Check [[TextureImporterSettings.textureType]] instead. Getter will work as expected. Setter will set textureType to NormalMap if true, nothing otherwise.")]
        public bool normalmap
        {
            get
            {
                return (this.textureType == TextureImporterType.NormalMap);
            }
            set
            {
                if (value)
                {
                    this.textureType = TextureImporterType.NormalMap;
                }
                else
                {
                    this.textureType = TextureImporterType.Default;
                }
            }
        }

        public TextureImporterNormalFilter normalmapFilter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterNPOTScale npotScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool qualifiesForSpritePacking { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public Vector4 spriteBorder
        {
            get
            {
                Vector4 vector;
                this.INTERNAL_get_spriteBorder(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_spriteBorder(ref value);
            }
        }

        public SpriteImportMode spriteImportMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string spritePackingTag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Vector2 spritePivot
        {
            get
            {
                Vector2 vector;
                this.INTERNAL_get_spritePivot(out vector);
                return vector;
            }
            set
            {
                this.INTERNAL_set_spritePivot(ref value);
            }
        }

        public float spritePixelsPerUnit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("Use spritePixelsPerUnit property instead.")]
        public float spritePixelsToUnits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public SpriteMetaData[] spritesheet { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool sRGBTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterCompression textureCompression { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("textureFormat is not longer accessible at the TextureImporter level. For old 'simple' formats use the textureCompression property for the equivalent automatic choice (Uncompressed for TrueColor, Compressed and HQCommpressed for 16 bits). For platform specific formats use the [[PlatformTextureSettings]] API. Using this setter will setup various parameters to match the new automatic system as well possible. Getter will return the last value set.")]
        public TextureImporterFormat textureFormat { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterShape textureShape { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureImporterType textureType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public TextureWrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

