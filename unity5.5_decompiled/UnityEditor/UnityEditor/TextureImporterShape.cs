﻿namespace UnityEditor
{
    using System;

    [Flags]
    public enum TextureImporterShape
    {
        Texture2D = 1,
        TextureCube = 2
    }
}

