﻿namespace UnityEditor
{
    using System;

    public enum TextureImporterFormat
    {
        Alpha8 = 1,
        ARGB16 = 2,
        ARGB32 = 5,
        ASTC_RGB_10x10 = 0x34,
        ASTC_RGB_12x12 = 0x35,
        ASTC_RGB_4x4 = 0x30,
        ASTC_RGB_5x5 = 0x31,
        ASTC_RGB_6x6 = 50,
        ASTC_RGB_8x8 = 0x33,
        ASTC_RGBA_10x10 = 0x3a,
        ASTC_RGBA_12x12 = 0x3b,
        ASTC_RGBA_4x4 = 0x36,
        ASTC_RGBA_5x5 = 0x37,
        ASTC_RGBA_6x6 = 0x38,
        ASTC_RGBA_8x8 = 0x39,
        ATC_RGB4 = 0x23,
        ATC_RGBA8 = 0x24,
        Automatic = -1,
        [Obsolete("Use textureCompression property instead")]
        Automatic16bit = -2,
        [Obsolete("Use textureCompression property instead")]
        AutomaticCompressed = -1,
        [Obsolete("HDR is handled automatically now")]
        AutomaticCompressedHDR = -7,
        [Obsolete("Use crunchedCompression property instead")]
        AutomaticCrunched = -5,
        [Obsolete("HDR is handled automatically now")]
        AutomaticHDR = -6,
        [Obsolete("Use textureCompression property instead")]
        AutomaticTruecolor = -3,
        BC4 = 0x1a,
        BC5 = 0x1b,
        BC6H = 0x18,
        BC7 = 0x19,
        DXT1 = 10,
        DXT1Crunched = 0x1c,
        DXT5 = 12,
        DXT5Crunched = 0x1d,
        EAC_R = 0x29,
        EAC_R_SIGNED = 0x2a,
        EAC_RG = 0x2b,
        EAC_RG_SIGNED = 0x2c,
        ETC_RGB4 = 0x22,
        ETC2_RGB4 = 0x2d,
        ETC2_RGB4_PUNCHTHROUGH_ALPHA = 0x2e,
        ETC2_RGBA8 = 0x2f,
        PVRTC_RGB2 = 30,
        PVRTC_RGB4 = 0x20,
        PVRTC_RGBA2 = 0x1f,
        PVRTC_RGBA4 = 0x21,
        RGB16 = 7,
        RGB24 = 3,
        RGBA16 = 13,
        RGBA32 = 4,
        RGBAHalf = 0x11
    }
}

