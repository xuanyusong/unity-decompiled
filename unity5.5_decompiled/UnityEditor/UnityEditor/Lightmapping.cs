﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEditor.SceneManagement;
    using UnityEngine;

    public sealed class Lightmapping
    {
        public static OnCompletedFunction completed;

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool Bake();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool BakeAllReflectionProbesSnapshots();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool BakeAsync();
        [Obsolete("BakeLightProbesOnly has been deprecated. Use Bake instead (UnityUpgradable) -> Bake()", true)]
        public static bool BakeLightProbesOnly()
        {
            return false;
        }

        [Obsolete("BakeLightProbesOnlyAsync has been deprecated. Use BakeAsync instead (UnityUpgradable) -> BakeAsync()", true)]
        public static bool BakeLightProbesOnlyAsync()
        {
            return false;
        }

        public static void BakeMultipleScenes(string[] paths)
        {
            if (paths.Length != 0)
            {
                for (int i = 0; i < paths.Length; i++)
                {
                    for (int j = i + 1; j < paths.Length; j++)
                    {
                        if (paths[i] == paths[j])
                        {
                            throw new Exception("no duplication of scenes is allowed");
                        }
                    }
                }
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    SceneSetup[] sceneManagerSetup = EditorSceneManager.GetSceneManagerSetup();
                    EditorSceneManager.OpenScene(paths[0]);
                    for (int k = 1; k < paths.Length; k++)
                    {
                        EditorSceneManager.OpenScene(paths[k], OpenSceneMode.Additive);
                    }
                    Bake();
                    EditorSceneManager.SaveOpenScenes();
                    EditorSceneManager.RestoreSceneManagerSetup(sceneManagerSetup);
                }
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool BakeReflectionProbe(ReflectionProbe probe, string path);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool BakeReflectionProbeSnapshot(ReflectionProbe probe);
        [Obsolete("BakeSelected has been deprecated. Use Bake instead (UnityUpgradable) -> Bake()", true)]
        public static bool BakeSelected()
        {
            return false;
        }

        [Obsolete("BakeSelectedAsync has been deprecated. Use BakeAsync instead (UnityUpgradable) -> BakeAsync()", true)]
        public static bool BakeSelectedAsync()
        {
            return false;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Cancel();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Clear();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ClearDiskCache();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ClearLightingDataAsset();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void ClearPrecompSetIsDone();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void GetTerrainGIChunks(Terrain terrain, ref int numChunksX, ref int numChunksY);
        private static void Internal_CallCompletedFunctions()
        {
            if (completed != null)
            {
                completed();
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void PrintStateToConsole();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Tetrahedralize(Vector3[] positions, out int[] outIndices, out Vector3[] outPositions);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern void UpdateCachePath();

        public static bool bakedGI { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float bounceBoost { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float buildProgress { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal static ConcurrentJobsType concurrentJobsType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static string diskCachePath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal static long diskCacheSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal static bool enlightenForceUpdates { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static bool enlightenForceWhiteAlbedo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal static UnityEngine.FilterMode filterMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static GIWorkflowMode giWorkflowMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static float indirectOutputScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool isRunning { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public static LightingDataAsset lightingDataAsset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("lightmapSnapshot has been deprecated. Use lightingDataAsset instead (UnityUpgradable) -> lightingDataAsset", true)]
        public static LightmapSnapshot lightmapSnapshot
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        internal static bool openRLEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static bool realtimeGI { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        internal enum ConcurrentJobsType
        {
            Min,
            Low,
            High
        }

        public enum GIWorkflowMode
        {
            Iterative,
            OnDemand,
            Legacy
        }

        public delegate void OnCompletedFunction();
    }
}

