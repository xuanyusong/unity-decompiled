﻿namespace UnityEditor
{
    using System;
    using System.Collections;
    using UnityEngine;

    internal class CurveRendererCache
    {
        private static Hashtable m_CombiRenderers = new Hashtable();
        private static Hashtable m_NormalRenderers = new Hashtable();

        public static void ClearCurveRendererCache()
        {
            m_CombiRenderers = new Hashtable();
            m_NormalRenderers = new Hashtable();
        }

        public static UnityEditor.CurveRenderer GetCurveRenderer(AnimationClip clip, UnityEditor.EditorCurveBinding curveBinding)
        {
            if ((curveBinding.type == typeof(Transform)) && curveBinding.propertyName.StartsWith("localEulerAngles."))
            {
                int curveIndexFromName = UnityEditor.RotationCurveInterpolation.GetCurveIndexFromName(curveBinding.propertyName);
                string str = UnityEditor.CurveUtility.GetCurveGroupID(clip, curveBinding).ToString();
                UnityEditor.EulerCurveCombinedRenderer renderer = (UnityEditor.EulerCurveCombinedRenderer) m_CombiRenderers[str];
                if (renderer == null)
                {
                    renderer = new UnityEditor.EulerCurveCombinedRenderer(UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "m_LocalRotation.x")), UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "m_LocalRotation.y")), UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "m_LocalRotation.z")), UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "m_LocalRotation.w")), UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "localEulerAngles.x")), UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "localEulerAngles.y")), UnityEditor.AnimationUtility.GetEditorCurve(clip, UnityEditor.EditorCurveBinding.FloatCurve(curveBinding.path, typeof(Transform), "localEulerAngles.z")));
                    m_CombiRenderers.Add(str, renderer);
                }
                return new UnityEditor.EulerCurveRenderer(curveIndexFromName, renderer);
            }
            string key = UnityEditor.CurveUtility.GetCurveID(clip, curveBinding).ToString();
            UnityEditor.NormalCurveRenderer renderer2 = (UnityEditor.NormalCurveRenderer) m_NormalRenderers[key];
            if (renderer2 == null)
            {
                renderer2 = new UnityEditor.NormalCurveRenderer(UnityEditor.AnimationUtility.GetEditorCurve(clip, curveBinding));
                m_NormalRenderers.Add(key, renderer2);
            }
            return renderer2;
        }
    }
}

