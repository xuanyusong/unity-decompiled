﻿namespace UnityEditor
{
    using System;

    public enum WebGLExceptionSupport
    {
        None,
        ExplicitlyThrownExceptionsOnly,
        Full
    }
}

