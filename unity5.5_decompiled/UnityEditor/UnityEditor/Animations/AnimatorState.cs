﻿namespace UnityEditor.Animations
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;
    using UnityEngineInternal;

    public sealed class AnimatorState : UnityEngine.Object
    {
        private PushUndoIfNeeded undoHandler = new PushUndoIfNeeded(true);

        public AnimatorState()
        {
            Internal_Create(this);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void AddBehaviour(int instanceID);
        public AnimatorStateTransition AddExitTransition()
        {
            AnimatorStateTransition transition = this.CreateTransition(false);
            transition.isExit = true;
            this.AddTransition(transition);
            return transition;
        }

        public AnimatorStateTransition AddExitTransition(bool defaultExitTime)
        {
            AnimatorStateTransition newTransition = this.CreateTransition(false);
            newTransition.isExit = true;
            if (defaultExitTime)
            {
                this.SetDefaultTransitionExitTime(ref newTransition);
            }
            this.AddTransition(newTransition);
            return newTransition;
        }

        public T AddStateMachineBehaviour<T>() where T: StateMachineBehaviour
        {
            return (this.AddStateMachineBehaviour(typeof(T)) as T);
        }

        [TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
        public StateMachineBehaviour AddStateMachineBehaviour(System.Type stateMachineBehaviourType)
        {
            return (StateMachineBehaviour) this.Internal_AddStateMachineBehaviourWithType(stateMachineBehaviourType);
        }

        public AnimatorStateTransition AddTransition(AnimatorState destinationState)
        {
            AnimatorStateTransition transition = this.CreateTransition(false);
            transition.destinationState = destinationState;
            this.AddTransition(transition);
            return transition;
        }

        public AnimatorStateTransition AddTransition(AnimatorStateMachine destinationStateMachine)
        {
            AnimatorStateTransition transition = this.CreateTransition(false);
            transition.destinationStateMachine = destinationStateMachine;
            this.AddTransition(transition);
            return transition;
        }

        public void AddTransition(AnimatorStateTransition transition)
        {
            this.undoHandler.DoUndo(this, "Transition added");
            AnimatorStateTransition[] transitions = this.transitions;
            ArrayUtility.Add<AnimatorStateTransition>(ref transitions, transition);
            this.transitions = transitions;
        }

        public AnimatorStateTransition AddTransition(AnimatorState destinationState, bool defaultExitTime)
        {
            AnimatorStateTransition transition = this.CreateTransition(defaultExitTime);
            transition.destinationState = destinationState;
            this.AddTransition(transition);
            return transition;
        }

        public AnimatorStateTransition AddTransition(AnimatorStateMachine destinationStateMachine, bool defaultExitTime)
        {
            AnimatorStateTransition transition = this.CreateTransition(defaultExitTime);
            transition.destinationStateMachine = destinationStateMachine;
            this.AddTransition(transition);
            return transition;
        }

        private AnimatorStateTransition CreateTransition(bool setDefaultExitTime)
        {
            AnimatorStateTransition objectToAdd = new AnimatorStateTransition {
                hasExitTime = false,
                hasFixedDuration = true
            };
            if (AssetDatabase.GetAssetPath(this) != "")
            {
                AssetDatabase.AddObjectToAsset(objectToAdd, AssetDatabase.GetAssetPath(this));
            }
            objectToAdd.hideFlags = HideFlags.HideInHierarchy;
            if (setDefaultExitTime)
            {
                this.SetDefaultTransitionExitTime(ref objectToAdd);
            }
            return objectToAdd;
        }

        internal AnimatorStateMachine FindParent(AnimatorStateMachine root)
        {
            if (root.HasState(this, false))
            {
                return root;
            }
            return root.stateMachinesRecursive.Find(sm => sm.stateMachine.HasState(this, false)).stateMachine;
        }

        internal AnimatorStateTransition FindTransition(AnimatorState destinationState)
        {
            <FindTransition>c__AnonStorey0 storey = new <FindTransition>c__AnonStorey0 {
                destinationState = destinationState
            };
            return new List<AnimatorStateTransition>(this.transitions).Find(new Predicate<AnimatorStateTransition>(storey.<>m__0));
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern MonoScript GetBehaviourMonoScript(int index);
        [Obsolete("GetMotion() is obsolete. Use motion", true)]
        public Motion GetMotion()
        {
            return null;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern ScriptableObject Internal_AddStateMachineBehaviourWithType(System.Type stateMachineBehaviourType);
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Internal_Create(AnimatorState mono);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void RemoveBehaviour(int index);
        public void RemoveTransition(AnimatorStateTransition transition)
        {
            this.undoHandler.DoUndo(this, "Transition removed");
            AnimatorStateTransition[] transitions = this.transitions;
            ArrayUtility.Remove<AnimatorStateTransition>(ref transitions, transition);
            this.transitions = transitions;
            if (MecanimUtilities.AreSameAsset(this, transition))
            {
                Undo.DestroyObjectImmediate(transition);
            }
        }

        private void SetDefaultTransitionExitTime(ref AnimatorStateTransition newTransition)
        {
            newTransition.hasExitTime = true;
            if ((this.motion != null) && (this.motion.averageDuration > 0f))
            {
                float num = 0.25f / this.motion.averageDuration;
                newTransition.duration = !newTransition.hasFixedDuration ? num : 0.25f;
                newTransition.exitTime = 1f - num;
            }
        }

        public StateMachineBehaviour[] behaviours { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float cycleOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string cycleOffsetParameter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool cycleOffsetParameterActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool iKOnFeet { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool mirror { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string mirrorParameter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool mirrorParameterActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public Motion motion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public int nameHash { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal bool pushUndo
        {
            set
            {
                this.undoHandler.pushUndo = value;
            }
        }

        public float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string speedParameter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool speedParameterActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public string tag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public AnimatorStateTransition[] transitions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [Obsolete("uniqueName does not exist anymore. Consider using .name instead.", true)]
        public string uniqueName
        {
            get
            {
                return "";
            }
        }

        [Obsolete("uniqueNameHash does not exist anymore.", true)]
        public int uniqueNameHash
        {
            get
            {
                return -1;
            }
        }

        public bool writeDefaultValues { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        [CompilerGenerated]
        private sealed class <FindTransition>c__AnonStorey0
        {
            internal AnimatorState destinationState;

            internal bool <>m__0(AnimatorStateTransition t)
            {
                return (t.destinationState == this.destinationState);
            }
        }
    }
}

