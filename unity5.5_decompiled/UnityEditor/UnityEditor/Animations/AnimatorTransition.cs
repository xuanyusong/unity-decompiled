﻿namespace UnityEditor.Animations
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class AnimatorTransition : AnimatorTransitionBase
    {
        public AnimatorTransition()
        {
            Internal_Create(this);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Internal_Create(AnimatorTransition mono);
    }
}

