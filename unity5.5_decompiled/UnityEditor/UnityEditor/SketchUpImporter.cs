﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SketchUpImporter : ModelImporter
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern SketchUpImportCamera GetDefaultCamera();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern SketchUpNodeInfo[] GetNodes();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern SketchUpImportScene[] GetScenes();

        public double latitude { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public double longitude { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public double northCorrection { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

