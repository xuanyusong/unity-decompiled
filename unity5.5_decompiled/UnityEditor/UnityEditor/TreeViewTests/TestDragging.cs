﻿namespace UnityEditor.TreeViewTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;

    internal class TestDragging : UnityEditor.TreeViewDragging
    {
        [CompilerGenerated]
        private static Func<UnityEditor.TreeViewItem, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<UnityEditor.TreeViewItem, UnityEditor.TreeViewTests.BackendData.Foo> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<UnityEditor.TreeViewItem, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<UnityEditor.TreeViewItem, int> <>f__am$cache4;
        private const string k_GenericDragID = "FooDragging";
        private UnityEditor.TreeViewTests.BackendData m_BackendData;

        public TestDragging(UnityEditor.TreeView treeView, UnityEditor.TreeViewTests.BackendData data) : base(treeView)
        {
            this.m_BackendData = data;
        }

        public override UnityEditor.DragAndDropVisualMode DoDrag(UnityEditor.TreeViewItem parentItem, UnityEditor.TreeViewItem targetItem, bool perform, UnityEditor.TreeViewDragging.DropPosition dropPos)
        {
            FooDragData genericData = UnityEditor.DragAndDrop.GetGenericData("FooDragging") as FooDragData;
            UnityEditor.TreeViewTests.FooTreeViewItem item = targetItem as UnityEditor.TreeViewTests.FooTreeViewItem;
            UnityEditor.TreeViewTests.FooTreeViewItem item2 = parentItem as UnityEditor.TreeViewTests.FooTreeViewItem;
            if ((item2 == null) || (genericData == null))
            {
                return UnityEditor.DragAndDropVisualMode.None;
            }
            bool flag = this.ValidDrag(parentItem, genericData.m_DraggedItems);
            if (perform && flag)
            {
                if (<>f__am$cache1 == null)
                {
                    <>f__am$cache1 = x => x is UnityEditor.TreeViewTests.FooTreeViewItem;
                }
                if (<>f__am$cache2 == null)
                {
                    <>f__am$cache2 = x => ((UnityEditor.TreeViewTests.FooTreeViewItem) x).foo;
                }
                List<UnityEditor.TreeViewTests.BackendData.Foo> draggedItems = genericData.m_DraggedItems.Where<UnityEditor.TreeViewItem>(<>f__am$cache1).Select<UnityEditor.TreeViewItem, UnityEditor.TreeViewTests.BackendData.Foo>(<>f__am$cache2).ToList<UnityEditor.TreeViewTests.BackendData.Foo>();
                if (<>f__am$cache3 == null)
                {
                    <>f__am$cache3 = x => x is UnityEditor.TreeViewTests.FooTreeViewItem;
                }
                if (<>f__am$cache4 == null)
                {
                    <>f__am$cache4 = x => ((UnityEditor.TreeViewTests.FooTreeViewItem) x).id;
                }
                int[] selectedIDs = genericData.m_DraggedItems.Where<UnityEditor.TreeViewItem>(<>f__am$cache3).Select<UnityEditor.TreeViewItem, int>(<>f__am$cache4).ToArray<int>();
                this.m_BackendData.ReparentSelection(item2.foo, item.foo, draggedItems);
                base.m_TreeView.ReloadData();
                base.m_TreeView.SetSelection(selectedIDs, true);
            }
            return (!flag ? UnityEditor.DragAndDropVisualMode.None : UnityEditor.DragAndDropVisualMode.Move);
        }

        private List<UnityEditor.TreeViewItem> GetItemsFromIDs(IEnumerable<int> draggedItemIDs)
        {
            return UnityEditor.TreeViewUtility.FindItemsInList(draggedItemIDs, base.m_TreeView.data.GetRows());
        }

        public override void StartDrag(UnityEditor.TreeViewItem draggedNode, List<int> draggedItemIDs)
        {
            UnityEditor.DragAndDrop.PrepareStartDrag();
            UnityEditor.DragAndDrop.SetGenericData("FooDragging", new FooDragData(this.GetItemsFromIDs(draggedItemIDs)));
            UnityEditor.DragAndDrop.objectReferences = new Object[0];
            UnityEditor.DragAndDrop.StartDrag(draggedItemIDs.Count + " Foo" + ((draggedItemIDs.Count <= 1) ? string.Empty : "s"));
        }

        private bool ValidDrag(UnityEditor.TreeViewItem parent, List<UnityEditor.TreeViewItem> draggedItems)
        {
            for (UnityEditor.TreeViewItem item = parent; item != null; item = item.parent)
            {
                if (draggedItems.Contains(item))
                {
                    return false;
                }
            }
            return true;
        }

        private class FooDragData
        {
            public List<UnityEditor.TreeViewItem> m_DraggedItems;

            public FooDragData(List<UnityEditor.TreeViewItem> draggedItems)
            {
                this.m_DraggedItems = draggedItems;
            }
        }
    }
}

