﻿namespace UnityEditor.TreeViewTests
{
    using System;
    using UnityEditor;
    using UnityEngine;

    internal class TreeViewTestWithCustomHeight
    {
        private UnityEditor.TreeViewTests.BackendData m_BackendData;
        private UnityEditor.TreeView m_TreeView;

        public TreeViewTestWithCustomHeight(UnityEditor.EditorWindow editorWindow, UnityEditor.TreeViewTests.BackendData backendData, Rect rect)
        {
            UnityEditor.TreeViewTests.TestDataSource source;
            this.m_BackendData = backendData;
            UnityEditor.TreeViewState treeViewState = new UnityEditor.TreeViewState();
            this.m_TreeView = new UnityEditor.TreeView(editorWindow, treeViewState);
            UnityEditor.TreeViewTests.TestGUICustomItemHeights gui = new UnityEditor.TreeViewTests.TestGUICustomItemHeights(this.m_TreeView);
            UnityEditor.TreeViewTests.TestDragging dragging = new UnityEditor.TreeViewTests.TestDragging(this.m_TreeView, this.m_BackendData);
            source = new UnityEditor.TreeViewTests.TestDataSource(this.m_TreeView, this.m_BackendData) {
                onVisibleRowsChanged = (System.Action) Delegate.Combine(source.onVisibleRowsChanged, new System.Action(gui.CalculateRowRects))
            };
            this.m_TreeView.Init(rect, source, gui, dragging);
            source.SetExpanded(source.root, true);
        }

        public void OnGUI(Rect rect)
        {
            int controlID = GUIUtility.GetControlID(FocusType.Keyboard, rect);
            this.m_TreeView.OnGUI(rect, controlID);
        }
    }
}

