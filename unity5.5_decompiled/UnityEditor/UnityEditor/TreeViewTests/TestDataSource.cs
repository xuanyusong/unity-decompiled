﻿namespace UnityEditor.TreeViewTests
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEditor;

    internal class TestDataSource : UnityEditor.TreeViewDataSource
    {
        private UnityEditor.TreeViewTests.BackendData m_Backend;

        public TestDataSource(UnityEditor.TreeView treeView, UnityEditor.TreeViewTests.BackendData data) : base(treeView)
        {
            this.m_Backend = data;
            this.FetchData();
        }

        private void AddChildrenRecursive(UnityEditor.TreeViewTests.BackendData.Foo source, UnityEditor.TreeViewItem dest)
        {
            if (source.hasChildren)
            {
                dest.children = new List<UnityEditor.TreeViewItem>(source.children.Count);
                for (int i = 0; i < source.children.Count; i++)
                {
                    UnityEditor.TreeViewTests.BackendData.Foo foo = source.children[i];
                    dest.children.Add(new UnityEditor.TreeViewTests.FooTreeViewItem(foo.id, dest.depth + 1, dest, foo.name, foo));
                    this.itemCounter++;
                    this.AddChildrenRecursive(foo, dest.children[i]);
                }
            }
        }

        public override bool CanBeParent(UnityEditor.TreeViewItem item)
        {
            return true;
        }

        public override void FetchData()
        {
            this.itemCounter = 1;
            base.m_RootItem = new UnityEditor.TreeViewTests.FooTreeViewItem(this.m_Backend.root.id, 0, null, this.m_Backend.root.name, this.m_Backend.root);
            this.AddChildrenRecursive(this.m_Backend.root, base.m_RootItem);
            base.m_NeedRefreshVisibleFolders = true;
        }

        public int itemCounter { get; private set; }
    }
}

