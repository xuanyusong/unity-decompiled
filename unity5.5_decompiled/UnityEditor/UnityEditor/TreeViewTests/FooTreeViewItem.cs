﻿namespace UnityEditor.TreeViewTests
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEditor;

    internal class FooTreeViewItem : UnityEditor.TreeViewItem
    {
        public FooTreeViewItem(int id, int depth, UnityEditor.TreeViewItem parent, string displayName, UnityEditor.TreeViewTests.BackendData.Foo foo) : base(id, depth, parent, displayName)
        {
            this.foo = foo;
        }

        public UnityEditor.TreeViewTests.BackendData.Foo foo { get; private set; }
    }
}

