﻿namespace UnityEditor.RestService
{
    using System;
    using UnityEditor;
    using UnityEditorInternal;

    internal class PlayModeRestHandler : Handler
    {
        internal string CurrentState()
        {
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return "stopped";
            }
            return (!EditorApplication.isPaused ? "playing" : "paused");
        }

        protected override JSONValue HandleGet(Request request, JSONValue payload)
        {
            JSONValue value2 = new JSONValue();
            value2["state"] = this.CurrentState();
            return value2;
        }

        protected override JSONValue HandlePost(Request request, JSONValue payload)
        {
            JSONValue value3;
            string str = payload.Get("action").AsString();
            string str2 = this.CurrentState();
            if (str != null)
            {
                if (!(str == "play"))
                {
                    if (str == "pause")
                    {
                        EditorApplication.isPaused = true;
                        goto Label_00A3;
                    }
                    if (str == "stop")
                    {
                        EditorApplication.isPlaying = false;
                        goto Label_00A3;
                    }
                }
                else
                {
                    EditorApplication.isPlaying = true;
                    EditorApplication.isPaused = false;
                    goto Label_00A3;
                }
            }
            RestRequestException exception = new RestRequestException {
                HttpStatusCode = HttpStatusCode.BadRequest,
                RestErrorString = "Invalid action: " + str
            };
            throw exception;
        Label_00A3:
            value3 = new JSONValue();
            value3["oldstate"] = str2;
            value3["newstate"] = this.CurrentState();
            return value3;
        }

        internal static void Register()
        {
            Router.RegisterHandler("/unity/playmode", new PlayModeRestHandler());
        }
    }
}

