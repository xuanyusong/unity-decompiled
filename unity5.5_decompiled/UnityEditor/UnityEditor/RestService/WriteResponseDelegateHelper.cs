﻿namespace UnityEditor.RestService
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal sealed class WriteResponseDelegateHelper
    {
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern void DoWriteResponse(IntPtr cppResponse, UnityEditor.RestService.HttpStatusCode resultCode, string payload, IntPtr callbackData);
        internal static UnityEditor.RestService.WriteResponse MakeDelegateFor(IntPtr response, IntPtr callbackData)
        {
            <MakeDelegateFor>c__AnonStorey2A storeya = new <MakeDelegateFor>c__AnonStorey2A {
                response = response,
                callbackData = callbackData
            };
            return new UnityEditor.RestService.WriteResponse(storeya.<>m__3D);
        }

        [CompilerGenerated]
        private sealed class <MakeDelegateFor>c__AnonStorey2A
        {
            internal IntPtr callbackData;
            internal IntPtr response;

            internal void <>m__3D(UnityEditor.RestService.HttpStatusCode resultCode, string payload)
            {
                UnityEditor.RestService.WriteResponseDelegateHelper.DoWriteResponse(this.response, resultCode, payload, this.callbackData);
            }
        }
    }
}

