﻿namespace UnityEditor
{
    using System;

    public enum WSASubtarget
    {
        AnyDevice,
        PC,
        Mobile,
        HoloLens
    }
}

