﻿namespace UnityEditor
{
    using System;

    public enum TextureImporterCompression
    {
        Uncompressed,
        Compressed,
        CompressedHQ,
        CompressedLQ
    }
}

