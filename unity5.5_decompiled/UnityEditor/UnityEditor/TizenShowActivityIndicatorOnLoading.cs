﻿namespace UnityEditor
{
    using System;

    public enum TizenShowActivityIndicatorOnLoading
    {
        DontShow = -1,
        InversedLarge = 1,
        InversedSmall = 3,
        Large = 0,
        Small = 2
    }
}

