﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    internal static class TreeViewUtility
    {
        public static void DebugPrintToEditorLogRecursive(UnityEditor.TreeViewItem item)
        {
            if (item != null)
            {
                Console.WriteLine(new string(' ', item.depth * 3) + item.displayName);
                if (item.hasChildren)
                {
                    foreach (UnityEditor.TreeViewItem item2 in item.children)
                    {
                        DebugPrintToEditorLogRecursive(item2);
                    }
                }
            }
        }

        public static UnityEditor.TreeViewItem FindItem(int id, UnityEditor.TreeViewItem searchFromThisItem)
        {
            return FindItemRecursive(id, searchFromThisItem);
        }

        public static UnityEditor.TreeViewItem FindItemInList<T>(int id, List<T> treeViewItems) where T: UnityEditor.TreeViewItem
        {
            <FindItemInList>c__AnonStorey7B<T> storeyb = new <FindItemInList>c__AnonStorey7B<T> {
                id = id
            };
            return treeViewItems.FirstOrDefault<T>(new Func<T, bool>(storeyb.<>m__117));
        }

        private static UnityEditor.TreeViewItem FindItemRecursive(int id, UnityEditor.TreeViewItem item)
        {
            if (item != null)
            {
                if (item.id == id)
                {
                    return item;
                }
                if (!item.hasChildren)
                {
                    return null;
                }
                foreach (UnityEditor.TreeViewItem item2 in item.children)
                {
                    UnityEditor.TreeViewItem item3 = FindItemRecursive(id, item2);
                    if (item3 != null)
                    {
                        return item3;
                    }
                }
            }
            return null;
        }

        public static List<UnityEditor.TreeViewItem> FindItemsInList(IEnumerable<int> itemIDs, List<UnityEditor.TreeViewItem> treeViewItems)
        {
            <FindItemsInList>c__AnonStorey7A storeya = new <FindItemsInList>c__AnonStorey7A {
                itemIDs = itemIDs
            };
            return treeViewItems.Where<UnityEditor.TreeViewItem>(new Func<UnityEditor.TreeViewItem, bool>(storeya.<>m__116)).ToList<UnityEditor.TreeViewItem>();
        }

        public static void SetChildParentReferences(List<UnityEditor.TreeViewItem> visibleItems, UnityEditor.TreeViewItem root)
        {
            for (int i = 0; i < visibleItems.Count; i++)
            {
                visibleItems[i].parent = null;
            }
            int capacity = 0;
            for (int j = 0; j < visibleItems.Count; j++)
            {
                SetChildParentReferences(j, visibleItems);
                if (visibleItems[j].parent == null)
                {
                    capacity++;
                }
            }
            if (capacity > 0)
            {
                List<UnityEditor.TreeViewItem> list = new List<UnityEditor.TreeViewItem>(capacity);
                for (int k = 0; k < visibleItems.Count; k++)
                {
                    if (visibleItems[k].parent == null)
                    {
                        list.Add(visibleItems[k]);
                        visibleItems[k].parent = root;
                    }
                }
                root.children = list;
            }
        }

        private static void SetChildParentReferences(int parentIndex, List<UnityEditor.TreeViewItem> visibleItems)
        {
            UnityEditor.TreeViewItem item = visibleItems[parentIndex];
            if (((item.children == null) || (item.children.Count <= 0)) || (item.children[0] == null))
            {
                int depth = item.depth;
                int capacity = 0;
                for (int i = parentIndex + 1; i < visibleItems.Count; i++)
                {
                    if (visibleItems[i].depth == (depth + 1))
                    {
                        capacity++;
                    }
                    if (visibleItems[i].depth <= depth)
                    {
                        break;
                    }
                }
                List<UnityEditor.TreeViewItem> newChildList = null;
                if (capacity != 0)
                {
                    newChildList = new List<UnityEditor.TreeViewItem>(capacity);
                    capacity = 0;
                    for (int j = parentIndex + 1; j < visibleItems.Count; j++)
                    {
                        if (visibleItems[j].depth == (depth + 1))
                        {
                            visibleItems[j].parent = item;
                            newChildList.Add(visibleItems[j]);
                            capacity++;
                        }
                        if (visibleItems[j].depth <= depth)
                        {
                            break;
                        }
                    }
                }
                SetChildren(item, newChildList);
            }
        }

        private static void SetChildren(UnityEditor.TreeViewItem item, List<UnityEditor.TreeViewItem> newChildList)
        {
            if (!UnityEditor.LazyTreeViewDataSource.IsChildListForACollapsedParent(item.children) || (newChildList != null))
            {
                item.children = newChildList;
            }
        }

        [CompilerGenerated]
        private sealed class <FindItemInList>c__AnonStorey7B<T> where T: UnityEditor.TreeViewItem
        {
            internal int id;

            internal bool <>m__117(T t)
            {
                return (t.id == this.id);
            }
        }

        [CompilerGenerated]
        private sealed class <FindItemsInList>c__AnonStorey7A
        {
            internal IEnumerable<int> itemIDs;

            internal bool <>m__116(UnityEditor.TreeViewItem x)
            {
                return this.itemIDs.Contains<int>(x.id);
            }
        }
    }
}

