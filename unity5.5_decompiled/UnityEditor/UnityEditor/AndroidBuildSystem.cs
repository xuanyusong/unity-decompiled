﻿namespace UnityEditor
{
    using System;

    public enum AndroidBuildSystem
    {
        Internal,
        Gradle,
        ADT
    }
}

