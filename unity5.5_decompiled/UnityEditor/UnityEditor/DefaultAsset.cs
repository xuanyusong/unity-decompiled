﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class DefaultAsset : UnityEngine.Object
    {
        internal bool isWarning { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal string message { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

