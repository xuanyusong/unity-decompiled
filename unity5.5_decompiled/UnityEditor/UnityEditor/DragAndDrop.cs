﻿namespace UnityEditor
{
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class DragAndDrop
    {
        private static Hashtable ms_GenericData;

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void AcceptDrag();
        public static object GetGenericData(string type)
        {
            if ((ms_GenericData != null) && ms_GenericData.Contains(type))
            {
                return ms_GenericData[type];
            }
            return null;
        }

        internal static bool HandleDelayedDrag(Rect position, int id, UnityEngine.Object objectToDrag)
        {
            Event current = Event.current;
            EventType typeForControl = current.GetTypeForControl(id);
            if (typeForControl != EventType.MouseDown)
            {
                if ((typeForControl == EventType.MouseDrag) && (GUIUtility.hotControl == id))
                {
                    DragAndDropDelay stateObject = (DragAndDropDelay) GUIUtility.GetStateObject(typeof(DragAndDropDelay), id);
                    if (stateObject.CanStartDrag())
                    {
                        GUIUtility.hotControl = 0;
                        PrepareStartDrag();
                        UnityEngine.Object[] objArray = new UnityEngine.Object[] { objectToDrag };
                        objectReferences = objArray;
                        StartDrag(ObjectNames.GetDragAndDropTitle(objectToDrag));
                        return true;
                    }
                }
            }
            else if ((position.Contains(current.mousePosition) && (current.clickCount == 1)) && ((current.button == 0) && ((Application.platform != RuntimePlatform.OSXEditor) || !current.control)))
            {
                GUIUtility.hotControl = id;
                DragAndDropDelay delay = (DragAndDropDelay) GUIUtility.GetStateObject(typeof(DragAndDropDelay), id);
                delay.mouseDownPosition = current.mousePosition;
                return true;
            }
            return false;
        }

        public static void PrepareStartDrag()
        {
            ms_GenericData = null;
            PrepareStartDrag_Internal();
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void PrepareStartDrag_Internal();
        public static void SetGenericData(string type, object data)
        {
            if (ms_GenericData == null)
            {
                ms_GenericData = new Hashtable();
            }
            ms_GenericData[type] = data;
        }

        public static void StartDrag(string title)
        {
            if ((Event.current.type == EventType.MouseDown) || (Event.current.type == EventType.MouseDrag))
            {
                StartDrag_Internal(title);
            }
            else
            {
                Debug.LogError("Drags can only be started from MouseDown or MouseDrag events");
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void StartDrag_Internal(string title);

        public static int activeControlID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static UnityEngine.Object[] objectReferences { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static string[] paths { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public static DragAndDropVisualMode visualMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

