﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MovieImporter : AssetImporter
    {
        public float duration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public bool linearTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public float quality { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
    }
}

