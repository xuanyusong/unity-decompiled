﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class SerializedObject
    {
        private IntPtr m_Property;

        public SerializedObject(UnityEngine.Object obj)
        {
            UnityEngine.Object[] monoObjs = new UnityEngine.Object[] { obj };
            this.InternalCreate(monoObjs);
        }

        public SerializedObject(UnityEngine.Object[] objs)
        {
            this.InternalCreate(objs);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool ApplyModifiedProperties();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern bool ApplyModifiedPropertiesWithoutUndo();
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void Cache(int instanceID);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void CopyFromSerializedProperty(SerializedProperty prop);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe]
        public extern void Dispose();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern PropertyModification ExtractPropertyModification(string propertyPath);
        ~SerializedObject()
        {
            this.Dispose();
        }

        public SerializedProperty FindProperty(string propertyPath)
        {
            SerializedProperty property = this.GetIterator_Internal();
            property.m_SerializedObject = this;
            if (property.FindPropertyInternal(propertyPath))
            {
                return property;
            }
            return null;
        }

        public SerializedProperty GetIterator()
        {
            SerializedProperty property = this.GetIterator_Internal();
            property.m_SerializedObject = this;
            return property;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern SerializedProperty GetIterator_Internal();
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void InternalCreate(UnityEngine.Object[] monoObjs);
        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern SerializedObject LoadFromCache(int instanceID);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void SetIsDifferentCacheDirty();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void Update();
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern void UpdateIfDirtyOrScript();

        internal bool hasModifiedProperties { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        internal InspectorMode inspectorMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public bool isEditingMultipleObjects { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public int maxArraySizeForMultiEditing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

        public UnityEngine.Object targetObject { [MethodImpl(MethodImplOptions.InternalCall)] get; }

        public UnityEngine.Object[] targetObjects { [MethodImpl(MethodImplOptions.InternalCall)] get; }
    }
}

