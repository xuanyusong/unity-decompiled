﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal interface ITreeViewDataSource
    {
        bool CanBeMultiSelected(UnityEditor.TreeViewItem item);
        bool CanBeParent(UnityEditor.TreeViewItem item);
        UnityEditor.TreeViewItem FindItem(int id);
        int[] GetExpandedIDs();
        UnityEditor.TreeViewItem GetItem(int row);
        int GetRow(int id);
        List<UnityEditor.TreeViewItem> GetRows();
        bool HasFakeItem();
        void InitIfNeeded();
        void InsertFakeItem(int id, int parentID, string name, Texture2D icon);
        bool IsExpandable(UnityEditor.TreeViewItem item);
        bool IsExpanded(UnityEditor.TreeViewItem item);
        bool IsRenamingItemAllowed(UnityEditor.TreeViewItem item);
        bool IsRevealed(int id);
        void OnInitialize();
        void OnSearchChanged();
        void ReloadData();
        void RemoveFakeItem();
        void RevealItem(int id);
        void SetExpanded(UnityEditor.TreeViewItem item, bool expand);
        void SetExpandedIDs(int[] ids);
        void SetExpandedWithChildren(UnityEditor.TreeViewItem item, bool expand);

        UnityEditor.TreeViewItem root { get; }

        int rowCount { get; }
    }
}

