﻿namespace UnityEditor.Collaboration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEditor.Connect;
    using UnityEditor.Web;
    using UnityEditorInternal;
    using UnityEngine;

    [InitializeOnLoad]
    internal sealed class Collab : AssetPostprocessor
    {
        public static string[] clientType = new string[] { "Cloud Server", "Mock Server" };
        public string[] currentProjectBrowserSelection;
        internal static string editorPrefCollabClientType = "CollabConfig_Client";
        private static Collab s_Instance = new Collab();
        private static bool s_IsFirstStateChange = true;

        public event StateChangedDelegate StateChanged;

        static Collab()
        {
            s_Instance.projectBrowserSingleSelectionPath = string.Empty;
            s_Instance.projectBrowserSingleMetaSelectionPath = string.Empty;
            JSProxyMgr.GetInstance().AddGlobalObject("unity/collab", s_Instance);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void CancelJob(int jobID);
        public void CancelJobWithoutException(int jobId)
        {
            try
            {
                this.CancelJob(jobId);
            }
            catch (Exception exception)
            {
                Debug.Log("Cannot cancel job, reason:" + exception.Message);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern bool ClearConflictResolved(string path);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern bool ClearConflictsResolved(string[] paths);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void ClearErrors();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void Disconnect();
        public CollabStates GetAssetState(string guid)
        {
            return (CollabStates) this.GetAssetStateInternal(guid);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private extern long GetAssetStateInternal(string guid);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern Change[] GetChangesToPublish();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern Change[] GetCollabConflicts();
        public CollabInfo GetCollabInfo()
        {
            return this.collabInfo;
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern ProgressInfo GetJobProgress(int jobID);
        public static string GetProjectClientType()
        {
            string configValue = EditorUserSettings.GetConfigValue(editorPrefCollabClientType);
            return (!string.IsNullOrEmpty(configValue) ? configValue : clientType[0]);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall, ThreadAndSerializationSafe]
        public extern string GetProjectPath();
        public CollabStates GetSelectedAssetState()
        {
            return (CollabStates) this.GetSelectedAssetStateInternal();
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        private extern long GetSelectedAssetStateInternal();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void GoBackToRevision(string revisionID, bool updateToRevision);
        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe, WrapperlessIcall]
        public extern bool IsConnected();
        public static bool IsDiffToolsAvailable()
        {
            return (InternalEditorUtility.GetAvailableDiffTools().Length > 0);
        }

        [MethodImpl(MethodImplOptions.InternalCall), ThreadAndSerializationSafe, WrapperlessIcall]
        public extern bool JobRunning(int a_jobID);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void LaunchConflictExternalMerge(string path);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void OnPostprocessAssetbundleNameChanged(string assetPath, string previousAssetBundleName, string newAssetBundleName);
        private static void OnStateChanged()
        {
            if (s_IsFirstStateChange)
            {
                s_IsFirstStateChange = false;
                UnityConnect.instance.StateChanged += new StateChangedDelegate(Collab.OnUnityConnectStateChanged);
            }
            StateChangedDelegate stateChanged = instance.StateChanged;
            if (stateChanged != null)
            {
                stateChanged(instance.collabInfo);
            }
        }

        private static void OnUnityConnectStateChanged(ConnectInfo state)
        {
            instance.SendNotification();
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void Publish(string comment);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void ResyncSnapshot();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void ResyncToRevision(string revisionID);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void RevertFile(string path, bool forceOverwrite);
        public void SaveAssets()
        {
            AssetDatabase.SaveAssets();
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void SendNotification();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void SetCollabEnabledForCurrentProject(bool enabled);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern bool SetConflictResolvedMine(string path);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern bool SetConflictResolvedTheirs(string path);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern bool SetConflictsResolvedMine(string[] paths);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern bool SetConflictsResolvedTheirs(string[] paths);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void ShowConflictDifferences(string path);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void ShowDifferences(string path);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void Update(string revisionID, bool updateToRevision);
        public void UpdateEditorSelectionCache()
        {
            List<string> list = new List<string>();
            foreach (string str in Selection.assetGUIDsDeepSelection)
            {
                string item = AssetDatabase.GUIDToAssetPath(str);
                list.Add(item);
                string path = item + ".meta";
                if (File.Exists(path))
                {
                    list.Add(path);
                }
            }
            this.currentProjectBrowserSelection = list.ToArray();
        }

        public CollabInfo collabInfo { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public static Collab instance
        {
            get
            {
                return s_Instance;
            }
        }

        public string projectBrowserSingleMetaSelectionPath { get; set; }

        public string projectBrowserSingleSelectionPath { get; set; }

        [Flags]
        public enum CollabStates : ulong
        {
            kCollabAddedLocal = 0x100L,
            kCollabAddedRemote = 0x200L,
            kCollabChanges = 0x40000L,
            kCollabCheckedOutLocal = 0x10L,
            kCollabCheckedOutRemote = 0x20L,
            kCollabConflicted = 0x400L,
            kCollabDeletedLocal = 0x40L,
            kCollabDeletedRemote = 0x80L,
            kCollabFolderMetaFile = 0x200000L,
            kCollabInvalidState = 0x400000L,
            kCollabLocal = 1L,
            kCollabMerged = 0x80000L,
            kCollabMetaFile = 0x8000L,
            kCollabMissing = 8L,
            kCollabMovedLocal = 0x800L,
            kCollabMovedRemote = 0x1000L,
            kCollabNone = 0L,
            kCollabOutOfSync = 4L,
            kCollabPendingMerge = 0x100000L,
            kCollabReadOnly = 0x4000L,
            kCollabSynced = 2L,
            kCollabUpdating = 0x2000L,
            kCollabUseMine = 0x10000L,
            kCollabUseTheir = 0x20000L
        }
    }
}

