﻿namespace UnityEditor.RestService
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal sealed class Response
    {
        private IntPtr m_nativeRequestPtr;

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void SimpleResponse(HttpStatusCode status, string payload);
    }
}

