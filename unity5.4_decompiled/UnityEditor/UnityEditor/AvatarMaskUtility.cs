﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEditor.Animations;

    internal class AvatarMaskUtility
    {
        private static string sBoneName = "m_BoneName";
        private static string sHuman = "m_HumanDescription.m_Human";

        public static string[] GetAvatarHumanTransform(SerializedObject so, string[] refTransformsPath)
        {
            SerializedProperty property = so.FindProperty(sHuman);
            if ((property == null) || !property.isArray)
            {
                return null;
            }
            string[] array = new string[0];
            for (int i = 0; i < property.arraySize; i++)
            {
                SerializedProperty property2 = property.GetArrayElementAtIndex(i).FindPropertyRelative(sBoneName);
                ArrayUtility.Add<string>(ref array, property2.stringValue);
            }
            return TokeniseHumanTransformsPath(refTransformsPath, array);
        }

        public static void SetActiveHumanTransforms(AvatarMask mask, string[] humanTransforms)
        {
            for (int i = 0; i < mask.transformCount; i++)
            {
                <SetActiveHumanTransforms>c__AnonStorey98 storey = new <SetActiveHumanTransforms>c__AnonStorey98 {
                    path = mask.GetTransformPath(i)
                };
                if (ArrayUtility.FindIndex<string>(humanTransforms, new Predicate<string>(storey.<>m__16E)) != -1)
                {
                    mask.SetTransformActive(i, true);
                }
            }
        }

        private static string[] TokeniseHumanTransformsPath(string[] refTransformsPath, string[] humanTransforms)
        {
            <TokeniseHumanTransformsPath>c__AnonStorey99 storey = new <TokeniseHumanTransformsPath>c__AnonStorey99 {
                humanTransforms = humanTransforms
            };
            if (storey.humanTransforms == null)
            {
                return null;
            }
            string[] array = new string[] { string.Empty };
            <TokeniseHumanTransformsPath>c__AnonStorey9A storeya = new <TokeniseHumanTransformsPath>c__AnonStorey9A {
                <>f__ref$153 = storey,
                i = 0
            };
            while (storeya.i < storey.humanTransforms.Length)
            {
                int index = ArrayUtility.FindIndex<string>(refTransformsPath, new Predicate<string>(storeya.<>m__16F));
                if (index != -1)
                {
                    <TokeniseHumanTransformsPath>c__AnonStorey9B storeyb = new <TokeniseHumanTransformsPath>c__AnonStorey9B();
                    int length = array.Length;
                    storeyb.path = refTransformsPath[index];
                    while (storeyb.path.Length > 0)
                    {
                        if (ArrayUtility.FindIndex<string>(array, new Predicate<string>(storeyb.<>m__170)) == -1)
                        {
                            ArrayUtility.Insert<string>(ref array, length, storeyb.path);
                        }
                        int num4 = storeyb.path.LastIndexOf('/');
                        storeyb.path = storeyb.path.Substring(0, (num4 == -1) ? 0 : num4);
                    }
                }
                storeya.i++;
            }
            return array;
        }

        public static void UpdateTransformMask(AvatarMask mask, string[] refTransformsPath, string[] humanTransforms)
        {
            <UpdateTransformMask>c__AnonStorey94 storey = new <UpdateTransformMask>c__AnonStorey94 {
                refTransformsPath = refTransformsPath
            };
            mask.transformCount = storey.refTransformsPath.Length;
            <UpdateTransformMask>c__AnonStorey95 storey2 = new <UpdateTransformMask>c__AnonStorey95 {
                <>f__ref$148 = storey,
                i = 0
            };
            while (storey2.i < storey.refTransformsPath.Length)
            {
                mask.SetTransformPath(storey2.i, storey.refTransformsPath[storey2.i]);
                bool flag = (humanTransforms != null) ? (ArrayUtility.FindIndex<string>(humanTransforms, new Predicate<string>(storey2.<>m__16C)) != -1) : true;
                mask.SetTransformActive(storey2.i, flag);
                storey2.i++;
            }
        }

        public static void UpdateTransformMask(SerializedProperty transformMask, string[] refTransformsPath, string[] humanTransforms)
        {
            <UpdateTransformMask>c__AnonStorey96 storey = new <UpdateTransformMask>c__AnonStorey96 {
                refTransformsPath = refTransformsPath
            };
            AvatarMask mask = new AvatarMask {
                transformCount = storey.refTransformsPath.Length
            };
            <UpdateTransformMask>c__AnonStorey97 storey2 = new <UpdateTransformMask>c__AnonStorey97 {
                <>f__ref$150 = storey,
                i = 0
            };
            while (storey2.i < storey.refTransformsPath.Length)
            {
                bool flag = (humanTransforms != null) ? (ArrayUtility.FindIndex<string>(humanTransforms, new Predicate<string>(storey2.<>m__16D)) != -1) : true;
                mask.SetTransformPath(storey2.i, storey.refTransformsPath[storey2.i]);
                mask.SetTransformActive(storey2.i, flag);
                storey2.i++;
            }
            ModelImporter.UpdateTransformMask(mask, transformMask);
        }

        [CompilerGenerated]
        private sealed class <SetActiveHumanTransforms>c__AnonStorey98
        {
            internal string path;

            internal bool <>m__16E(string s)
            {
                return (this.path == s);
            }
        }

        [CompilerGenerated]
        private sealed class <TokeniseHumanTransformsPath>c__AnonStorey99
        {
            internal string[] humanTransforms;
        }

        [CompilerGenerated]
        private sealed class <TokeniseHumanTransformsPath>c__AnonStorey9A
        {
            internal AvatarMaskUtility.<TokeniseHumanTransformsPath>c__AnonStorey99 <>f__ref$153;
            internal int i;

            internal bool <>m__16F(string s)
            {
                return (this.<>f__ref$153.humanTransforms[this.i] == FileUtil.GetLastPathNameComponent(s));
            }
        }

        [CompilerGenerated]
        private sealed class <TokeniseHumanTransformsPath>c__AnonStorey9B
        {
            internal string path;

            internal bool <>m__170(string s)
            {
                return (this.path == s);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateTransformMask>c__AnonStorey94
        {
            internal string[] refTransformsPath;
        }

        [CompilerGenerated]
        private sealed class <UpdateTransformMask>c__AnonStorey95
        {
            internal AvatarMaskUtility.<UpdateTransformMask>c__AnonStorey94 <>f__ref$148;
            internal int i;

            internal bool <>m__16C(string s)
            {
                return (this.<>f__ref$148.refTransformsPath[this.i] == s);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateTransformMask>c__AnonStorey96
        {
            internal string[] refTransformsPath;
        }

        [CompilerGenerated]
        private sealed class <UpdateTransformMask>c__AnonStorey97
        {
            internal AvatarMaskUtility.<UpdateTransformMask>c__AnonStorey96 <>f__ref$150;
            internal int i;

            internal bool <>m__16D(string s)
            {
                return (this.<>f__ref$150.refTransformsPath[this.i] == s);
            }
        }
    }
}

