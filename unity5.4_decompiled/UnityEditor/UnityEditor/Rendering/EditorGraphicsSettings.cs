﻿namespace UnityEditor.Rendering
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Rendering;

    public sealed class EditorGraphicsSettings
    {
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern bool AreShaderSettingsAutomatic(BuildTargetGroup target, ShaderHardwareTier tier);
        public static PlatformShaderSettings GetShaderSettingsForPlatform(BuildTargetGroup target, ShaderHardwareTier tier)
        {
            return GetShaderSettingsForPlatformImpl(target, tier);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern PlatformShaderSettings GetShaderSettingsForPlatformImpl(BuildTargetGroup target, ShaderHardwareTier tier);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern void MakeShaderSettingsAutomatic(BuildTargetGroup target, ShaderHardwareTier tier, bool automatic);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern void OnUpdateShaderSettingsForPlatformImpl(BuildTargetGroup target, bool shouldReloadShaders);
        public static void SetShaderSettingsForPlatform(BuildTargetGroup target, ShaderHardwareTier tier, PlatformShaderSettings settings)
        {
            SetShaderSettingsForPlatformImpl(target, tier, settings);
            OnUpdateShaderSettingsForPlatformImpl(target, true);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal static extern void SetShaderSettingsForPlatformImpl(BuildTargetGroup target, ShaderHardwareTier tier, PlatformShaderSettings settings);
    }
}

