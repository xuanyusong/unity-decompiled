﻿namespace UnityEditor.Modules
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;

    internal class DefaultTextureImportSettingsExtension : ITextureImportSettingsExtension
    {
        [CompilerGenerated]
        private static Func<GUIContent, string> <>f__am$cache3;
        private static readonly string[] kMaxTextureSizeStrings = new string[] { "32", "64", "128", "256", "512", "1024", "2048", "4096", "8192" };
        private static readonly int[] kMaxTextureSizeValues = new int[] { 0x20, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000 };
        private static readonly GUIContent maxSize = EditorGUIUtility.TextContent("Max Size|Textures larger than this will be scaled down.");

        private int EditCompressionQuality(BuildTarget target, int compression)
        {
            if ((((target == BuildTarget.iOS) || (target == BuildTarget.tvOS)) || ((target == BuildTarget.Android) || (target == BuildTarget.Tizen))) || (target == BuildTarget.SamsungTV))
            {
                int selectedIndex = 1;
                if (compression == 0)
                {
                    selectedIndex = 0;
                }
                else if (compression == 100)
                {
                    selectedIndex = 2;
                }
                switch (EditorGUILayout.Popup(TextureImporterInspector.s_Styles.compressionQuality, selectedIndex, TextureImporterInspector.s_Styles.mobileCompressionQualityOptions, new GUILayoutOption[0]))
                {
                    case 0:
                        return 0;

                    case 1:
                        return 50;

                    case 2:
                        return 100;
                }
                return 50;
            }
            compression = EditorGUILayout.IntSlider(TextureImporterInspector.s_Styles.compressionQualitySlider, compression, 0, 100, new GUILayoutOption[0]);
            return compression;
        }

        public virtual void ShowImportSettings(Editor baseEditor, TextureImportPlatformSettings platformSettings)
        {
            TextureImporterInspector inspector = baseEditor as TextureImporterInspector;
            EditorGUI.BeginChangeCheck();
            EditorGUI.showMixedValue = platformSettings.overriddenIsDifferent || platformSettings.maxTextureSizeIsDifferent;
            int maxTextureSize = EditorGUILayout.IntPopup(maxSize.text, platformSettings.maxTextureSize, kMaxTextureSizeStrings, kMaxTextureSizeValues, new GUILayoutOption[0]);
            EditorGUI.showMixedValue = false;
            if (EditorGUI.EndChangeCheck())
            {
                platformSettings.SetMaxTextureSizeForAll(maxTextureSize);
            }
            int[] second = null;
            string[] strArray = null;
            bool flag = false;
            int selectedValue = 0;
            bool flag2 = false;
            int num3 = 0;
            bool flag3 = false;
            for (int i = 0; i < inspector.targets.Length; i++)
            {
                TextureImporter importer = inspector.targets[i] as TextureImporter;
                TextureImporterType tType = !inspector.textureTypeHasMultipleDifferentValues ? inspector.textureType : importer.textureType;
                TextureImporterSettings settings = platformSettings.GetSettings(importer);
                int num5 = (int) platformSettings.textureFormats[i];
                int num6 = num5;
                if (!platformSettings.isDefault && (num5 < 0))
                {
                    num6 = (int) TextureImporter.SimpleToFullTextureFormat2((TextureImporterFormat) num6, tType, settings, importer.DoesSourceTextureHaveAlpha(), importer.IsSourceTextureHDR(), platformSettings.m_Target);
                }
                if (settings.normalMap && !TextureImporterInspector.IsGLESMobileTargetPlatform(platformSettings.m_Target))
                {
                    num6 = (int) TextureImporterInspector.MakeTextureFormatHaveAlpha((TextureImporterFormat) num6);
                }
                TextureImporterType type2 = tType;
                if (type2 != TextureImporterType.Cookie)
                {
                    if (type2 == TextureImporterType.Advanced)
                    {
                        goto Label_0157;
                    }
                    goto Label_032D;
                }
                int[] first = new int[1];
                string[] strArray2 = new string[] { "8 Bit Alpha" };
                num5 = 0;
                goto Label_0384;
            Label_0157:
                num5 = num6;
                if (TextureImporterInspector.IsGLESMobileTargetPlatform(platformSettings.m_Target))
                {
                    if (TextureImporterInspector.s_TextureFormatStringsiPhone == null)
                    {
                        TextureImporterInspector.s_TextureFormatStringsiPhone = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kTextureFormatsValueiPhone);
                    }
                    if (TextureImporterInspector.s_TextureFormatStringsAndroid == null)
                    {
                        TextureImporterInspector.s_TextureFormatStringsAndroid = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kTextureFormatsValueAndroid);
                    }
                    if (TextureImporterInspector.s_TextureFormatStringsTizen == null)
                    {
                        TextureImporterInspector.s_TextureFormatStringsTizen = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kTextureFormatsValueTizen);
                    }
                    if (platformSettings.m_Target == BuildTarget.iOS)
                    {
                        first = TextureImportPlatformSettings.kTextureFormatsValueiPhone;
                        strArray2 = TextureImporterInspector.s_TextureFormatStringsiPhone;
                    }
                    else if (platformSettings.m_Target == BuildTarget.SamsungTV)
                    {
                        if (TextureImporterInspector.s_TextureFormatStringsSTV == null)
                        {
                            TextureImporterInspector.s_TextureFormatStringsSTV = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kTextureFormatsValueSTV);
                        }
                        first = TextureImportPlatformSettings.kTextureFormatsValueSTV;
                        strArray2 = TextureImporterInspector.s_TextureFormatStringsSTV;
                    }
                    else
                    {
                        first = TextureImportPlatformSettings.kTextureFormatsValueAndroid;
                        strArray2 = TextureImporterInspector.s_TextureFormatStringsAndroid;
                    }
                }
                else if (!settings.normalMap)
                {
                    if (TextureImporterInspector.s_TextureFormatStringsAll == null)
                    {
                        TextureImporterInspector.s_TextureFormatStringsAll = TextureImporterInspector.BuildTextureStrings(TextureImporterInspector.TextureFormatsValueAll);
                    }
                    if (TextureImporterInspector.s_TextureFormatStringsWiiU == null)
                    {
                        TextureImporterInspector.s_TextureFormatStringsWiiU = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kTextureFormatsValueWiiU);
                    }
                    if (TextureImporterInspector.s_TextureFormatStringsWeb == null)
                    {
                        TextureImporterInspector.s_TextureFormatStringsWeb = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kTextureFormatsValueWeb);
                    }
                    if (platformSettings.isDefault)
                    {
                        first = TextureImporterInspector.TextureFormatsValueAll;
                        strArray2 = TextureImporterInspector.s_TextureFormatStringsAll;
                    }
                    else if (platformSettings.m_Target == BuildTarget.WiiU)
                    {
                        first = TextureImportPlatformSettings.kTextureFormatsValueWiiU;
                        strArray2 = TextureImporterInspector.s_TextureFormatStringsWiiU;
                    }
                    else
                    {
                        first = TextureImportPlatformSettings.kTextureFormatsValueWeb;
                        strArray2 = TextureImporterInspector.s_TextureFormatStringsWeb;
                    }
                }
                else
                {
                    if (TextureImporterInspector.s_NormalFormatStringsAll == null)
                    {
                        TextureImporterInspector.s_NormalFormatStringsAll = TextureImporterInspector.BuildTextureStrings(TextureImporterInspector.NormalFormatsValueAll);
                    }
                    if (TextureImporterInspector.s_NormalFormatStringsWeb == null)
                    {
                        TextureImporterInspector.s_NormalFormatStringsWeb = TextureImporterInspector.BuildTextureStrings(TextureImportPlatformSettings.kNormalFormatsValueWeb);
                    }
                    if (platformSettings.isDefault)
                    {
                        first = TextureImporterInspector.NormalFormatsValueAll;
                        strArray2 = TextureImporterInspector.s_NormalFormatStringsAll;
                    }
                    else
                    {
                        first = TextureImportPlatformSettings.kNormalFormatsValueWeb;
                        strArray2 = TextureImporterInspector.s_NormalFormatStringsWeb;
                    }
                }
                goto Label_0384;
            Label_032D:
                first = inspector.m_TextureFormatValues;
                if (<>f__am$cache3 == null)
                {
                    <>f__am$cache3 = content => content.text;
                }
                strArray2 = TextureImporterInspector.s_Styles.textureFormatOptions.Select<GUIContent, string>(<>f__am$cache3).ToArray<string>();
                if (num5 >= 0)
                {
                    num5 = (int) TextureImporter.FullToSimpleTextureFormat((TextureImporterFormat) num5);
                }
                num5 = -1 - num5;
            Label_0384:
                if (i == 0)
                {
                    second = first;
                    strArray = strArray2;
                    selectedValue = num5;
                    num3 = num6;
                }
                else
                {
                    if (num5 != selectedValue)
                    {
                        flag2 = true;
                    }
                    if (num6 != num3)
                    {
                        flag3 = true;
                    }
                    if (!first.SequenceEqual<int>(second) || !strArray2.SequenceEqual<string>(strArray))
                    {
                        flag = true;
                        break;
                    }
                }
            }
            using (new EditorGUI.DisabledScope(flag || (strArray.Length == 1)))
            {
                EditorGUI.BeginChangeCheck();
                EditorGUI.showMixedValue = flag || flag2;
                selectedValue = EditorGUILayout.IntPopup(TextureImporterInspector.s_Styles.textureFormat, selectedValue, EditorGUIUtility.TempContent(strArray), second, new GUILayoutOption[0]);
                EditorGUI.showMixedValue = false;
                if (inspector.textureType != TextureImporterType.Advanced)
                {
                    selectedValue = -1 - selectedValue;
                }
                if (EditorGUI.EndChangeCheck())
                {
                    platformSettings.SetTextureFormatForAll((TextureImporterFormat) selectedValue);
                }
            }
            if ((num3 == -5) || (!flag3 && ArrayUtility.Contains<TextureImporterFormat>(TextureImporterInspector.kFormatsWithCompressionSettings, (TextureImporterFormat) num3)))
            {
                EditorGUI.BeginChangeCheck();
                EditorGUI.showMixedValue = platformSettings.overriddenIsDifferent || platformSettings.compressionQualityIsDifferent;
                int quality = this.EditCompressionQuality(platformSettings.m_Target, platformSettings.compressionQuality);
                EditorGUI.showMixedValue = false;
                if (EditorGUI.EndChangeCheck())
                {
                    platformSettings.SetCompressionQualityForAll(quality);
                }
            }
            if ((TextureImporter.FullToSimpleTextureFormat((TextureImporterFormat) selectedValue) == TextureImporterFormat.AutomaticCrunched) && (TextureImporter.FullToSimpleTextureFormat((TextureImporterFormat) num3) != TextureImporterFormat.AutomaticCrunched))
            {
                EditorGUILayout.HelpBox("Crunched is not supported on this platform. Falling back to 'Compressed'.", MessageType.Warning);
            }
            bool flag4 = (selectedValue == -1) || TextureImporter.IsTextureFormatETC1Compression((TextureFormat) num3);
            if ((platformSettings.overridden && (platformSettings.m_Target == BuildTarget.Android)) && (flag4 && (platformSettings.importers.Length > 0)))
            {
                TextureImporter importer2 = platformSettings.importers[0];
                EditorGUI.BeginChangeCheck();
                bool flag5 = GUILayout.Toggle(importer2.GetAllowsAlphaSplitting(), TextureImporterInspector.s_Styles.etc1Compression, new GUILayoutOption[0]);
                if (EditorGUI.EndChangeCheck())
                {
                    foreach (TextureImporter importer3 in platformSettings.importers)
                    {
                        importer3.SetAllowsAlphaSplitting(flag5);
                    }
                    platformSettings.SetChanged();
                }
            }
            if ((!platformSettings.overridden && (platformSettings.m_Target == BuildTarget.Android)) && ((platformSettings.importers.Length > 0) && platformSettings.importers[0].GetAllowsAlphaSplitting()))
            {
                platformSettings.importers[0].SetAllowsAlphaSplitting(false);
                platformSettings.SetChanged();
            }
        }
    }
}

