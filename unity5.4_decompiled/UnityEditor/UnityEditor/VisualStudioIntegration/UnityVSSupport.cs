﻿namespace UnityEditor.VisualStudioIntegration
{
    using Microsoft.Win32;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using UnityEditor;
    using UnityEditor.Utils;
    using UnityEditorInternal;
    using UnityEngine;

    internal class UnityVSSupport
    {
        [CompilerGenerated]
        private static Func<Assembly, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<Assembly, bool> <>f__am$cache5;
        private static bool m_ShouldUnityVSBeActive;
        private static string s_AboutLabel;
        private static bool? s_IsUnityVSEnabled;
        public static string s_UnityVSBridgeToLoad;

        private static string CalculateAboutWindowLabel()
        {
            if (!IsUnityVSEnabled())
            {
                return string.Empty;
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = a => a.Location == s_UnityVSBridgeToLoad;
            }
            Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault<Assembly>(<>f__am$cache5);
            if (assembly == null)
            {
                return string.Empty;
            }
            StringBuilder builder = new StringBuilder("Microsoft Visual Studio Tools for Unity ");
            builder.Append(assembly.GetName().Version);
            builder.Append(" enabled");
            return builder.ToString();
        }

        public static string GetAboutWindowLabel()
        {
            if (s_AboutLabel == null)
            {
                s_AboutLabel = CalculateAboutWindowLabel();
            }
            return s_AboutLabel;
        }

        private static string GetAssemblyLocation(Assembly a)
        {
            try
            {
                return a.Location;
            }
            catch (NotSupportedException)
            {
                return null;
            }
        }

        private static string GetVstuBridgeAssembly(VisualStudioVersion version)
        {
            try
            {
                string vsVersion = string.Empty;
                switch (version)
                {
                    case VisualStudioVersion.VisualStudio2010:
                        vsVersion = "2010";
                        break;

                    case VisualStudioVersion.VisualStudio2012:
                        vsVersion = "2012";
                        break;

                    case VisualStudioVersion.VisualStudio2013:
                        vsVersion = "2013";
                        break;

                    case VisualStudioVersion.VisualStudio2015:
                        vsVersion = "2015";
                        break;

                    case VisualStudioVersion.VisualStudio15:
                        vsVersion = "15.0";
                        break;
                }
                string vstuBridgePathFromRegistry = GetVstuBridgePathFromRegistry(vsVersion, true);
                if (vstuBridgePathFromRegistry != null)
                {
                    return vstuBridgePathFromRegistry;
                }
                return GetVstuBridgePathFromRegistry(vsVersion, false);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string GetVstuBridgePathFromRegistry(string vsVersion, bool currentUser)
        {
            return (string) Registry.GetValue(string.Format(@"{0}\Software\Microsoft\Microsoft Visual Studio {1} Tools for Unity", !currentUser ? "HKEY_LOCAL_MACHINE" : "HKEY_CURRENT_USER", vsVersion), "UnityExtensionPath", null);
        }

        public static void Initialize()
        {
            Initialize(null);
        }

        public static void Initialize(string editorPath)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                string str2;
                VisualStudioVersion version;
                if (editorPath != null)
                {
                    str2 = editorPath;
                }
                else
                {
                    str2 = EditorPrefs.GetString("kScriptsDefaultApp");
                }
                if (str2.EndsWith("UnityVS.OpenFile.exe"))
                {
                    str2 = SyncVS.FindBestVisualStudio();
                    if (str2 != null)
                    {
                        EditorPrefs.SetString("kScriptsDefaultApp", str2);
                    }
                }
                if (IsVisualStudio(str2, out version))
                {
                    m_ShouldUnityVSBeActive = true;
                    string vstuBridgeAssembly = GetVstuBridgeAssembly(version);
                    if (vstuBridgeAssembly == null)
                    {
                        Console.WriteLine("Unable to find bridge dll in registry for Microsoft Visual Studio Tools for Unity for " + str2);
                    }
                    else if (!File.Exists(vstuBridgeAssembly))
                    {
                        Console.WriteLine("Unable to find bridge dll on disk for Microsoft Visual Studio Tools for Unity for " + vstuBridgeAssembly);
                    }
                    else
                    {
                        s_UnityVSBridgeToLoad = vstuBridgeAssembly;
                        InternalEditorUtility.SetupCustomDll(Path.GetFileNameWithoutExtension(vstuBridgeAssembly), vstuBridgeAssembly);
                    }
                }
            }
        }

        public static bool IsUnityVSEnabled()
        {
            if (!s_IsUnityVSEnabled.HasValue)
            {
                if (m_ShouldUnityVSBeActive)
                {
                }
                s_IsUnityVSEnabled = new bool?((<>f__am$cache4 == null) && AppDomain.CurrentDomain.GetAssemblies().Any<Assembly>(<>f__am$cache4));
            }
            return s_IsUnityVSEnabled.Value;
        }

        private static bool IsVisualStudio(string externalEditor, out VisualStudioVersion vsVersion)
        {
            <IsVisualStudio>c__AnonStoreyD1 yd = new <IsVisualStudio>c__AnonStoreyD1 {
                externalEditor = externalEditor
            };
            if (string.IsNullOrEmpty(yd.externalEditor))
            {
                vsVersion = VisualStudioVersion.Invalid;
                return false;
            }
            KeyValuePair<VisualStudioVersion, string>[] pairArray = SyncVS.InstalledVisualStudios.Where<KeyValuePair<VisualStudioVersion, string>>(new Func<KeyValuePair<VisualStudioVersion, string>, bool>(yd.<>m__267)).ToArray<KeyValuePair<VisualStudioVersion, string>>();
            if (pairArray.Length > 0)
            {
                vsVersion = pairArray[0].Key;
                return true;
            }
            if (yd.externalEditor.EndsWith("devenv.exe", StringComparison.OrdinalIgnoreCase) && TryGetVisualStudioVersion(yd.externalEditor, out vsVersion))
            {
                return true;
            }
            vsVersion = VisualStudioVersion.Invalid;
            return false;
        }

        private static Version ProductVersion(string externalEditor)
        {
            try
            {
                return new Version(FileVersionInfo.GetVersionInfo(externalEditor).ProductVersion);
            }
            catch (Exception)
            {
                return new Version(0, 0);
            }
        }

        public static void ScriptEditorChanged(string editorPath)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                Initialize(editorPath);
                InternalEditorUtility.RequestScriptReload();
            }
        }

        public static bool ShouldUnityVSBeActive()
        {
            return m_ShouldUnityVSBeActive;
        }

        private static bool TryGetVisualStudioVersion(string externalEditor, out VisualStudioVersion vsVersion)
        {
            switch (ProductVersion(externalEditor).Major)
            {
                case 9:
                    vsVersion = VisualStudioVersion.VisualStudio2008;
                    return true;

                case 10:
                    vsVersion = VisualStudioVersion.VisualStudio2010;
                    return true;

                case 11:
                    vsVersion = VisualStudioVersion.VisualStudio2012;
                    return true;

                case 12:
                    vsVersion = VisualStudioVersion.VisualStudio2013;
                    return true;

                case 14:
                    vsVersion = VisualStudioVersion.VisualStudio2015;
                    return true;

                case 15:
                    vsVersion = VisualStudioVersion.VisualStudio15;
                    return true;
            }
            vsVersion = VisualStudioVersion.Invalid;
            return false;
        }

        [CompilerGenerated]
        private sealed class <IsVisualStudio>c__AnonStoreyD1
        {
            internal string externalEditor;

            internal bool <>m__267(KeyValuePair<VisualStudioVersion, string> kvp)
            {
                return Paths.AreEqual(kvp.Value, this.externalEditor, true);
            }
        }
    }
}

