﻿namespace UnityEditor
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class LightingDataAsset : Object
    {
        internal bool isValid { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        internal string validityErrorMessage { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }
    }
}

