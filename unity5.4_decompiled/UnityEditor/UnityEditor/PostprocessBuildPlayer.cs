﻿namespace UnityEditor
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using UnityEditor.BuildReporting;
    using UnityEditor.Modules;
    using UnityEditor.Utils;
    using UnityEngine;

    internal class PostprocessBuildPlayer
    {
        internal const string StreamingAssets = "Assets/StreamingAssets";

        internal static string ExecuteSystemProcess(string command, string args, string workingdir)
        {
            ProcessStartInfo si = new ProcessStartInfo {
                FileName = command,
                Arguments = args,
                WorkingDirectory = workingdir,
                CreateNoWindow = true
            };
            Program program = new Program(si);
            program.Start();
            while (!program.WaitForExit(100))
            {
            }
            string standardOutputAsString = program.GetStandardOutputAsString();
            program.Dispose();
            return standardOutputAsString;
        }

        internal static string GenerateBundleIdentifier(string companyName, string productName)
        {
            return ("unity." + companyName + "." + productName);
        }

        internal static string GetArchitectureForTarget(BuildTarget target)
        {
            BuildTarget target2 = target;
            switch (target2)
            {
                case BuildTarget.StandaloneLinux:
                    goto Label_0040;

                case BuildTarget.StandaloneWindows64:
                    break;

                default:
                    switch (target2)
                    {
                        case BuildTarget.StandaloneOSXIntel:
                        case BuildTarget.StandaloneWindows:
                            goto Label_0040;

                        default:
                            switch (target2)
                            {
                                case BuildTarget.StandaloneLinuxUniversal:
                                    goto Label_0040;
                            }
                            return string.Empty;
                    }
                    break;
            }
            return "x86_64";
        Label_0040:
            return "x86";
        }

        public static string GetExtensionForBuildTarget(BuildTarget target, BuildOptions options)
        {
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            if (buildPostProcessor == null)
            {
                return string.Empty;
            }
            return buildPostProcessor.GetExtension(target, options);
        }

        public static string GetScriptLayoutFileFromBuild(BuildOptions options, BuildTarget target, string installPath, string fileName)
        {
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            if (buildPostProcessor != null)
            {
                return buildPostProcessor.GetScriptLayoutFileFromBuild(options, installPath, fileName);
            }
            return string.Empty;
        }

        internal static bool InstallPluginsByExtension(string pluginSourceFolder, string extension, string debugExtension, string destPluginFolder, bool copyDirectories)
        {
            bool flag = false;
            if (Directory.Exists(pluginSourceFolder))
            {
                foreach (string str in Directory.GetFileSystemEntries(pluginSourceFolder))
                {
                    string fileName = Path.GetFileName(str);
                    string str3 = Path.GetExtension(str);
                    bool flag2 = str3.Equals(extension, StringComparison.OrdinalIgnoreCase) || fileName.Equals(extension, StringComparison.OrdinalIgnoreCase);
                    bool flag3 = ((debugExtension != null) && (debugExtension.Length != 0)) && (str3.Equals(debugExtension, StringComparison.OrdinalIgnoreCase) || fileName.Equals(debugExtension, StringComparison.OrdinalIgnoreCase));
                    if (flag2 || flag3)
                    {
                        if (!Directory.Exists(destPluginFolder))
                        {
                            Directory.CreateDirectory(destPluginFolder);
                        }
                        string target = Path.Combine(destPluginFolder, fileName);
                        if (copyDirectories)
                        {
                            FileUtil.CopyDirectoryRecursive(str, target);
                        }
                        else if (!Directory.Exists(str))
                        {
                            FileUtil.UnityFileCopy(str, target);
                        }
                        flag = true;
                    }
                }
            }
            return flag;
        }

        internal static void InstallStreamingAssets(string stagingAreaDataPath)
        {
            if (Directory.Exists("Assets/StreamingAssets"))
            {
                FileUtil.CopyDirectoryRecursiveForPostprocess("Assets/StreamingAssets", Path.Combine(stagingAreaDataPath, "StreamingAssets"), true);
            }
        }

        public static void Launch(BuildTarget target, string path, string productName, BuildOptions options)
        {
            BuildLaunchPlayerArgs args;
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            if (buildPostProcessor == null)
            {
                throw new UnityException(string.Format("Launching {0} build target via mono is not supported", target));
            }
            args.target = target;
            args.playerPackage = BuildPipeline.GetPlaybackEngineDirectory(target, options);
            args.installPath = path;
            args.productName = productName;
            args.options = options;
            buildPostProcessor.LaunchPlayer(args);
        }

        public static void Postprocess(BuildTarget target, string installPath, string companyName, string productName, int width, int height, string downloadWebplayerUrl, string manualDownloadWebplayerUrl, BuildOptions options, RuntimeClassRegistry usedClassRegistry, BuildReport report)
        {
            BuildPostProcessArgs args;
            string str = "Temp/StagingArea";
            string str2 = "Temp/StagingArea/Data";
            string str3 = "Temp/StagingArea/Data/Managed";
            string playbackEngineDirectory = BuildPipeline.GetPlaybackEngineDirectory(target, options);
            bool flag = ((options & BuildOptions.InstallInBuildFolder) != BuildOptions.CompressTextures) && SupportsInstallInBuildFolder(target);
            if ((installPath == string.Empty) && !flag)
            {
                throw new Exception(installPath + " must not be an empty string");
            }
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            if (buildPostProcessor == null)
            {
                throw new UnityException(string.Format("Build target '{0}' not supported", target));
            }
            args.target = target;
            args.stagingAreaData = str2;
            args.stagingArea = str;
            args.stagingAreaDataManaged = str3;
            args.playerPackage = playbackEngineDirectory;
            args.installPath = installPath;
            args.companyName = companyName;
            args.productName = productName;
            args.productGUID = PlayerSettings.productGUID;
            args.options = options;
            args.usedClassRegistry = usedClassRegistry;
            args.report = report;
            buildPostProcessor.PostProcess(args);
        }

        public static string PrepareForBuild(BuildOptions options, BuildTarget target)
        {
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            if (buildPostProcessor == null)
            {
                return null;
            }
            return buildPostProcessor.PrepareForBuild(options, target);
        }

        public static bool SupportsInstallInBuildFolder(BuildTarget target)
        {
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            if (buildPostProcessor != null)
            {
                return buildPostProcessor.SupportsInstallInBuildFolder();
            }
            switch (target)
            {
                case BuildTarget.PS3:
                case BuildTarget.Android:
                case BuildTarget.PSP2:
                case BuildTarget.PSM:
                case BuildTarget.WSAPlayer:
                    return true;
            }
            return false;
        }

        public static bool SupportsScriptsOnlyBuild(BuildTarget target)
        {
            IBuildPostprocessor buildPostProcessor = ModuleManager.GetBuildPostProcessor(target);
            return ((buildPostProcessor != null) && buildPostProcessor.SupportsScriptsOnlyBuild());
        }

        public static string subDir32Bit
        {
            get
            {
                return "x86";
            }
        }

        public static string subDir64Bit
        {
            get
            {
                return "x86_64";
            }
        }
    }
}

