﻿namespace UnityEditor
{
    using System;
    using UnityEditor.Audio;

    [CustomEditor(typeof(AudioMixerSnapshotController)), CanEditMultipleObjects]
    internal class AudioMixerSnapshotControllerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
        }
    }
}

