﻿namespace UnityEditor.BuildReporting
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;

    internal sealed class BuildReport : Object
    {
        public event Action<BuildReport> Changed;

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void AddAppendix(Object obj);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void AddFile(string path, string role);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void AddFilesRecursive(string rootDir, string role);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void AddMessage(LogType messageType, string message);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void BeginBuildStep(string stepName);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void BeginBuildStepNoTiming(string stepName);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal extern Object[] GetAllAppendices();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern Object[] GetAppendices(Type type);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        internal extern Object[] GetAppendicesByClassID(int classID);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern BuildReport GetLatestReport();
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern void RelocateFiles(string originalPathPrefix, string newPathPrefix);
        public void SendChanged()
        {
            if (this.Changed != null)
            {
                this.Changed(this);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public extern string SummarizeErrors();

        public BuildOptions buildOptions { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public BuildTarget buildTarget { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public uint crc { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public string outputPath { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public int totalErrors { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public long totalSize { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public long totalTimeMS { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }

        public int totalWarnings { [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall] get; }
    }
}

