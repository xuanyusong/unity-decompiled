﻿namespace UnityEditor
{
    using System;

    public enum tvOSTargetOSVersion
    {
        tvOS_9_0 = 900,
        tvOS_9_1 = 0x385,
        Unknown = 0
    }
}

