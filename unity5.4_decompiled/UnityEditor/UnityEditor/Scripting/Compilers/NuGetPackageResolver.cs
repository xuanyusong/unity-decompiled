﻿namespace UnityEditor.Scripting.Compilers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using UnityEditor;

    internal sealed class NuGetPackageResolver
    {
        public NuGetPackageResolver()
        {
            this.TargetMoniker = "UAP,Version=v10.0";
        }

        private string ConvertToWindowsPath(string path)
        {
            return path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
        }

        private string GetPackagesPath()
        {
            string packagesDirectory = this.PackagesDirectory;
            if (!string.IsNullOrEmpty(packagesDirectory))
            {
                return packagesDirectory;
            }
            packagesDirectory = Environment.GetEnvironmentVariable("NUGET_PACKAGES");
            if (!string.IsNullOrEmpty(packagesDirectory))
            {
                return packagesDirectory;
            }
            return Path.Combine(Path.Combine(Environment.GetEnvironmentVariable("USERPROFILE"), ".nuget"), "packages");
        }

        public string[] Resolve()
        {
            Dictionary<string, object> dictionary = (Dictionary<string, object>) Json.Deserialize(File.ReadAllText(this.ProjectLockFile));
            Dictionary<string, object> dictionary2 = (Dictionary<string, object>) dictionary["targets"];
            Dictionary<string, object> dictionary3 = (Dictionary<string, object>) dictionary2[this.TargetMoniker];
            List<string> list = new List<string>();
            string str2 = this.ConvertToWindowsPath(this.GetPackagesPath());
            foreach (KeyValuePair<string, object> pair in dictionary3)
            {
                object obj2;
                Dictionary<string, object> dictionary4 = (Dictionary<string, object>) pair.Value;
                if (dictionary4.TryGetValue("compile", out obj2))
                {
                    Dictionary<string, object> dictionary5 = (Dictionary<string, object>) obj2;
                    char[] separator = new char[] { '/' };
                    string[] strArray = pair.Key.Split(separator);
                    string str3 = strArray[0];
                    string str4 = strArray[1];
                    string path = Path.Combine(Path.Combine(str2, str3), str4);
                    if (!Directory.Exists(path))
                    {
                        throw new Exception(string.Format("Package directory not found: \"{0}\".", path));
                    }
                    foreach (string str6 in dictionary5.Keys)
                    {
                        if (!string.Equals(Path.GetFileName(str6), "_._", StringComparison.InvariantCultureIgnoreCase))
                        {
                            string str8 = Path.Combine(path, this.ConvertToWindowsPath(str6));
                            if (!File.Exists(str8))
                            {
                                throw new Exception(string.Format("Reference not found: \"{0}\".", str8));
                            }
                            list.Add(str8);
                        }
                    }
                    if (dictionary4.ContainsKey("frameworkAssemblies"))
                    {
                        throw new NotImplementedException("Support for \"frameworkAssemblies\" property has not been implemented yet.");
                    }
                }
            }
            this.ResolvedReferences = list.ToArray();
            return this.ResolvedReferences;
        }

        public string PackagesDirectory { get; set; }

        public string ProjectLockFile { get; set; }

        public string[] ResolvedReferences { get; private set; }

        public string TargetMoniker { get; set; }
    }
}

