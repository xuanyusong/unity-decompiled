﻿namespace UnityEditor
{
    using System;

    internal interface ISelectionBinding
    {
        bool animationIsEditable { get; }

        bool clipIsEditable { get; }

        int id { get; }

        float timeOffset { get; }
    }
}

