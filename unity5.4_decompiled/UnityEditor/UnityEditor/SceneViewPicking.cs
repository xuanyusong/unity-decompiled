﻿namespace UnityEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using UnityEngine;

    internal class SceneViewPicking
    {
        private static int s_PreviousPrefixHash;
        private static int s_PreviousTopmostHash;
        private static bool s_RetainHashes;

        static SceneViewPicking()
        {
            Selection.selectionChanged = (Action) Delegate.Combine(Selection.selectionChanged, new Action(SceneViewPicking.ResetHashes));
        }

        [DebuggerHidden]
        private static IEnumerable<GameObject> GetAllOverlapping(Vector2 position)
        {
            return new <GetAllOverlapping>c__Iterator8 { position = position, <$>position = position, $PC = -2 };
        }

        public static GameObject PickGameObject(Vector2 mousePosition)
        {
            s_RetainHashes = true;
            IEnumerator<GameObject> enumerator = GetAllOverlapping(mousePosition).GetEnumerator();
            if (!enumerator.MoveNext())
            {
                return null;
            }
            GameObject current = enumerator.Current;
            GameObject obj3 = HandleUtility.FindSelectionBase(current);
            GameObject obj4 = (obj3 != null) ? obj3 : current;
            int hashCode = current.GetHashCode();
            int hash = hashCode;
            if (Selection.activeGameObject == null)
            {
                s_PreviousTopmostHash = hashCode;
                s_PreviousPrefixHash = hash;
                return obj4;
            }
            if (hashCode != s_PreviousTopmostHash)
            {
                s_PreviousTopmostHash = hashCode;
                s_PreviousPrefixHash = hash;
                return ((Selection.activeGameObject != obj3) ? obj4 : current);
            }
            s_PreviousTopmostHash = hashCode;
            if (Selection.activeGameObject == obj3)
            {
                if (hash == s_PreviousPrefixHash)
                {
                    return current;
                }
                s_PreviousPrefixHash = hash;
                return obj3;
            }
            GameObject[] filter = new GameObject[] { Selection.activeGameObject };
            if (HandleUtility.PickGameObject(mousePosition, false, null, filter) == Selection.activeGameObject)
            {
                while (enumerator.Current != Selection.activeGameObject)
                {
                    if (!enumerator.MoveNext())
                    {
                        s_PreviousPrefixHash = hashCode;
                        return obj4;
                    }
                    UpdateHash(ref hash, enumerator.Current);
                }
            }
            if (hash != s_PreviousPrefixHash)
            {
                s_PreviousPrefixHash = hashCode;
                return obj4;
            }
            if (!enumerator.MoveNext())
            {
                s_PreviousPrefixHash = hashCode;
                return obj4;
            }
            UpdateHash(ref hash, enumerator.Current);
            if (enumerator.Current == obj3)
            {
                if (!enumerator.MoveNext())
                {
                    s_PreviousPrefixHash = hashCode;
                    return obj4;
                }
                UpdateHash(ref hash, enumerator.Current);
            }
            s_PreviousPrefixHash = hash;
            return enumerator.Current;
        }

        private static void ResetHashes()
        {
            if (!s_RetainHashes)
            {
                s_PreviousTopmostHash = 0;
                s_PreviousPrefixHash = 0;
            }
            s_RetainHashes = false;
        }

        private static void UpdateHash(ref int hash, object obj)
        {
            hash = (hash * 0x21) + obj.GetHashCode();
        }

        [CompilerGenerated]
        private sealed class <GetAllOverlapping>c__Iterator8 : IDisposable, IEnumerator, IEnumerable, IEnumerable<GameObject>, IEnumerator<GameObject>
        {
            internal GameObject $current;
            internal int $PC;
            internal Vector2 <$>position;
            internal List<GameObject> <allOverlapping>__0;
            internal GameObject <go>__1;
            internal Vector2 position;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<allOverlapping>__0 = new List<GameObject>();
                        break;

                    case 1:
                        this.<allOverlapping>__0.Add(this.<go>__1);
                        break;

                    default:
                        goto Label_00E4;
                }
                this.<go>__1 = HandleUtility.PickGameObject(this.position, false, this.<allOverlapping>__0.ToArray());
                if (this.<go>__1 != null)
                {
                    if ((this.<allOverlapping>__0.Count <= 0) || (this.<go>__1 != this.<allOverlapping>__0.Last<GameObject>()))
                    {
                        this.$current = this.<go>__1;
                        this.$PC = 1;
                        return true;
                    }
                    Debug.LogError("GetAllOverlapping failed, could not ignore game object '" + this.<go>__1.name + "' when picking");
                }
                this.$PC = -1;
            Label_00E4:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<GameObject> IEnumerable<GameObject>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new SceneViewPicking.<GetAllOverlapping>c__Iterator8 { position = this.<$>position };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<UnityEngine.GameObject>.GetEnumerator();
            }

            GameObject IEnumerator<GameObject>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

