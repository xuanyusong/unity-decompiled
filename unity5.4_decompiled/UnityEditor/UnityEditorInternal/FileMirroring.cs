﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;

    internal static class FileMirroring
    {
        private static bool AreFilesIdentical(string filePath1, string filePath2)
        {
            using (FileStream stream = File.OpenRead(filePath1))
            {
                using (FileStream stream2 = File.OpenRead(filePath2))
                {
                    int num2;
                    if (stream.Length != stream2.Length)
                    {
                        return false;
                    }
                    byte[] array = new byte[0x10000];
                    byte[] buffer2 = new byte[0x10000];
                    while ((num2 = stream.Read(array, 0, array.Length)) > 0)
                    {
                        stream2.Read(buffer2, 0, buffer2.Length);
                        for (int i = 0; i < num2; i++)
                        {
                            if (array[i] != buffer2[i])
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public static bool CanSkipCopy(string from, string to)
        {
            if (!File.Exists(to))
            {
                return false;
            }
            return AreFilesIdentical(from, to);
        }

        private static void DeleteFileOrDirectory(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            else if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        private static FileEntryType FileEntryTypeFor(string fileEntry)
        {
            if (File.Exists(fileEntry))
            {
                return FileEntryType.File;
            }
            if (Directory.Exists(fileEntry))
            {
                return FileEntryType.Directory;
            }
            return FileEntryType.NotExisting;
        }

        public static void MirrorFile(string from, string to)
        {
            MirrorFile(from, to, new Func<string, string, bool>(FileMirroring.CanSkipCopy));
        }

        public static void MirrorFile(string from, string to, Func<string, string, bool> comparer)
        {
            if (!comparer(from, to))
            {
                if (!File.Exists(from))
                {
                    DeleteFileOrDirectory(to);
                }
                else
                {
                    string directoryName = Path.GetDirectoryName(to);
                    if (!Directory.Exists(directoryName))
                    {
                        Directory.CreateDirectory(directoryName);
                    }
                    File.Copy(from, to, true);
                }
            }
        }

        public static void MirrorFolder(string from, string to)
        {
            MirrorFolder(from, to, new Func<string, string, bool>(FileMirroring.CanSkipCopy));
        }

        public static void MirrorFolder(string from, string to, Func<string, string, bool> comparer)
        {
            <MirrorFolder>c__AnonStorey7E storeye;
            storeye = new <MirrorFolder>c__AnonStorey7E {
                to = to,
                from = from,
                from = Path.GetFullPath(storeye.from),
                to = Path.GetFullPath(storeye.to)
            };
            if (!Directory.Exists(storeye.from))
            {
                if (Directory.Exists(storeye.to))
                {
                    Directory.Delete(storeye.to, true);
                }
            }
            else
            {
                if (!Directory.Exists(storeye.to))
                {
                    Directory.CreateDirectory(storeye.to);
                }
                IEnumerable<string> first = Directory.GetFileSystemEntries(storeye.to).Select<string, string>(new Func<string, string>(storeye.<>m__11E));
                IEnumerable<string> second = Directory.GetFileSystemEntries(storeye.from).Select<string, string>(new Func<string, string>(storeye.<>m__11F));
                IEnumerator<string> enumerator = first.Except<string>(second).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        string current = enumerator.Current;
                        DeleteFileOrDirectory(Path.Combine(storeye.to, current));
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                IEnumerator<string> enumerator2 = second.GetEnumerator();
                try
                {
                    while (enumerator2.MoveNext())
                    {
                        string str2 = enumerator2.Current;
                        string fileEntry = Path.Combine(storeye.from, str2);
                        string str4 = Path.Combine(storeye.to, str2);
                        FileEntryType type = FileEntryTypeFor(fileEntry);
                        FileEntryType type2 = FileEntryTypeFor(str4);
                        if ((type == FileEntryType.File) && (type2 == FileEntryType.Directory))
                        {
                            DeleteFileOrDirectory(str4);
                        }
                        if (type == FileEntryType.Directory)
                        {
                            if (type2 == FileEntryType.File)
                            {
                                DeleteFileOrDirectory(str4);
                            }
                            if (type2 != FileEntryType.Directory)
                            {
                                Directory.CreateDirectory(str4);
                            }
                            MirrorFolder(fileEntry, str4);
                        }
                        if (type == FileEntryType.File)
                        {
                            MirrorFile(fileEntry, str4, comparer);
                        }
                    }
                }
                finally
                {
                    if (enumerator2 == null)
                    {
                    }
                    enumerator2.Dispose();
                }
            }
        }

        private static string StripPrefix(string s, string prefix)
        {
            return s.Substring(prefix.Length + 1);
        }

        [CompilerGenerated]
        private sealed class <MirrorFolder>c__AnonStorey7E
        {
            internal string from;
            internal string to;

            internal string <>m__11E(string s)
            {
                return FileMirroring.StripPrefix(s, this.to);
            }

            internal string <>m__11F(string s)
            {
                return FileMirroring.StripPrefix(s, this.from);
            }
        }

        private enum FileEntryType
        {
            File,
            Directory,
            NotExisting
        }
    }
}

