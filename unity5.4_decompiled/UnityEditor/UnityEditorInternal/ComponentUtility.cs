﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;

    public sealed class ComponentUtility
    {
        private static bool CompareComponentOrderAndTypes(List<Component> srcComponents, List<Component> dstComponents)
        {
            if (srcComponents.Count != dstComponents.Count)
            {
                return false;
            }
            for (int i = 0; i != srcComponents.Count; i++)
            {
                if (srcComponents[i].GetType() != dstComponents[i].GetType())
                {
                    return false;
                }
            }
            return true;
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool CopyComponent(Component component);
        private static void DestroyComponents(List<Component> components)
        {
            for (int i = components.Count - 1; i >= 0; i--)
            {
                Object.DestroyImmediate(components[i]);
            }
        }

        public static void DestroyComponentsMatching(GameObject dst, IsDesiredComponent componentFilter)
        {
            <DestroyComponentsMatching>c__AnonStorey12 storey = new <DestroyComponentsMatching>c__AnonStorey12 {
                componentFilter = componentFilter
            };
            List<Component> results = new List<Component>();
            dst.GetComponents<Component>(results);
            results.RemoveAll(new Predicate<Component>(storey.<>m__E));
            DestroyComponents(results);
        }

        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool MoveComponentDown(Component component);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool MoveComponentUp(Component component);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool PasteComponentAsNew(GameObject go);
        [MethodImpl(MethodImplOptions.InternalCall), WrapperlessIcall]
        public static extern bool PasteComponentValues(Component component);
        public static void ReplaceComponentsIfDifferent(GameObject src, GameObject dst, IsDesiredComponent componentFilter)
        {
            <ReplaceComponentsIfDifferent>c__AnonStorey13 storey = new <ReplaceComponentsIfDifferent>c__AnonStorey13 {
                componentFilter = componentFilter
            };
            List<Component> results = new List<Component>();
            src.GetComponents<Component>(results);
            results.RemoveAll(new Predicate<Component>(storey.<>m__F));
            List<Component> list2 = new List<Component>();
            dst.GetComponents<Component>(list2);
            list2.RemoveAll(new Predicate<Component>(storey.<>m__10));
            if (!CompareComponentOrderAndTypes(results, list2))
            {
                DestroyComponents(list2);
                list2.Clear();
                for (int j = 0; j != results.Count; j++)
                {
                    Component item = dst.AddComponent(results[j].GetType());
                    list2.Add(item);
                }
            }
            for (int i = 0; i != results.Count; i++)
            {
                EditorUtility.CopySerializedIfDifferent(results[i], list2[i]);
            }
        }

        [CompilerGenerated]
        private sealed class <DestroyComponentsMatching>c__AnonStorey12
        {
            internal ComponentUtility.IsDesiredComponent componentFilter;

            internal bool <>m__E(Component x)
            {
                return !this.componentFilter(x);
            }
        }

        [CompilerGenerated]
        private sealed class <ReplaceComponentsIfDifferent>c__AnonStorey13
        {
            internal ComponentUtility.IsDesiredComponent componentFilter;

            internal bool <>m__10(Component x)
            {
                return !this.componentFilter(x);
            }

            internal bool <>m__F(Component x)
            {
                return !this.componentFilter(x);
            }
        }

        public delegate bool IsDesiredComponent(Component c);
    }
}

