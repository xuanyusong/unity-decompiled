﻿namespace UnityEditorInternal
{
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    internal class AnimationWindowSelectionItem : ScriptableObject, ISelectionBinding, IEquatable<AnimationWindowSelectionItem>
    {
        [SerializeField]
        private AnimationClip m_AnimationClip;
        private List<AnimationWindowCurve> m_CurvesCache;
        [SerializeField]
        private GameObject m_GameObject;
        [SerializeField]
        private int m_Id;
        [SerializeField]
        private float m_TimeOffset;

        public void ClearCache()
        {
            this.m_CurvesCache = null;
        }

        public static AnimationWindowSelectionItem Create()
        {
            AnimationWindowSelectionItem item = ScriptableObject.CreateInstance(typeof(AnimationWindowSelectionItem)) as AnimationWindowSelectionItem;
            item.hideFlags = HideFlags.HideAndDontSave;
            return item;
        }

        public bool Equals(AnimationWindowSelectionItem other)
        {
            return (((this.id == other.id) && (this.animationClip == other.animationClip)) && (this.gameObject == other.gameObject));
        }

        public int GetRefreshHash()
        {
            return (((this.id * 0x4c93) ^ ((this.animationClip == null) ? 0 : (0x2d9 * this.animationClip.GetHashCode()))) ^ ((this.rootGameObject == null) ? 0 : (0x1b * this.rootGameObject.GetHashCode())));
        }

        public virtual AnimationClip animationClip
        {
            get
            {
                return this.m_AnimationClip;
            }
            set
            {
                this.m_AnimationClip = value;
            }
        }

        public virtual bool animationIsEditable
        {
            get
            {
                if ((this.animationClip != null) && ((this.animationClip.hideFlags & HideFlags.NotEditable) != HideFlags.None))
                {
                    return false;
                }
                if (this.objectIsPrefab)
                {
                    return false;
                }
                return true;
            }
        }

        public virtual Component animationPlayer
        {
            get
            {
                if (this.gameObject != null)
                {
                    return AnimationWindowUtility.GetClosestAnimationPlayerComponentInParents(this.gameObject.transform);
                }
                return null;
            }
        }

        public virtual bool clipIsEditable
        {
            get
            {
                if (this.animationClip == null)
                {
                    return false;
                }
                if ((this.animationClip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
                {
                    return false;
                }
                if (!AssetDatabase.IsOpenForEdit(this.animationClip))
                {
                    return false;
                }
                return true;
            }
        }

        public List<AnimationWindowCurve> curves
        {
            get
            {
                if (this.m_CurvesCache == null)
                {
                    this.m_CurvesCache = new List<AnimationWindowCurve>();
                    if (this.animationClip != null)
                    {
                        EditorCurveBinding[] curveBindings = AnimationUtility.GetCurveBindings(this.animationClip);
                        EditorCurveBinding[] objectReferenceCurveBindings = AnimationUtility.GetObjectReferenceCurveBindings(this.animationClip);
                        foreach (EditorCurveBinding binding in curveBindings)
                        {
                            if (AnimationWindowUtility.ShouldShowAnimationWindowCurve(binding))
                            {
                                AnimationWindowCurve item = new AnimationWindowCurve(this.animationClip, binding, CurveBindingUtility.GetEditorCurveValueType(this.rootGameObject, binding)) {
                                    selectionBindingInterface = this
                                };
                                this.m_CurvesCache.Add(item);
                            }
                        }
                        foreach (EditorCurveBinding binding2 in objectReferenceCurveBindings)
                        {
                            AnimationWindowCurve curve2 = new AnimationWindowCurve(this.animationClip, binding2, CurveBindingUtility.GetEditorCurveValueType(this.rootGameObject, binding2)) {
                                selectionBindingInterface = this
                            };
                            this.m_CurvesCache.Add(curve2);
                        }
                        this.m_CurvesCache.Sort();
                    }
                }
                return this.m_CurvesCache;
            }
        }

        public virtual GameObject gameObject
        {
            get
            {
                return this.m_GameObject;
            }
            set
            {
                this.m_GameObject = value;
            }
        }

        public virtual int id
        {
            get
            {
                return this.m_Id;
            }
            set
            {
                this.m_Id = value;
            }
        }

        public virtual bool objectIsPrefab
        {
            get
            {
                if (this.gameObject == null)
                {
                    return false;
                }
                return (EditorUtility.IsPersistent(this.gameObject) || ((this.gameObject.hideFlags & HideFlags.NotEditable) != HideFlags.None));
            }
        }

        public virtual GameObject rootGameObject
        {
            get
            {
                Component animationPlayer = this.animationPlayer;
                if (animationPlayer != null)
                {
                    return animationPlayer.gameObject;
                }
                return null;
            }
        }

        public virtual float timeOffset
        {
            get
            {
                return this.m_TimeOffset;
            }
            set
            {
                this.m_TimeOffset = value;
            }
        }
    }
}

