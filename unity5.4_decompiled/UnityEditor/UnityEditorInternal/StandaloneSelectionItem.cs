﻿namespace UnityEditorInternal
{
    using System;
    using UnityEngine;

    internal class StandaloneSelectionItem : AnimationWindowSelectionItem
    {
        public static StandaloneSelectionItem Create()
        {
            StandaloneSelectionItem item = ScriptableObject.CreateInstance(typeof(StandaloneSelectionItem)) as StandaloneSelectionItem;
            item.hideFlags = HideFlags.HideAndDontSave;
            return item;
        }

        public override AnimationClip animationClip
        {
            get
            {
                if (this.animationPlayer == null)
                {
                    return null;
                }
                return base.animationClip;
            }
            set
            {
                base.animationClip = value;
            }
        }
    }
}

